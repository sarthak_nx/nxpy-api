import os
from setuptools import setup, find_packages
from distutils.command.sdist import sdist as _sdist

class sdistzip(_sdist):

    user_options = _sdist.user_options + [
        ('version=', None, "Version of the package")
    ]

    def initialize_options(self):
        _sdist.initialize_options(self)
        self.version = self.distribution.metadata.version

    def run(self):
        self.distribution.metadata.version = self.version
        _sdist.run(self)

    def make_distribution(self):
        base_dir = 'target/' + self.distribution.get_name() + '-' + self.version
        self.make_release_tree(base_dir, self.filelist.files)
setup(
    name='nxpy-api',
    version='0.01',
    packages=find_packages(),
    url='https://bitbucket.org/sarthak_nx/nxpy-api/src',
    license='',
    install_requires = [],
    author='Numerix',
    author_email='',
    description=''
)
