"""
This is a set of common tools
"""
from __future__ import print_function
from __future__ import absolute_import
import re
from itertools import repeat
import numbers, logging
from nxpy import nxpropy as nx

from nxpy2.nxtools.NxDict import NxDict
import re as _re

__author__ = 'Pawel Konieczny'

logger = logging.getLogger('nxtools.common')

def canonical_id(_id):
    """
    Function returns a canonical version of the id provided.
    :param _id: ID to be transformed
    :return: canonical version of the ID
    """
    if isinstance(_id, str):
        return re.sub('\s', '', _id).upper()  # remove white spaces and upper case the string
    else:
        return _id
        # OLD: return re.sub('[\s+]', '', id).upper()  # remove white spaces and upper case the string


def as_list(el):
    """
    Function checks if the element is iterable - if not, it's wrapped as a one-element list.
    Strings are treated separately (they are iterable). If `el` is a string then it's wrapped
    as a one-element list.
    :param el: element to be treated as a list
    :return: iterable (list) with elements from `el`
    :rtype: iterable
    """
    # TODO: consider returning a generator () rather than a list []
    if isinstance(el, str):
        return [el]
    try:
        _ = iter(el)
    except TypeError:
        return [el]
    return el


def as_date(d, datemode=0):
    """
    Convenience function which returns `datetime` objects corresponding to a given date `d` if that
      `d` is in Excel format (a number). Otherwise it returns `d` without modifications.
    :param d: 
    :param datemode: passed to `xlrd` module.
    Denotes whether the epoch should be 1900 or 1904 (default = 0).
    :return: 
    """
    try:
        import xlrd
        if isinstance(d, numbers.Number):
            return xlrd.xldate.xldate_as_datetime(d, datemode=datemode)
        return d
    except ImportError as e:
        raise ImportError("as_date requires `xlrd` module to be installed.")


def add_tenor(app, date, tenor, conventions='None', calendar='None', warning=None,
              end_of_month=False, object_field='DATES', datemode=0):
    """
    Convenience function to return a new date based on the start date `date` and `tenor`.
    If `date` is a string it is treated as a name of an object within `app`. In that case
    that object will be queried for the field `object_field`.
    If `date` is a number then it is assumed it should be converted from Excel format
     (see `as_date` function).
    :param app: `Application` object
    :param date: start date
    :param tenor: tenor
    :param conventions: roll convention passed to `add_tenor` function (default 'None') 
    :param calendar: holidays calendar to use
    :param warning: `ApplicationWarning` object. If `None` it will be automatically created.
    :param end_of_month: end of month convention (see `Application.end_of_month` for more details)
    :param object_field: id of the field to be queried if `date` is an ID (string).
    :return: date
    :rtype: date
    """
    from nxpy2.nxtools import view
    if isinstance(date, str):
        call_obj = view.call(app, date)
        assert object_field in call_obj, "Field {} does not exist in object {}".format(object_field,
                                                                                       date)
        date = call_obj[object_field]
    date = as_date(date, datemode=datemode)
    warning = nx.ApplicationWarning() if warning is None else warning
    return app.add_tenor(date, tenor, conventions, calendar, warning, end_of_month)


def sub_tenor(app, date, tenor, conventions='None', calendar='None', warning=None,
              end_of_month=False, object_field='DATES', datemode=0):
    """
    Convenience function to return a new date based on the start date `date` and `tenor`.
    If `date` is a string it is treated as a name of an object within `app`. In that case
    that object will be queried for the field `object_field`.
    If `date` is a number then it is assumed it should be converted from Excel format
     (see `as_date` function).
    :param app: `Application` object
    :param date: start date
    :param tenor: tenor
    :param conventions: roll convention passed to `add_tenor` function (default 'None') 
    :param calendar: holidays calendar to use
    :param warning: `ApplicationWarning` object. If `None` it will be automatically created.
    :param end_of_month: end of month convention (see `Application.end_of_month` for more details)
    :param object_field: id of the field to be queried if `date` is an ID (string).
    :return: date
    :rtype: date
    """
    from nxpy2.nxtools import view
    if isinstance(date, str):
        call_obj = view.call(app, date)
        assert object_field in call_obj, "Field {} does not exist in object {}".format(object_field,
                                                                                       date)
        date = call_obj[object_field]
    date = as_date(date, datemode=datemode)
    warning = nx.ApplicationWarning() if warning is None else warning
    return app.sub_tenor(date, tenor, conventions, calendar, warning, end_of_month)


def as_iterators_longest(*args):
    """
    Function returns arguments as a list of iterators matching the length of the longest one.
    Shorter ones will be reused but only single ones (to prevent accidental use when
    the longest one is not a multiple of shorter ones).
    :param args: 
    :return: zip of iterators
    """
    list_args = list(map(as_list, args))
    lengths = list(map(len, list_args))
    all_lengths = set(lengths)
    assert (len(all_lengths) == 1) or (len(all_lengths) == 2 and 1 in all_lengths), \
        "Length of arguments should be either one or m for some m. Instead lengths: {}".format(
            all_lengths)
    max_len = max(all_lengths)
    iter_list = [repeat(next(iter(list_args[k])), max_len) if lengths[k] == 1 else list_args[k] for
                 k in range(len(list_args))]
    return zip(*iter_list)


def match_key_values_into_dict(keys, values):
    """
    Function returns a dictionary with keys and values. It takes care of assembling a list
    for a given key if there is more than one corresponding value.
    :param keys: list of keys (can be with duplicates)
    :param values: list of corresponding values
    :rtype: NxDict
    """
    # Note: we could use defaultdict from collections
    # but we anticipate doing some key manipulation later on
    ret_dic = NxDict()
    assert len(keys) == len(values), "Keys and Values should have the same length"
    for key, val in zip(keys, values):
        if key not in ret_dic:
            ret_dic[key] = val  # one element
        else:
            if isinstance(ret_dic[key], list):
                ret_dic[key].append(val)  # more than two elements
            else:
                ret_dic[key] = [ret_dic[key], val]  # two elements
    return ret_dic

def as_key_value(d):
    """Function returns key/value pair for a given dictionary according to the rules
    when lists are present as values"""
    keys = []
    values = []
    for key in d:
        if isinstance(d[key], list):     # was type is list, 20161215
            keys.extend([key] * len(d[key]))
            values.extend(d[key])
        else:
            keys.append(key)
            values.append(d[key])
    # use the previously created function to do the rest
    return keys, values

def inverse_dict(d):
    """
    Function inverses the dictionary, that is (key, value) -> (value, key), taking care
    of lists too.
    :param d:
    :return:
    """
    keys, values = as_key_value(d)
    return match_key_values_into_dict(keys=values, values=keys)


def filter_dictionary(data, query, value_name='quote'):
    """Function filters out the dictionary using provided query. The query result will be expanded
    to a dictionary."""
    if isinstance(query, str):
        q = _re.compile(query, _re.I)
    else:
        q = query   # assuming it's a compiled query

    matched = [m for m in (q.match(k) for k in data) if m]
    res = {m.string: m.groupdict() for m in matched}
    for m in matched:
        res[m.string][value_name] = data[m.string]
    return res


def pprint(x, trim_col=20):
    """ Function prints a key/value dictionary in a table format.
     This is to show Numerix objects (or Outputs) in a pretty form.
     This implementation is not perfect - rather an example of how this can be done.
     It may have some flaws.
    :param x: dictionary to be printed.
    :param trim_col: widest column - if the text is wider it'll be trimmed. If None - no trimming
    :type x: dict
    """
    key_max_len = max([len(k) for k in list(x.keys())])
    val_max_len = max([len(v) for v in list(x.values())])
    if trim_col is not None:
        key_max_len = min(key_max_len, trim_col)
        val_max_len = min(val_max_len, trim_col)
    keys = [str(key)[:key_max_len] + '..'
            if len(str(key)) > key_max_len else str(key) for key in x.keys()]
    vals = [str(val)[:key_max_len] + '..'
            if len(str(val)) > key_max_len else str(val) for val in x.values()]
    # keys left align, vals right aligh
    format_string = '|{:<' + repr(key_max_len) + '} | {:>' + repr(val_max_len) + '}|'
    rows = [format_string.format(k, v) for (k, v) in zip(keys, vals)]
    print('-' * (key_max_len + val_max_len + 5) + '\n' + '\n'.join(rows) +
          '\n' + '-' * (key_max_len + val_max_len + 5))


def parse_warnings(warning):

    warning_str = warning.to_string()
    warning_list = warning_str.split('\n')[1:]
    return warning_list


