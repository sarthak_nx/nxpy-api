"""
This is a set of tools to compute statistics and summaries of the Application object
"""
from __future__ import absolute_import

from nxpy import nxpropy as nx

import nxpy2.nxtools.view as nxview

__author__ = 'Pawel Konieczny'


def count_objects(app):
    # TODO: description
    return {
        'calls': len(app.getCallIDs()),
        'data': len(app.getDataIDs()),
        'matrix': len(app.getMatrixIDs())
    }


def count_times(app, objects_ids=[]):
    """
    This function goes through the Application object and sums up all the TIMER CPU fields.
    The scope can be restricted by providing list of objects_ids.

    :param app: Application object
    :param objects_ids: list of objects to be queried for TIMER CPU. If None - all Call objects will be queried.
    :return: cumulative time
    """
    if not objects_ids:
        objects_ids = app.get_call_ids()    # compute all
    w = nx.ApplicationWarning()
    ret = {'TIMER CPU': 0, 'TIMER': 0}
    for object_id in objects_ids:
        output = nxview.outputs(app=app, object_id=object_id, warning=w)
        ret['TIMER CPU'] += output.get('TIMER CPU', 0)
        ret['TIMER'] += output.get('TIMER', 0)
    return ret
