"""
This is a set of tools to browse through Numerix objects.
"""
from __future__ import absolute_import
import pandas as pd
import logging
import nxpy2.nxtools.view as nxview

__author__ = 'Pawel Konieczny'

logger = logging.getLogger('nxtools.search')

def all(app, predicate=None):
    """
    Function returns an iterator through all Application objects.
    :param app: Application object to extract information from
    :type app: Application
    :param predicate: predicate to be applied. If C{None} then all is returned.
    """
    for el in __yield_list_search(my_list=app.get_all_ids(), predicate=predicate):
        yield el


def calls(app, predicate=None):
    """
    Function returns an iterator through all ApplicationCall objects.

    :param app: Application object to extract information from
    :type app: Application
    :param predicate: predicate to be applied. If C{None} then all is returned.
    """
    for el in __yield_list_search(my_list=app.get_call_ids(), predicate=predicate):
        yield el


def data(app, predicate=None):
    """
    Function returns an iterator through all ApplicationMatrix objects.
    :param app: Application object to extract information from
    :type app: Application
    :param predicate: predicate to be applied. If C{None} then all is returned.
    """
    for el in __yield_list_search(my_list=app.get_data_ids(), predicate=predicate):
        yield el


def matrices(app, predicate=None):
    """
    Function returns an iterator through all ApplicationMatrix objects.

    :param app: Application object to extract information from
    :type app: Application
    :param predicate: predicate to be applied. If C{None} then all is returned.
    """
    for el in __yield_list_search(my_list=app.get_matrix_ids(), predicate=predicate):
        yield el


def list_of_type(app, object=None, type=None):
    """
    Function returns a list of object ids which correspond to a given choice of object and type.
    For example: object=MARKETDATA and type=YIELD will list all yield curve objects

    :param app: Application object
    :type app: nx.Application
    :param object: string describing the first (highest) level of hierarchy
    :param type: string describing the second level of hierarchy
    :return: list of IDs satisfying given criteria
    :rtype: list(str)
    """
    search_string = ""
    if object:
        search_string += object
    if type:
        search_string += "::" + type
    all_objects = nxview.all(app=app, object_id='GLOBALOBJECTS', headers_subset=['ID', 'Type'])
    return [_id for _id, _type in zip(all_objects['ID'], all_objects['Type'])
            if _type.startswith(search_string)]

def describe_object_list(app, object_list=None):
    """
    Function returns a list of object ids which correspond to a given choice of object and type.
    For example: object=MARKETDATA and type=YIELD will list all yield curve objects

    :param app: Application object
    :type app: nx.Application
    :param object_list: list of object IDs to be considered in search
    :return: list of IDs satisfying given criteria
    :rtype: list(str)
    """
    search_string = ""
    # if object:
    #     search_string += object
    predicate = lambda x: x in object_list

    all_objects = nxview.all(app=app, object_id='GLOBALOBJECTS')
    # all_objects = nxview.all(app=app, object_id='GLOBALOBJECTS', headers_subset=['ID', 'Type'])
    return [dict(id = _id, updated = _updated, type = _type, warning = _warning) for _id, _updated, _type, _warning in zip(all_objects['ID'], all_objects['UPDATED'], all_objects['TYPE'], all_objects['WARNINGS'])
            if predicate(_id)]

def find_object_from_keyword(app, keyword = ''):
    """Find an object type with type SCRIPT::INSTANTIATION in the Application object.

    :param app: Application object
    :type app: nx.Application
    :param predicate: predicate to subset the returned IDs. None = True.
    :rtype: list(str)
    """
    predicate = lambda x: keyword.upper() in x.upper()
    if not predicate:
        predicate = lambda x: True  # trivial predicate

    id = [id for id in all(app=app, predicate =predicate)]
    return id


def find_object_by_keyword(app, keyword = ''):
    """Find an object type with type SCRIPT::INSTANTIATION in the Application object.

    :param app: Application object
    :type app: nx.Application
    :param predicate: predicate to subset the returned IDs. None = True.
    :rtype: list(str)
    """
    predicate = lambda x: keyword in x
    if not predicate:
        predicate = lambda x: True  # trivial predicate
    id = [id for id in
          __yield_list_matching_type(app=app, type_predicate=lambda x: x.startswith('SCRIPT::INSTANTIATION')) if
          predicate(id)]
    return id


def find_object_by_keyword_by_type(app, keyword = '', type = None):
    """Find an object type with type 'type' in the Application object.

    :param app: Application object
    :type app: nx.Application
    :param keyword: object name substring to subset the search for object
    :param type: type of object to search for (MARKETDATA, EVENTS..)
    :rtype: list(str)
    """
    predicate = lambda x: keyword in x
    if not predicate:
        predicate = lambda x: True  # trivial predicate
    id = [id for id in
          __yield_list_matching_type(app=app, type_predicate=lambda x: x.startswith(type)) if
          predicate(id)]
    return id

def find_object(app, predicate=None):
    """Find an object type with type SCRIPT::INSTANTIATION in the Application object.
    
    :param app: Application object
    :type app: nx.Application
    :param predicate: predicate to subset the returned IDs. None = True.
    :rtype: list(str)
    """

    if not predicate:
        predicate = lambda x: True  # trivial predicate
    id = [id for id in __yield_list_matching_type(app=app, type_predicate=lambda x: x.startswith('SCRIPT::INSTANTIATION')) if
            predicate(id)]
    return id

def find_object_by_type(app, type = None):
    """Find an object type with type SCRIPT::INSTANTIATION in the Application object.

    :param app: Application object
    :type app: nx.Application
    :param predicate: type of object to search for (MARKETDATA, EVENTS..)
    :rtype: list(str)
    """

    id = [id for id in __yield_list_matching_type(app=app, type_predicate=lambda x: x.startswith(type))]
    return id


def terms_object(app):
    """Find terms object in the Application object.

    :param app: Application object
    :type app: nx.Application
    :param predicate: predicate to subset the returned IDs. None = True.
    :rtype: str
    """
    terms_id = find_object(app, lambda x: 'TERMS' in x and len(x.split('^')) == 3)
    return terms_id[0]


def pricer_object(app):
    """Find pricer object in the Application object.

    :param app: Application object
    :type app: nx.Application
    :param predicate: predicate to subset the returned IDs. None = True.
    :rtype: str
    """
    pricer_id = find_object(app, lambda x: 'PRICER' in x and len(x.split('^')) == 3)

    return pricer_id[0]

def model_objects(app):
    """Find all model objects in the Application object.

    :param app: Application object
    :type app: nx.Application
    :param predicate: predicate to subset the returned IDs. None = True.
    :rtype: list(str)
    """
    model_id = find_object(app, lambda x: 'MODEL' in x and len(x.split('^')) == 3)
    return model_id

def market_data_objects(app, predicate = None):
    """Find all MDE objects in the Application object.

    :param app: Application object
    :type app: nx.Application
    :param predicate: predicate to subset the returned IDs. None = True.
    :rtype: list(str)
    """
    market_objects_id = find_object(app, lambda x: 'MDE' in x and len(x.split('^')) == 3)
    if bool(market_objects_id) == False:
        market_objects_id = find_object(app, lambda x: 'MID' in x and len(x.split('^')) == 3)
    return [x for x in market_objects_id if predicate.upper() in x.upper()]

def find_script_objects(app, predicate=None):
    """Find an object type in the Application object.

    :param predicate: predicate to subset the returned IDs. None = True.
    :rtype: list(str)
    """

    if not predicate:
        predicate = lambda x: True  # trivial predicate
    id = [id for id in __yield_list_matching_type(app=app, type_predicate=lambda x: x.startswith('SCRIPT::INSTANTIATION')) if
            predicate(id)]

    return id

def pricers(app, predicate=None):
    """Find all pricers in the Application object. This should include KERNEL PRICERS, Analytic Pricers, etc.

    :param predicate: predicate to subset the returned IDs. None = True.
    :rtype: list(str)
    """
    # TODO: add other pricers
    if not predicate:
        predicate = lambda x: True  # trivial predicate
    return [_id for _id in __yield_list_matching_type(app=app, type_predicate=lambda x: x in set(['KERNEL::PRICE'])) if
            predicate(_id)]


def market_data(app, predicate=None):
    """Function returns a list of market data elements in a given Application

    :param predicate: predicate to subset the returned IDs. None = True.
    :rtype: list(str)
    """
    if not predicate:
        predicate = lambda x: True  # trivial predicate
    return [id for id in __yield_list_matching_type(app=app, type_predicate=lambda x: x.startswith('MARKETDATA::')) if
            predicate(id)]


def models(app, predicate=None):
    """Function returns a list of models in a given Application

    :param predicate: predicate to subset the returned IDs. None = True.
    :rtype: list(str)
    """
    if not predicate:
        predicate = lambda x: True  # trivial predicate
    return [id for id in __yield_list_matching_type(app=app, type_predicate=lambda x: x.startswith('MODEL::')) if
            predicate(id)]


def indices(app, predicate=None):
    """Function returns a list of models in a given Application

    :param predicate: predicate to subset the returned IDs. None = True.
    :rtype: list(str)
    """
    if not predicate:
        predicate = lambda x: True  # trivial predicate
    return [id for id in __yield_list_matching_type(app=app, type_predicate=lambda x: x.startswith('INDEX::')) if
            predicate(id)]


def kernel_components(app, predicate=None):
    """Function returns a list of Kernel Components

    :param predicate: predicate to subset the returned IDs. None = True.
    :rtype: list(str)
    """
    if not predicate:
        predicate = lambda x: True  # trivial predicate
    return [id for id in __yield_list_matching_type(app=app, type_predicate=lambda x: x == 'KERNEL::COMPONENTS')
            if predicate(id)]


def tsos(app, predicate=None):
    """Function returns a list of TSO (Time Slice Observers) in a given Application

    :param predicate: predicate to subset the returned IDs. None = True.
    :rtype: list(str)
    """
    if not predicate:
        predicate = lambda x: True  # trivial predicate
    return [id for id in __yield_list_matching_type(app=app, type_predicate=lambda x: x == 'INDEX::TIMESLICEOBSERVER')
            if predicate(id)]


def tso_results(app, tso_ids=None, predicate=None, warning=None):
    """Function returns a list of TSO (Time Slice Observers) Results in a given Application

    :param predicate: predicate to subset the returned IDs. None = True.
    :rtype: list(str)
    """
    # TODO: make predicate work!
    res = []
    if not tso_ids:
        tso_ids = tsos(app=app)
    elif not isinstance(tso_ids, list):
        tso_ids = [tso_ids]
    for tso_id in tso_ids:
        # for each TSO grab the outputs and extract results IDs
        outputs = nxview.outputs(app=app, object_id=tso_id, warning=warning)
        for output in outputs:
            if output.startswith('RESULTS'):
                res.append(outputs[output])
    return res


def events(app, predicate=None):
    """Function returns a list of TSO (Time Slice Observers) in a given Application

    :param predicate: predicate to subset the returned IDs. None = True.
    :rtype: list(str)
    """
    if not predicate:
        predicate = lambda x: True  # trivial predicate
    return [_id for _id in __yield_list_matching_type(app=app, type_predicate=lambda x: x.startswith('EVENTS::'))
            if predicate(_id)]


def __yield_list_matching_type(app, type_predicate):
    """Find all IDs which correspond to a given list of types.

    :param type_predicate: predicate which will tell whether to include an id or not
    """
    all_objects = nxview.all(app=app, object_id='GLOBALOBJECTS', headers_subset=['ID', 'Type'])
    for obj_id, obj_type in zip(all_objects['ID'], all_objects['Type']):
        if type_predicate(obj_type):
            yield obj_id


def __yield_list_search(my_list, predicate):
    """
    Generic function which searches through a given list and yields the result

    :rtype: generator
    :param my_list: list to be iterated through
    :param predicate: predicate to be used to filter results. If C{None} then always true.
    """
    for el in my_list:
        if not predicate or predicate(el):
            yield el


def application_object_summary(app):
    """
    Function which summarizes the objects contained in the application based on the object type. As example,
    NXLIBDATA objects will be grouped together, MARKETDATA::YIELD objects will be grouped together, etc.
    Args:
    :param app: Application object
    :type app: nx.Application
    Returns:
    :return: dict of object IDs categorized by type
    :rtype: dict(list)
    """
    all_objects = nxview.all(app=app, object_id='GLOBALOBJECTS', headers_subset=['ID', 'Type'])

    all_objects_df = pd.DataFrame([], columns=['ID', 'Type'])
    all_objects_df['ID'] = all_objects['ID']
    all_objects_df['Type'] = all_objects['Type']
    all_objects_df = all_objects_df.sort_values(['Type','ID'])
    application_summary = {}
    for object_type in all_objects_df['Type'].unique():
        application_summary[object_type] = list(all_objects_df[all_objects_df['Type'] == object_type]['ID'])

    return application_summary