"""
This is a set of tools to query Numerix objects.
"""
from __future__ import print_function
from __future__ import absolute_import
from nxpy import nxpropy as _nx
import sys  # for error reporting
from nxpy2.nxtools import common as _common
# from market_scenario_service.core.nxtools import static as _static
from nxpy2.nxtools.NxDict import NxDict as _NxDict


__author__ = 'Pawel Konieczny'


# TODO: unify providing warning object in accessors


def all(app, object_id, warning=None, headers_subset=[]):
    """
    Function returns a given object as a dictionary with dictionary keys from object view headers
    and with dictionary values from object view values (lists).
    It is equivalent to Object Viewer from Excel.

    :param app: Application object
    :param object_id: ID of the object to be queried
    :param warning: warning object to be used while querying the object.
    If *None* a temporary will be created.
    :param headers_subset: request only provided list of headers.
    If *subset* is empty then no restrictions.
    :return: Dictionary (key'd by headers) with corresponding lists as values
    :rtype: dict
    """

    if not warning:
        warning = _nx.ApplicationWarning()
    # temporary data object to query for results
    data = _nx.ApplicationData()
    # querying the application object
    if headers_subset:
        app.view(object_id, headers_subset, data, warning)
    else:
        app.view(object_id, data, warning)
    # at this point warning may have a Fatal error if no object has been found
    # but also data will be empty. Mind as well just go through data headers.
    ret = _NxDict()  # this is more important if one wants to recreate a matrix from data object

    for k in data.headers():
        if not headers_subset or k in headers_subset:
            try:
                ret[k] = data.data(k)
            except Exception as e:
                raise e
    return ret


def field(app, object_id, field_id, warning=None):
    """
    Function returns a given output for a given object.
    Typically one would fetch 'results' this way.

    :param app: Application object
    :param object_id: ID of the object to be queried
    :param field_id: name of the output field
    :param warning: warning object to be used while querying the object.
    If *None* a temporary will be created.
    :return: scalar or a list
    """
    if not warning:
        warning = _nx.ApplicationWarning()
    # temporary data object to query for results
    data = _nx.ApplicationData()
    # querying the application object
    app.view(object_id, field_id, data, warning)
    return data.data(field_id)


def call(app, object_id, warning=None):
    """
    Function returns a dictionary (key/value(s)) which can be used to recreate this object.

    :param app: Application object
    :param object_id: ID of the object to be queried
    :param warning: warning object to be used while querying the object.
    If *None* a temporary will be created.
    :return: Dictionary (key'd by headers) with corresponding lists as values
    :rtype: dict
    """
    call_headers = ['CALL HEADERS', 'CALL VALUES']
    warning = _nx.ApplicationWarning() if warning is None else warning
    res = all(app=app, object_id=object_id, warning=warning, headers_subset=call_headers)
    if res:
        assert call_headers[0] in res and call_headers[1] in res, '{} is not a CALL'.format(object_id)
        return _common.match_key_values_into_dict(res['CALL HEADERS'], res['CALL VALUES'])
    else:
        return res


def outputs(app, object_id, warning=None):
    """
    For a given object function returns a dictionary of results which is assembled
    from OUTPUT HEADERS matched with OUTPUT VALUES for a given object.
    Assumption here is that OUTPUT HEADERS are unique. If they are not please use other methods.
    This function is useful if one wants to get results of some computations as a dictionary.

    :param app: Application object
    :param object_id: ID of the object to be queried
    :param warning: warning object to be used while querying the object.
    If *None* a temporary will be created.
    :return: Dictionary (key'd by headers) with corresponding lists as values
    :rtype: dict
    """
    output_headers = ['OUTPUT HEADERS', 'OUTPUT VALUES']
    res = all(app=app, object_id=object_id, warning=warning, headers_subset=output_headers)
    assert output_headers[0] in res and \
           output_headers[1] in res, '{} does not have OUTPUT headers'.format(object_id)
    assert len(res[output_headers[0]]) == len(res[output_headers[1]]), \
        '{} should have the same length of output headers and values'.format(object_id)
    return _common.match_key_values_into_dict(res['OUTPUT HEADERS'], res['OUTPUT VALUES'])


def dependencies(app, object_id, warning=None):
    """
    Function returns a list of dependencies for a given Numerix object.
    If the object is ApplicationData or ApplicationMatrix then empty list is returned.
    :param app: Application
    :param object_id: id of the object
    :param warning: warning object (optional)
    :return: list of dependencies
    """
    if warning is None:
        warning = _nx.ApplicationWarning()
    all_call_ids = app.get_call_ids()
    object_id = _common.canonical_id(object_id)
    if object_id not in all_call_ids:
        return []
    res = all(app=app, object_id=object_id, warning=warning, headers_subset=['DEPENDENCIES'])
    return res.get('DEPENDENCIES', [])


def matrix(app, object_id, warning=None, transpose=True, as_matrix=False):
    """
    This function returns an ApplicationMatrix as a dictionary (NxDict).
    It takes into account missing values in the matrix.
    :param transpose: whether to transpose the matrix.
    This is often useful to overcome a small bug in CA.
    """
    if warning is None:
        warning = _nx.ApplicationWarning()
    mat = _as_matrix(app=app, object_id=object_id, warning=warning)
    if as_matrix:
        return mat
    if mat is None:
        return None
    cols_no = mat.num_columns()
    rows_no = mat.num_rows()
    ret_dict = _NxDict()
    ret_dict['DATAMATRIX'] = []
    if not transpose:
        for row_no in range(rows_no):
            ret_dict['DATAMATRIX'].append(mat.row_header(row_no))
        for col_no in range(cols_no):
            col = []
            for row_no in range(rows_no):
                if not mat.is_empty(row_no, col_no):
                    col.append(mat.get(row_no, col_no))
                else:
                    col.append(None)
            ret_dict[mat.col_header(col_no)] = col
    else:
        for col_no in range(cols_no):
            ret_dict['DATAMATRIX'].append(mat.col_header(col_no))
        for row_no in range(rows_no):
            col = []
            for col_no in range(cols_no):
                if not mat.is_empty(row_no, col_no):
                    col.append(mat.get(row_no, col_no))
                else:
                    col.append(None)
            ret_dict[mat.row_header(row_no)] = col
    return ret_dict


def convention_field(convention, field, app=None, default=None):
    """Function allows one to fetch value of a given convention field.
    If `app` is `None` the convention will be fetched from `static` module, otherwise
    the convention will be fetched from the Application with a fallback to the `static` module."""
    if app is not None:
        warning = _nx.ApplicationWarning()
        conv_call = call(app=app, object_id=convention, warning=warning)
        if conv_call:
            data_call = all(app=app, object_id=conv_call['DEFAULTVALUES'], warning=warning)
            _conv_dict = _common.match_key_values_into_dict(keys=data_call['NAME'],
                                                            values=data_call['VALUE'])
            return _conv_dict.get(field, default)
        else:
            app = None
    if app is None:
        conv_list = _static.get_conventions(conventions=convention)
        _type, (_id, _table_dict) = conv_list[0]     # asserting this is the first element
        _conv_dict = _common.match_key_values_into_dict(keys=_table_dict['NAME'], values=_table_dict['VALUE'])
        return _conv_dict.get(field, default)


def _as_matrix(app, object_id, warning=None):
    """
    This function returns an ApplicationMatrix instance from the Application.
    None and an error msg if there are any fatal errors
    """
    m = _nx.ApplicationMatrix()
    if warning is None:
        warning = _nx.ApplicationWarning()
    app.view_as_matrix(object_id, m, warning)
    if warning.count_fatals() > 0:
        print(str(warning), file=sys.stderr)
        # TODO: introduce Numerix Exceptions and throw them here and there
        return None
    return m


def str(app, object_id):
    """
    This function simply queries the app for the object with C{object_id} and displays
    it as an ApplicationData native string
    """
    d = _nx.ApplicationData()
    w = _nx.ApplicationWarning()
    app.view(object_id, d, w)
    if w.countFatals() > 0:
        print(str(w), file=sys.stderr)
        return None
    return str(d)
