"""
This is a set of tools to help streamline creation of Numerix objects.
"""
from __future__ import print_function
from __future__ import absolute_import
import numbers
import sys
import logging
from nxpy import nxpropy as nx
from nxpy2.nxtools.NxDict import NxDict


__author__ = 'Pawel Konieczny'

logger = logging.getLogger('nxtools.create')


def data(app, object_id, data_dictionary=None, warning=None, **kwargs):
    """
    This function makes it easier to create ApplicationData objects within an Application.
    If both data_dictionary and kwargs are provided then kwargs take precedence.

    :param app: Application object
    :param object_id: ID of the resulting ApplicationData object
    :param data_dictionary: dictionary of keys and values to be put as ApplicationData
    :param warning: ApplicationWarning object which will capture warnings
    :param kwargs: Additional parameters which will be used to update *data_dictionary*
    :return: ID of the newly created object
    :rtype: basestring
    """
    if data_dictionary is None:
        data_dictionary = {}
    logger.debug(
        'Data: registering object Data with ID=%s, '
        'data_dictionary=%s, kwargs=%s', object_id, data_dictionary, kwargs)
    d = nx.ApplicationData()
    if not warning:
        warning = nx.ApplicationWarning()
    # let's combine both dictionaries
    data_to_be_added = {}
    data_to_be_added.update(data_dictionary)
    data_to_be_added.update(kwargs)
    # and then cycle through the combination
    for header in data_to_be_added:
        if type(data_to_be_added[header]) is list:
            d.add_values(header, data_to_be_added[header])
        else:
            d.add_values(header, [data_to_be_added[header]])  # list with one element
    return app.data(d, object_id, warning)


def call(app, headers=None, values=None, values_dict=None, warning=None, **kwargs):
    """
    This function makes it easier to create and register ApplicationCall objects
    in a given Application context.
    Three ways are possible: by specifying headers/values, by specifying a dictionary of values,
    or by specifying function parameters (kwargs).
    Note: if both values_dict and kwargs are provided then kwargs will overwrite values_dict.

    :param app: Application object
    :param headers: list of headers
    :param values: list of corresponding values
    :param values_dict: dictionary of headers and corresponding values.
    If a value is a list it will be appropriately treated
    :param warning: ApplicationWarning object to capture any warnings
    :return: ID of the newly created object
    :rtype: basestring
    """
    if values_dict is None:
        values_dict = {}
    if values is None:
        values = []
    if headers is None:
        headers = []
    logger.debug('Call: registering object with headers=%s, '
                'values=%s, values_dict=%s, '
                'kwargs=%s', headers, values, values_dict, kwargs)

    if len(headers) > 0 or len(values) > 0:
        if len(values) != len(headers):
            raise BaseException("Headers and Values vectors should have the same length")
        else:
            return __register_call_using_headers_values(app=app, headers=headers,
                                                        values=values, warning=warning)

    dict_to_be_passed = values_dict.copy()
    dict_to_be_passed.update(kwargs)
    return __register_call_using_dictionary(app=app, values_dict=dict_to_be_passed, warning=warning)


def matrix(app, object_id, values_dict, warning=None):
    """
    This function registers a matrix from a dictionary. Please note that the dictionary should have
    the same structure as a dictionary which one would get from an ApplicationData object after
    viewing a given matrix. In particular "DATAMATRIX" header should be available in the first
    column. Order of the dictionary keys matters.
    """
    logger.debug('Matrix: registering object %s with dictionary %s', object_id, values_dict)
    if warning is None:
        warning = nx.ApplicationWarning()
    m = nx.ApplicationMatrix()
    # added on 20160628:
    if not isinstance(values_dict, NxDict):
        values_dict = NxDict(values_dict)
    if type(values_dict) is dict:
        values_dict = NxDict(values_dict)
    assert 'DataMatrix' in values_dict, "DataMatrix column should be present in the dictionary."
    for row_heading in values_dict['DataMatrix']:
        logger.debug('Matrix: Adding row heading "%s"', row_heading)
        m.add_row(row_heading)
    for col_idx, col_name in enumerate(values_dict):
        if col_name != 'DATAMATRIX':
            logger.debug('Matrix: Adding col "%s"', col_name)
            m.add_column(col_name)
            if type(values_dict[col_name]) is list:
                for row_idx, val in enumerate(values_dict[col_name]):
                    logger.debug('Matrix: Adding value "%s" '
                                 'at position (%s, %s)', val, row_idx, col_idx - 1)
                    if isinstance(val, numbers.Number):
                        m.set(row_idx, col_idx - 1, val)  # account for DataMatrix as a column
            else:
                logger.debug(
                    'Matrix: Adding value "%s" '
                    'at position (%s, %s)', values_dict[col_name], 0, col_idx - 1)
                if isinstance(values_dict[col_name], numbers.Number):
                    m.set(0, col_idx - 1, values_dict[col_name])  # account for DataMatrix as column
    return app.matrix(m, object_id, warning)


def from_sequence(app, objects, warning=None):
    """
    Function instantiates a sequence of objects. Iterable `objects` should be in a form of tuples:
    (type, data) where data is either a dictionary (for ApplicationCall objects)
    or a tuple (id, dictionary) for ApplicationData or ApplicationMatrix objects.
    :param app:
    :param objects: list of objects
    :param warning:
    :return: list of created objects
    :rtype: list(str)
    """
    warning = nx.ApplicationWarning() if warning is None else warning
    all_ids = []
    for t, form in objects:
        t = t.lower()
        if t == 'data':
            _id, _data = form
            all_ids.append(data(app=app, object_id=_id, data_dictionary=_data, warning=warning))
        elif t == 'call':
            all_ids.append(call(app=app, values_dict=form, warning=warning))
        elif t == 'matrix':
            _id, _data = form
            all_ids.append(matrix(app=app, object_id=_id, values_dict=_data, warning=warning))
        else:
            raise NotImplemented('Do not recognize type %s' % t)
    return all_ids


def __register_call_using_headers_values(app, headers, values, warning):
    if not warning:
        warning = nx.ApplicationWarning()
    fatals_count = warning.count_fatals()
    c = nx.ApplicationCall()
    for (h, v) in zip(headers, values):
        c.add_value(h, v)
    d = nx.ApplicationData()

    app.call(c, [], d, warning)
    # app.call(c, d, warning)
    if warning.count_fatals() > fatals_count:
        print(str(warning), sys.stderr)
        raise BaseException(
            "Error registering ApplicationCall with given headers and values.\n"
            "You may want to view GLOBALWARNINGS object in the Application.\n"
            "Warning object: {}".format(str(warning)))
    assert len(d.data('id')) == 1, "Registration of this object should result in an ID."
    return d.data('ID')[0]


def __register_call_using_dictionary(app, values_dict, warning):
    # let's do translation of dict into headers and values
    headers = []
    values = []
    for key in values_dict:
        if isinstance(values_dict[key], list):     # was type is list, 20161215
            for elem in values_dict[key]:
                headers.append(key)
                values.append(elem)
        else:
            headers.append(key)
            values.append(values_dict[key])
    # use the previously created function to do the rest
    return __register_call_using_headers_values(app=app, headers=headers,
                                                values=values, warning=warning)


def data_from_list(app, object_id, headers = None, data_list=None, warning=None, **kwargs):

    if data_list is None:
        data_list = {}
    logger.debug(
        'Data: registering object Data with ID=%s, '
        'data_list=%s, kwargs=%s', object_id, data_list, kwargs)
    d = nx.ApplicationData()
    if not warning:
        warning = nx.ApplicationWarning()

    table_headers = headers if headers else list(data_list[0].keys())

    data_dictionary = {}
    for k in table_headers:
        data_dictionary[k] = []

    for row in data_list:
        for k in row.keys():
            data_dictionary[k].append(row[k])

    data_to_be_added = {}
    data_to_be_added.update(data_dictionary)
    data_to_be_added.update(kwargs)

    for header in data_to_be_added:
        if type(data_to_be_added[header]) is list:
            d.add_values(header, data_to_be_added[header])
        else:
            d.add_values(header, [data_to_be_added[header]])  # list with one element
    return app.data(d, object_id, warning)