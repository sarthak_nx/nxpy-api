"""
Module provide implementation of a dictionary which keeps the order of entries (OrderedDict)
as well as uses sanitized version of a key to store results. This means that keys:
key, K e Y, KEY are all equivalent and interchangeable.
"""
from collections import OrderedDict
import six, re

import logging
logger = logging.getLogger('nxtools.NxDict')

class NxDict(OrderedDict):
    """
    An OrderedDict implementation which should equatize keys according to Numerix standard.
    Numerix treats keys case and white space insensitive.
    """

    @classmethod
    def __sanitize(self,key):
        return self.canonical_id(key)
#        if isinstance(key, numbers.Number):
#            return key
        # assert isinstance(key, str), 'currently supported only numeric and string values as keys'

#        return ''.join(str(key).split()).upper()   # remove any whitespace and upper

    @staticmethod
    def canonical_id(_id):
        """
        Function returns a canonical version of the id provided.
        :param _id: ID to be transformed
        :return: canonical version of the ID
        """
        if isinstance(_id, str):
            return re.sub('\s', '', _id).upper()  # remove white spaces and upper case the string
        else:
            return _id
            # OLD: return re.sub('[\s+]', '', id).upper()  # remove white spaces and upper case the string


    def __init__(self, initval=None):
        super(NxDict, self).__init__()
        if initval is None:
            initval = {}
        # self.__sanitize_re = re.compile('\s')    # for sanitize method

        if isinstance(initval, dict):
            for key, value in six.iteritems(initval):
                self.__setitem__(key, value)
        elif isinstance(initval, list):
            for (key, value) in initval:
                self.__setitem__(key, value)

    def __contains__(self, key):
        return OrderedDict.__contains__(self, self.__sanitize(key))

    def __getitem__(self, key):
        return OrderedDict.__getitem__(self, self.__sanitize(key))

    # previously the method below didn't have **kwargs
    def __setitem__(self, key, value, **kwargs):
        return OrderedDict.__setitem__(self, self.__sanitize(key), value, **kwargs)

    def __delitem__(self, key):
        OrderedDict.__delitem__(self, self.__sanitize(key))

    def get(self, key, default=None):
        try:
            val = OrderedDict.__getitem__(self, self.__sanitize(key))
        except KeyError:
            return default
        else:
            return val

    def has_key(self, key):
        if self.get(key):
            return True
        else:
            return False

if __name__ == '__main__':
    my_dict = NxDict()
    normal_dict = {}
    order_dict = OrderedDict()

    my_dict['z'] = 5
    my_dict['a'] = 3
    my_dict['a s d'] = 1

    order_dict['z'] = 5
    order_dict['a'] = 3
    order_dict['a s d'] = 1

    normal_dict['z'] = 5
    normal_dict['a'] = 3
    normal_dict['a s d'] = 1
    my_dict['ASd'] = 5
    normal_dict['ASd'] = 5

    print('my_dic.items = {}'.format(my_dict.items()))
    print('normal_dic.items = {}'.format(normal_dict.items()))
    print('order_dic.items = {}'.format(order_dict.items()))

    print('my_dic.items = {}'.format(my_dict))
    print('normal_dic.items = {}'.format(normal_dict))
    print('order_dic.items = {}'.format(order_dict))

    # print my_dic
