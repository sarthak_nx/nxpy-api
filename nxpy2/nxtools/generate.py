"""
This is a set of tools to generate code from abstract structures like graphs.
"""
from __future__ import print_function
from __future__ import absolute_import
import logging
from datetime import datetime

__author__ = 'Pawel Konieczny'

logger = logging.getLogger('nxtools.generate')
from nxpy2.nxtools import match_key_values_into_dict

def create_from_clipboard(dropna=True, nxtype='call', echo=True, appname='app',
                          substite_true_false=True,
                          guess_date = True,
                          guess_numbers=True,
                          date_format='%m/%d/%Y'):
    """
    Function reads from the clipboard a given object (keys/values for example) and prints
    out the code (using `nxtools.create`) needed to create the object.
    The code does not infer types of values.
    :param dropna: whether to drop NaN values
    :param nxtype: call/data/matrix
    :param echo: whether output the code
    :param appname: default name of the Application (as argument to nxtools.create)
    :param substite_true_false: whether to replace "TRUE" and "FALSE" strings with bool values,
    default `True`.
    :return: NxDict of the key/values for Call object.
    """
    import pandas as pd

    nxtype = nxtype.upper()
    if nxtype == 'CALL':

        tab = pd.read_clipboard(names=['Key', 'Value'], sep='\t')
        tab = tab.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
        if guess_date:
            def to_date(x):
                try:
                    return datetime.strptime(str(x), date_format).date()
                except ValueError:
                    return None
            res = tab.Value.apply(to_date)
            tab.Value[~res.isnull()] = res[~res.isnull()]
        if guess_numbers:
            def to_numbers(x):
                try:
                    return float(str(x))
                except ValueError:
                    return None
            res = tab.Value.apply(to_numbers)
            tab.Value[~res.isnull()] = res[~res.isnull()]
        if substite_true_false:
            tab.Value[tab.Value.str.upper() == 'FALSE'] = False
            tab.Value[tab.Value.str.upper() == 'TRUE'] = True
        if dropna:
            tab = tab.dropna()

        res = match_key_values_into_dict(keys=tab['Key'], values=tab['Value'])
        if echo:
            print('nxtools.create.call(app={appname}, values_dict={{'.format(appname=appname))
            print(',\n'.join(("'{key}': {value}".format(key=k, value=repr(res[k])) for k in res)))
            print('})')
        return res
    elif nxtype == 'DATA':
        raise NotImplemented('Data stripping is not yet implemented')
    elif nxtype == 'MATRIX':
        raise NotImplemented('Matrix stripping is not yet implemented')
    else:
        raise ValueError('Do not recognize nxtype. Available: call, data, matrix')
