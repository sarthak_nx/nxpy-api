from flask import Flask
import os
import logging
import json
from argparse import ArgumentParser
from inspect import currentframe, getframeinfo
from .base_settings import BaseSettings
from .config_server_client import ConfigServerClient

"""
Should not depend on anything except the /base folder contents
"""

# TODO how to use this log statement if logger hasnt been initialized yet !
log = logging.getLogger('BaseWebApp')


class BaseWebApp():
    _instance = None  # Singleton instance of this class
    _app = None  # Flask App
    _app_name = ''  # meant to be supplied by the class which inherits this class
    _app_url_prefix = '' # meant to be supplied by the class which inherits this class
    _url_prefix = ''  # meant to be supplied by the class which inherits this class
    _default_port = None #

    def __new__(cls, singleton=True):
        if not singleton:
            # Do not read or set the cls._instance in non singleton case
            return super(BaseWebApp, cls).__new__(cls)
        if not (cls._instance):
            # If instance doesnt yet exist, create it
            cls._instance = super(BaseWebApp, cls).__new__(cls)
        return cls._instance

    def __init__(self, singleton=True):
        pass
        # self.initialize()

    def initialize(self, **kwargs):
        """
        Pre defined sequence of steps to be used by all MS
        """
        try:
            env_parser = BaseWebApp.build_env_parser()
            self.initialize_settings_from_parser(env_parser)
            # self.initialize_settings(app_name= self._app_name, override_profile=args_dict.get('env', None), **kwargs)
            self.initialize_app_prerequisites(**kwargs)
            self.register_eureka(**kwargs)
            self.write_pid_file(**kwargs)
            self.create_app(**kwargs)
            log.info('App Routes: {}'.format(self.list_routes()))
        except Exception as e:
            log.error('Error in initializing app. Aborting...')
            log.error(e, exc_info=True)
            exit()

    def run(self):
        #TODO this should be same as Eureka?
        host = 'localhost' if BaseSettings().get('LOCAL_MODE', False) else '0.0.0.0'
        port=BaseSettings().get('PORT')
        try:
            from waitress import serve
            WAITRESS_INSTALLED = True
        except ImportError:
            log.info('Waitress not available')
            WAITRESS_INSTALLED = False

        #TODO: Remove
        WAITRESS_INSTALLED = False
        if BaseSettings().get('ENABLE_WAITRESS', True) and WAITRESS_INSTALLED:
            threads=BaseSettings().get('THREADS', 4)
            log.info('Running {} on Waitress with {} threads'.format(self._app_name, threads))
            #log.info('!!!!!!!!!!!!!!')
            serve(self._app, host=host, port=port, threads=threads)
        else:
            log.info('Running {} on Flask Server'.format(self._app_name))
            self._app.run(host=host, port=port,
                          threaded=BaseSettings().get('THREADED', True),
                          debug=BaseSettings().get('DEBUG_MODE', False))

    def create_app(self, **kwargs):
        """
        typically to be extended by each WebApp
        """
        log.info('Creating Flask App: {}'.format(self._app_name))
        self._app = Flask(self._app_name)
        if BaseSettings().get('ENABLE_CORS', True):
            from flask_cors import CORS
            CORS(self._app)
            log.info('Enabled CORS')
        log.info('Flask App created with name: {}'.format(self._app_name))
        return self._app

    def get_app(self):
        return self._app

    def initialize_settings_from_parser(self, parser, **kwargs):
        args_dict = BaseSettings().build_arg_dict(parser)
        print('Args : {}'.format(json.dumps(args_dict)))
        self.initialize_settings(app_name=self._app_name,
                                 app_url_prefix = self._app_url_prefix,
                                 root_dir_path=args_dict.get('app.home', None),
                                 oneview_home_dir_path=args_dict.get('oneview.home', None),
                                 log_dir_path=args_dict.get('logging.path', None),
                                 override_profile=args_dict.get('env', None), **kwargs)

    def initialize_settings(self, app_name: str, app_url_prefix:str, root_dir_path: str = None,
                            oneview_home_dir_path: str = None,
                            config_dir_path: str = None, data_dir_path: str = None, override_profile: str = None, log_dir_path:str=None,
                            logconfig_file_path: str = '', parser=None, config_file_extension='yml',
                            **kwargs):
        """
        Standard settings initialization to be used by all MS. caters for different modes
        :param app_name: eg - oneview-extsystem-interface. Used for property file prefix, both local and config server
        :param app_url_prefix: eg - oneview/rest/{app_url_prefix}. Used for all URLs of this MS including health, actuator etc
        :param root_dir_path: ROOT_DIR is defaulted assuming {root}/{app_name}/base/base_flask_app.py
        :param oneview_home_dir_path: ONEVIEW_DIR is defaulted to root_dir_path
        :param config_dir_path: CONFIG_DIR is defaulted using {root}/config
        :param data_dir_path: DATA_DIR is defaulted using {root}/data
        :param log_dir_path: LOG_DIR is defaulted using {oneview_home}/logs
        :param override_profile: PROFILE is defaulted by reading from {root}/config/profile.txt
        :param logconfig_file_path: is defaulted using {root}/config/logging.json
        :param parser: parser = argparse.ArgumentParser(). Not supported for microservice mode yet
        :param config_file_extension: used for loading property file when reading from directory or config server
        :return:
        """
        print('Initializing Settings...')
        try:
            if not root_dir_path:
                # use os.getcwd() approach instead of this? Refer to Radhi's work os.path.split(os.path.split(os.path.split(os.getcwd())[0])[0])[0]+ ‘/config/extsystem-interface/’
                path = getframeinfo(currentframe()).filename
                # ROOT_DIR is defaulted assuming {root}/{app_name}/base/base_flask_app.py
                parent_dir = BaseSettings.get_parent_dir(path)
                parent_dir = BaseSettings.get_parent_dir(parent_dir)
                root_dir_path = str(BaseSettings.get_parent_dir(parent_dir).resolve())
            BaseSettings().setup(root_dir_path=root_dir_path,
                                 oneview_home_dir_path=oneview_home_dir_path,
                                 config_dir_path=config_dir_path,
                                 data_dir_path=data_dir_path,
                                 log_dir_path=log_dir_path,
                                 override_profile=override_profile, app_name=app_name, app_url_prefix=app_url_prefix)
            BaseSettings().PORT = BaseSettings().get('PORT', self._default_port)
            BaseSettings().configure_logs(logconfig_file_path=logconfig_file_path)

            config_server_settings = dict()
            if override_profile:
                # if profile is not null, attempt fetching from config server. else assume that it will be picked using local profile
                config_server_settings = ConfigServerClient().get_properties_as_json(
                    app_name, override_profile, config_file_extension)
                if not config_server_settings:
                    log.info('No properties received from config server')

            # TODO add provision for initializing from profile but overriding from these other methods.
            # Probably can just load the {appname}.{fileextension} file without any env suffix and do a initialize_from_dict function first. and then do either of these below methods to override
            if config_server_settings:
                BaseSettings().initialize_from_dict(config_server_settings)
            elif parser:
                BaseSettings().initialize_from_args(parser)
            else:
                BaseSettings().initialize_from_profile(file_prefix='{}-'.format(app_name),
                                                       file_extension=config_file_extension)
            # Dont print this as it can have decrypted passwords
            # log.debug('Initialized settings: {}'.format(BaseSettings().toDict()))
        except Exception as e:
            print('Error in initializing settings:', e)
            raise e

    def register_eureka(self, **kwargs):
        if not BaseSettings().get('ENABLE_EUREKA', False):
            log.info('Eureka not enabled, skipping service registry')
            return
        from .eureka import register_and_renew_eureka
        register_and_renew_eureka()

    def initialize_app_prerequisites(self, **kwargs):
        """
        abstract method to be overridden by all MS where required
        """
        pass

    def list_routes(self):
        """
        :return: list of REST endpoints
        """
        return ['{}'.format(rule) for rule in self._app.url_map.iter_rules()]

    # self._app.iter_blueprints

    def write_pid_file(self):
        """
        writes process id in a file, used by deploy.bat for starting/stopping the MS
        """
        pid = str(os.getpid())
        f = open('application.pid', 'w')
        f.write(pid)
        f.close()
        log.info('Written pid: {} to application.pid'.format(pid))

    @staticmethod
    def build_env_parser():
        """
        Builds basic parser to parse the environment name on startup
        :return: parser
        """
        env_parser = ArgumentParser()
        env_parser.add_argument("-env", "--env",
                                help="env name. default will be picked from config/profile.txt")  # , default="local"
        env_parser.add_argument("-app.home", "--app.home",
                                help="application home. default will be picked from current file location")
        env_parser.add_argument("-oneview.home", "--oneview.home",
                                help="oneview deployment directory. default will be same as app.home")
        env_parser.add_argument("-logging.path", "--logging.path",
                                help="logging path. default will be oneview.home/logs")
        return env_parser
