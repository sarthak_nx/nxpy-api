from functools import wraps
from flask import request, jsonify
from .response import Response
from .base_settings import BaseSettings
import logging

log = logging.getLogger('AuthFilter')

def decode_jwt(encoded, public_key):
    import base64
    import jwt
    header, jwt_str, signature = encoded.split('.')
    pad = len(jwt_str) % 4
    jwt_str += "=" * pad
    encodedTrn = base64.urlsafe_b64decode(jwt_str.encode())
    b64_2 = base64.b64encode(encodedTrn)
    pritable = decompress_partial(base64.b64decode(pad_base64(b64_2)))
    token2 = header + '.' + base64.urlsafe_b64encode(pritable).decode() + '.' + signature
    decoded = jwt.decode(token2, public_key, algorithms='RS256', verify=False)
    return decoded


def pad_base64(data):
    missing_padding = len(data) % 4
    if missing_padding != 0:
        data += b'=' * (4 - missing_padding)
    return data


def decompress_partial(data):
    import zlib
    decompressor = zlib.decompressobj()
    return decompressor.decompress(data)


def secure_operation(object, operation, **kwargs):
    """
    Behaviour is similar to the Java implementation.
    Will raise a 403 exception if a user is not permitted for the object||operation combination.
    If devMode or system user, then entire check is bypassed.
    :param object: auth_object like trade, market-env etc
    :param operation: operation like view, create, approve etc
    :param kwargs:
    :return:
    """
    def decorator(f):
        @wraps(f)
        def decorated_function(**kwargs):
            if BaseSettings().get('DEV_MODE', False):
                return f(**kwargs)
            #print('DECORATOR for {}||{}'.format(object, operation))
            ov_claims = request.headers.get('X-Ov-Claims')
            if not ov_claims:
                message = 'Request unauthorised'
                print(message)
                return jsonify(Response.failure(message=message)), 401
            #print('DECORATOR Ov Claims: {}'.format(ov_claims))
            public_key = open(BaseSettings().get('API_GW_PUBLIC_KEY')).read()
            ov_claims_decoded = decode_jwt(ov_claims, public_key)
            #print('DECORATOR Decoded: {}'.format(json.dumps(ov_claims_decoded, indent=2)))
            user = ov_claims_decoded.get('ov-user', 'nxadmin')
            permissions_str = ov_claims_decoded.get('ov-claims', '')
            permissions = [tuple(p.split('||')) for p in permissions_str.split(",")]
            matches = [p for p in permissions if p[0].upper() == object.upper() and p[1].upper() == operation.upper()]
            if matches or user.upper() == BaseSettings().get('SYSTEM_USER', 'nxadmin').upper():
                log.debug('Authorization passed for user:{}, operation {}||{}'.format(user, object, operation))
                return f(**kwargs)
            else:
                message = 'Unauthorized operation [{}||{}]'.format(object, operation)
                log.error(message + 'for user:{}'.format(user))
                #raise Exception('Not authorized for {}||{}'.format(object, operation))
                return jsonify(Response.failure(message=message)), 403
        return decorated_function
    return decorator