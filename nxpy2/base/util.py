import json

from flask import jsonify

from .response import Response


def form_df_response(df):
    """
    :param df: pandas dataframe
    :return:
    """
    if not df.empty:
        return form_json_response(json.loads(df.to_json(orient='table', date_format='iso')))
    else:
        return form_json_response()


def form_json_response(data=None, message=None):
    return jsonify(Response.success(data=data, message=str(message)))


def form_error_response(message):
    return jsonify(Response.failure(message=str(message)))

# Function to display hostname and
# IP address
def get_host_name_IP():
    # Importing socket library
    import socket

    try:
        host_name = socket.gethostname()
        host_ip = socket.gethostbyname(host_name)
        # print("Hostname :  ", host_name)
        # print("IP : ", host_ip)
        return host_name, host_ip
    except:
        log.info("Unable to get Hostname and IP")
