import requests
import json
import yaml
import logging

log = logging.getLogger('ConfigServerClient')

class ConfigServerClient():
    _instance = None

    def __new__(cls, singleton=True, **kwargs):
        if not singleton:
            # Do not read or set the cls._instance in non singleton case
            return super(ConfigServerClient, cls).__new__(cls)
        if not (cls._instance):
            # If instance doesnt yet exist, create it
            cls._instance = super(ConfigServerClient, cls).__new__(cls)
        return cls._instance

    def __init__(self, singleton=True, **kwargs):
        self.initialize(**kwargs)

    def initialize(self, host:str='localhost', port:str='8002', username:str='ovmsapp', password:str='ovmsapp#123'):
        self.url = "http://{}:{}".format(host,port)
        self._username = username
        self._password = password
        import base64
        self._token = 'Basic {}'.format(base64.b64encode(
            bytearray('{}:{}'.format(self._username, self._password), 'utf8')).decode('utf8'))

    def get_properties(self, application, profile, file_extension:str=None):
        """
        https://github.com/spring-cloud/spring-cloud-config
        /{application}/{profile}[/{label}]
        /{application}-{profile}.yml
        /{label}/{application}-{profile}.yml
        /{application}-{profile}.properties
        /{label}/{application}-{profile}.properties
        :param application: eg - oneview-base, oneview-extsystem-interface
        :param profile: eg - local, test, sit2, ov2018
        :param file_extension: yml or properties
        :return:
        """
        if not file_extension:
            url = "{}/{}/{}".format(self.url, application, profile)
        else:
            url = "{}/{}-{}.{}".format(self.url, application, profile, file_extension)
        headers = {
            'Authorization': self._token,
            'Content-Type': 'application/json'
        }
        log.debug('Requesting properties from config server: {}'.format(url))
        response = requests.request("GET", url, headers=headers)
        return response.text

    def get_properties_as_json(self, application, profile, file_extension:str=None):
        try:
            response_text = self.get_properties(application, profile, file_extension)
            if file_extension and file_extension.upper() == 'YML':
                prop_json = yaml.safe_load(response_text)
            else:
                prop_json = json.loads(response_text)
            #dont print values as they can contain password
            log.debug('Received properties from config server'.format(prop_json))
            return prop_json
        except Exception as e:
            log.error('Error in loading properties for application:{} profile:{} file_extension:{}. Error: {}'.format(application, profile, file_extension, e))
            return None

if __name__ == '__main__':
    """
    Testing
    """
    # 4th item indicates whether to unflatten the data inside data['propertySources']['source'] data or not. Not used anymore
    TEST_PROPS = [
        ('oneview-base', 'pulak', None, True),
        # #('oneview-base', 'pulak', 'properties', True),
        # #('oneview-email', 'pulak', None,True),
        # ('oneview-ref', 'pulak', None,True),
        # #('oneview-notification-ms', 'pulak', None,True),
        # ('eureka', 'pulak', None,True),
        # #('eureka-api-gateway2', 'pulak', None,True),
        # ('application', 'pulak', None,True),
        # ('api-gateway2', 'pulak', None,False),
        # ('api-gateway2', 'pulak', 'yml',False),
        ('oneview-extsystem-interface', 'pulak', None,True),
        ('oneview-extsystem-interface', 'pulak', 'yml',True)
    ]

    for p in TEST_PROPS:
        print('Printing {} {}'.format(p[0], p[1], p[2]))
        print(json.dumps(ConfigServerClient().get_properties_as_json(p[0], p[1], p[2]), indent=2))
