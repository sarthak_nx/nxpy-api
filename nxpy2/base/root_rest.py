import logging
from flask import jsonify, Blueprint, request
from .base_settings import BaseSettings
from .response import Response

log = logging.getLogger('RootRest')

root_api = Blueprint('root', __name__)


@root_api.route('/health', methods=['GET'])
def health():
    return jsonify(Response.success(message='Connection Established. Test Passed'))


@root_api.route('/actuator/health', methods=['GET'])
def health_actuator():
    return jsonify({"status": "UP"})


@root_api.route('/actuator/info', methods=['GET'])
def info_actuator():
    # TODO make this generic
    return jsonify({"tags":{"environment":BaseSettings().get('PROFILE')},"version":"2.0","buildNumber":"001","buildTime":"2019-06-07T10:48:17Z","name":BaseSettings().get('APP_NAME')})


@root_api.route('/actuator/logfile', methods=['GET'])
def log_actuator():
    # TODO file shouldnt be locked
    tail = min(int(request.args.get('tail', 500)), 500)
    try:
        log_filename = ''
        for h in log.handlers:
            # print('Handler type {}'.format(type(h)))
            if 'FILE' in str(type(h)).upper():
                log_filename = h.baseFilename
                log.debug('Log filename: {} type: {}'.format(log_filename, type(h)))
                # print('Log filename: {} type: {}'.format(log_filename, type(h)))
                break

        if log_filename:
            with open(log_filename, 'r') as logfile:
                lines = logfile.readlines()[-tail:]
                lines = [line.rstrip('\n') for line in lines]
                data_str = '\n'.join(lines)
                from flask import Response as fr
                r = fr(response=data_str, status=200)
                r.headers["Content-Type"] = "text/plain; charset=utf-8"
                return r
        else:
            raise Exception('No log file found.')
    except Exception as e:
        log.error(e, exc_info=1)
        return jsonify(Response.failure(message=str('No logs found')))


@root_api.route('/backup', methods=['PUT'])
def backup():
    pass
    # from ov_order_mgmt_ms.client_order_service import ClientOrderService
    # ClientOrderService().backup_to_file()
    #
    # from ov_order_mgmt_ms.tranche_service import TrancheService
    # TrancheService().backup_to_file()
    # return jsonify(Response.success(message='Success'))
