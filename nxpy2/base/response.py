class Response(object):
    statusCode = None
    statusMessage = None
    data = None

    def __init__(self):
        pass

    def initialize(self, statusCode, statusMessage=None, data=None):
        self.statusCode = statusCode
        self.statusMessage = statusMessage
        self.data = data
        return self.__dict__

    def get_dict(self):
        return self.__dict__

    @staticmethod
    def success(message=None, data=None):
        return Response().initialize(0, message, data)

    @staticmethod
    def failure(message=None, data=None):
        return Response().initialize(-1, message, data)
