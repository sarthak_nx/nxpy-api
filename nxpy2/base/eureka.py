import asyncio
from threading import Thread
import logging
import json
from .base_settings import BaseSettings

log = logging.getLogger('eureka')


# TODO ideally should be passed from outside instead of being looked up from settings here
def get_eureka_config():
    # This needs to be an IP accessible by anyone that
    # may want to discover, connect and/or use your service.
    app_name = BaseSettings().get('APP_NAME').upper()
    ip_addr = BaseSettings().get('IP', '127.0.0.1')
    # .util.get_host_name_IP()[1]) #not working correctly, probably because of VPN
    port = BaseSettings().get('PORT')
    my_eureka_url = BaseSettings().get('EUREKA_SERVICE_URL', 'http://localhost:8003')
    status_page_url = "http://{}:{}/oneview/rest/{}/actuator/info".format(ip_addr, port,
                                                                          BaseSettings().get(
                                                                              'APP_URL_PREFIX'))
    health_check_url = "http://{}:{}/oneview/rest/{}/actuator/health".format(ip_addr, port,
                                                                             BaseSettings().get(
                                                                                 'APP_URL_PREFIX'))
    return dict(app_name=app_name, port=port, ip_addr=ip_addr, status_page_url=status_page_url,
                health_check_url=health_check_url, eureka_url=my_eureka_url)


def register_and_renew_eureka():
    # reference https://hackernoon.com/threaded-asynchronous-magic-and-how-to-wield-it-bba9ed602c32
    # Create the new loop and worker thread
    worker_loop = asyncio.new_event_loop()
    worker = Thread(target=register_and_renew_task, args=(worker_loop,))
    # Start the thread
    worker.start()


def register_and_renew_task(loop):
    # For 'eureka' object to be shared, register and renew has to be on same thread
    asyncio.set_event_loop(loop)
    eureka = loop.run_until_complete(register())
    if not eureka: return
    loop.run_until_complete(async_continuously_renew())


#
# async def print_apps(eureka):
#     apps = await eureka.get_apps()
#     print('{} applications '.format(len(apps['applications'])))

async def register():
    from .wasp_eureka_client import EurekaClient
    # from wasp_eureka.client import EurekaClient
    loop = asyncio.get_event_loop()
    log.info('Initializing eureka with: {}'.format(json.dumps(get_eureka_config())))
    # print('Initializing eureka with: {}'.format(json.dumps(get_eureka_config())))
    eureka = EurekaClient(**get_eureka_config(), loop=loop)
    metadata = {
        # "instanceId": "oneview-auth:428a7d798952e7e7c04529a4123c1db9",
        "contextPath": "/oneview/rest/{}".format(BaseSettings().get('APP_URL_PREFIX')),
        "management.context-path": "/oneview/rest/{}/actuator".format(
            BaseSettings().get('APP_URL_PREFIX')),
        "management.port": get_eureka_config()['port']
    }

    try:
        result = await eureka.register(
            lease_duration=BaseSettings().get('RENEW_EUREKA_LEASE_SEC', 30),
            lease_renewal_interval=BaseSettings().get(
                'RENEW_EUREKA_LEASE_SEC', 30), metadata=metadata)
        # print('Registered Eureka Client as :{}'.format(eureka.instance_id))
        log.info('Registered Eureka Client as :{}'.format(eureka.instance_id))
    except Exception as e:
        log.error('Unable to register app {} in Eureka. {}'.format(
            get_eureka_config()['app_name'].upper(), e))
        eureka = None

    global EUREKA_APP
    EUREKA_APP = eureka
    return eureka


async def async_continuously_renew():
    eureka = EUREKA_APP
    app_name = get_eureka_config()['app_name'].upper()
    # print('Entering renew loop')
    # log.info('Entering renew loop')
    while True:
        await asyncio.sleep(BaseSettings().get('RENEW_EUREKA_LEASE_SEC', 30))
        try:
            # log.info('Renewing..........')
            await eureka.renew()
        except Exception as e:
            # print(e)
            log.error('Unable to renew Eureka lease for app {}. {}'.format(app_name, e))
        try:
            apps = await eureka.get_app(app_name)
            # log.debug('{} instances of my app running'.format(
            #     len(apps.get('application', dict()).get('instance', list()))))
        except Exception as e:
            log.debug('No instances found in Eureka for app {}. {}'.format(app_name, e))
