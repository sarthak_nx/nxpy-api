# Settings for environment
import json
import logging
import logging.config
from inspect import currentframe, getframeinfo
from pathlib import Path


class BaseSettings():
    _instance = None

    def __new__(cls, singleton=True):
        if not singleton:
            # Do not read or set the cls._instance in non singleton case
            return super(BaseSettings, cls).__new__(cls)
        if not (cls._instance):
            # If instance doesnt yet exist, create it
            cls._instance = super(BaseSettings, cls).__new__(cls)
        return cls._instance

    def __init__(self, singleton=True):
        pass

    def __str__(self):
        return self.toJSON()

    def __repr__(self):
        return self.toJSON()

    def fromDict(self, request_dict):
        import copy
        if request_dict:
            copy_dict = copy.deepcopy(request_dict)
            self.update(**copy_dict)
        return self

    def toDict(self):
        return self.__dict__

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True)

    def update(self, **kwargs):
        self.__dict__.update(**kwargs)
        return self

    def get(self, property: str, default=None):
        value = self.toDict().get(property, default)
        # take care of situation when value is there in settings but null
        return value if value is not None else default

    def set(self, property: str, value):
        return self.override(property, value, force=True)

    def override(self, property: str, value, force=False):
        """
        :param property:
        :param value:
        :param force: if True, then force override else override only if its missing
        :return:
        """
        if property in self.toDict() and self.toDict()[property]:
            if force:
                self.update(**{property: value})
            else:
                # do nothing
                pass
        else:
            self.update(**{property: value})
        return self.get(property)

    def setup(self, root_dir_path: str, oneview_home_dir_path: str = None,
              config_dir_path: str = None, data_dir_path: str = None, log_dir_path: str = None, override_profile: str = None,
              app_name: str = None, app_url_prefix: str = None):
        self.ROOT_DIR = root_dir_path
        print('ROOT_DIR: {}'.format(self.ROOT_DIR))

        # default to {root}
        self.ONEVIEW_HOME = root_dir_path if not oneview_home_dir_path else oneview_home_dir_path
        print('ONEVIEW_HOME: {}'.format(self.ONEVIEW_HOME))

        # default to {root}/config
        self.CONFIG_DIR = self.append_path('config') if not config_dir_path else config_dir_path
        print('CONFIG_DIR: {}'.format(self.CONFIG_DIR))

        # default to {root}/data
        self.DATA_DIR = self.append_path('data') if not data_dir_path else data_dir_path
        print('DATA_DIR: {}'.format(self.DATA_DIR))

        # default to {oneview_home}/logs
        self.LOG_DIR = self.append_path('logs', base_path=self.ONEVIEW_HOME) if not log_dir_path else log_dir_path
        print('LOG_DIR: {}'.format(self.LOG_DIR))

        self.PROFILE = BaseSettings.get_profile(
            self.CONFIG_DIR) if not override_profile else override_profile
        print('PROFILE: {}'.format(self.PROFILE))

        self.APP_NAME = app_name
        print('APP_NAME: {}'.format(self.APP_NAME))

        self.APP_URL_PREFIX = app_url_prefix
        print('APP_URL_PREFIX: {}'.format(self.APP_URL_PREFIX))

        self.API_GW_PUBLIC_KEY = self.append_path('public_key.pub', base_path=self.CONFIG_DIR)
        print('API_GW_PUBLIC_KEY: {}'.format(self.API_GW_PUBLIC_KEY))

    def initialize_from_profile(self, file_prefix: str = '', file_extension='json'):
        """
        this expects that setup() method has been ran before and variables like ROOT_DIR, CONFIG_DIR and PROFILE are available already
        :param file_prefix: to be appened before file name when reading from {root}/config/* directory
        :param file_extension: used for loading property file
        :return:
        """
        # loads {prefix}{profile}.{extension) as the settings file
        filename = '{}{}.{}'.format(file_prefix, self.PROFILE, file_extension)
        settings_file_path = self.append_path(filename, self.CONFIG_DIR)
        return self.initialize_from_file(settings_file_path)

    def initialize_from_file(self, settings_file_path: str):
        """
        this expects that setup() method has been ran before and variables like ROOT_DIR, CONFIG_DIR and PROFILE are available already
        """
        settings_dict = BaseSettings.get_settings_file(settings_file_path)
        return self.initialize_from_dict(settings_dict=settings_dict)

    def initialize_from_dict(self, settings_dict: dict):
        """
        this expects that setup() method has been ran before and variables like ROOT_DIR, CONFIG_DIR and PROFILE are available already
        """
        self.fromDict(settings_dict)
        return self

    def initialize_from_args(self, parser):
        """
        this expects that setup() method has been ran before and variables like ROOT_DIR, CONFIG_DIR and PROFILE are available already
        :param parser: parser = argparse.ArgumentParser()
        :return:
        """
        # first build a dictionary and then settings object out of it
        settings_dict = BaseSettings.build_arg_dict(parser)
        return self.initialize_from_dict(settings_dict)

    def append_path(self, suffix_path: str, base_path: str = None):
        """
        :param suffix_path: path which needs to be appened to base_path
        :param base_path: if not given, ROOT_DIR will be used
        :return:
        """
        if not base_path:
            base_path = Path(self.ROOT_DIR)
        else:
            base_path = Path(base_path)
        joined_path = str(base_path.joinpath(suffix_path).resolve())
        self.create_path_if_not_exists(joined_path)
        return joined_path

    def create_path_if_not_exists(self, path: str):

        if Path(path).exists():
            pass
        else:
            Path(path).mkdir(parents=True, exist_ok=True)

    def find_replace_root(self, dir_path: str, default: str = '', replacement_string: str = None,
                          replacement_path: str = None):
        """
        :param dir_path: path in which root is to be replaced. like {root}/test_data
        :param default: like 'test_data', 'temp' . Used only if dir_path not provided
        :return:
        """
        replacement_string = '{root}' if not replacement_string else replacement_string
        replacement_path = self.ROOT_DIR if not replacement_path else replacement_path
        if not dir_path:
            # if no dir_path, join replacement_path and default path
            dir_path = self.append_path(default, replacement_path)
        elif replacement_string in dir_path:
            # replace variable
            dir_path = dir_path.replace(replacement_string, replacement_path)
        return dir_path

    def _append_log_dir_path(self, log_config: dict):
        # If filehandler is {root}/...., then root is replaced by root_dir_path
        for k, v in log_config['handlers'].items():
            if 'filename' in v and 'File' in v['class']:
                # v['filename'] = str(v['filename']).replace('{log_dir}', self.LOG_DIR)
                v['filename'] = self.find_replace_root(v['filename'],
                                                       replacement_string='{log_dir}',
                                                       replacement_path=self.LOG_DIR)
        return log_config

    def configure_logs(self, logconfig_file_path='', default_level=logging.INFO):
        # Json config file should be as per this standard - https://docs.python.org/3/library/logging.config.html#logging.config.dictConfig
        if not logconfig_file_path:
            # default to {root}/config/logging.json
            logconfig_file_path = self.append_path('logging.json', base_path=self.CONFIG_DIR)

        print('Initializing log config from: {}'.format(logconfig_file_path))
        try:
            with open(logconfig_file_path, 'r') as fp:
                log_config = json.load(fp)
                log_config = self._append_log_dir_path(log_config)
                logging.config.dictConfig(log_config)
        except Exception as e:
            print('Error in initializing logs, using default settings. Error: {}'.format(e))
            logging.basicConfig(level=default_level)

    @staticmethod
    def get_settings_file(settings_file_path: str):
        # Load {prefix}{profile}.{extension) as the settings file
        print('Initializing settings from: {}'.format(settings_file_path))
        settings_file = Path(settings_file_path)
        with open(str(settings_file.resolve()), 'r') as fp:
            if settings_file.suffix.upper() in ['.YML', '.YAML']:
                import yaml
                settings_dict = yaml.safe_load(fp)
            else:
                settings_dict = json.load(fp)
        return settings_dict

    @staticmethod
    def get_profile(profile_dir_path: str):
        # Load profile.txt from the given path, defaulted to {root}/config/ and read the first line as profile name
        profile_dir = Path(profile_dir_path)
        profile_filepath = profile_dir.joinpath('profile.txt')
        with open(str(profile_filepath.resolve()), 'r') as fp:
            profile = fp.readline()
        return profile

    @staticmethod
    def get_parent_dir(path: str = None):
        # return parent of parent of current file
        if not path:
            path = getframeinfo(currentframe()).filename
        parent_dir = Path(path).resolve().parent
        return parent_dir.resolve()

    @staticmethod
    def build_arg_dict(parser):
        args, unknown = parser.parse_known_args()
        # if unknown:
        #     print('Unknown arguments: {}'.format(unknown))
        # print('Arguments:')
        settings_dict = dict()
        for arg in vars(args):
            # print('{}={}'.format(arg, getattr(args, arg)))
            settings_dict[arg] = getattr(args, arg)
            # try to convert json strings to json objects
            if type(settings_dict[arg]) is str:
                try:
                    jsonstr = json.loads(settings_dict[arg])
                    settings_dict[arg] = jsonstr
                except:
                    pass
        return settings_dict
