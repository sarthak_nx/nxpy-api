from nxpy2.cail.util import result_util as r, request_util as u
from nxpy2.cail.cail_client.cail_context_manager import CAILContextMgr
from nxpy2.cail.request_dto.request import CalcTypeEnum, CalcRequest
# from nxpy2.cail.request_dto.calc_types.exposure_calc import ExposureRequest
# from nxpy2.cail.request_dto.calc_types.sensitivity_calc import SensitivityRequest
# from nxpy2.cail.request_dto.calc_types.pricing_calc import PricingRequest

import logging
import timeit

log = logging.getLogger('CAILClient')

try:
    from nxpy import integration
except ImportError as error:
    import sys

    log.exception("nxpy.integration is not currently available on this platform.")
    log.exception(error)
    raise SystemExit()


# TODO move to result util?
def summarize_results(calc_result, printMsg=True):
    if not calc_result: return
    summary = dict()
    if printMsg: print('____________________________________________')
    if printMsg: print('_____________RESULT SUMMARY_________________')

    summary['calcType'] = calc_result['calcType'] if 'calcType' in calc_result else 'Pricing'
    summary['marketName'] = calc_result['marketName'] if 'marketName' in calc_result else ''
    summary['marketDate'] = calc_result['marketDate'] if 'marketDate' in calc_result else ''
    if printMsg: print('Market Name: {}'.format(calc_result['marketName']))
    if printMsg: print('Market Date: {}'.format(calc_result['marketDate']))
    if not calc_result['unitCalcResults']:
        if printMsg: print('No results available')
    if 'calculationTime' in calc_result:
        summary['calculationTime'] = calc_result['calculationTime']
        if printMsg: print('Calculation Time (s): {}'.format(calc_result['calculationTime']))
    if printMsg: print('# of total instances : {}'.format(str(len(calc_result['unitCalcResults']))))
    summary['totalInstanceCount'] = len(calc_result['unitCalcResults'])
    error_instances = [u['ref'] for u in calc_result['unitCalcResults'] if u['error']]
    if error_instances:
        if printMsg: print('# of instances with error: {}'.format(str(len(error_instances))))
        if printMsg: print('Error instances: {}'.format(str(error_instances)))
        summary['errorInstanceCount'] = len(error_instances)
        summary['errorInstances'] = error_instances
    non_pv_instances = [u['ref'] for u in calc_result['unitCalcResults'] if
                        not u['error'] and not u['pv']]
    if non_pv_instances:
        if printMsg: print(
            '# of instances without error and without pv: {}'.format(str(len(non_pv_instances))))
        if printMsg: print('Non PV instances: {}'.format(str(non_pv_instances)))
        summary['nonPvInstanceCount'] = len(non_pv_instances)
        summary['nonPvInstances'] = non_pv_instances
    if printMsg: print('____________________________________________')
    return summary

def _parse_request(request_payload:dict):
    #TODO to be finished
    #request_payload is oneview like json format
    #figure out calculuationType
    #make use of request_dto.request.CalcRequest object
    request_ref = request_payload['requestRef'] if 'requestRef' in request_payload else None
    market_selection = request_payload['marketSelection'] if 'marketSelection' in request_payload else None
    market_policy_name = request_payload['marketPolicyName'] if 'marketPolicyName' in request_payload else None
    pricing_policy_name = request_payload['pricingPolicyName'] if 'pricingPolicyName' in request_payload else None
    instance_selection = request_payload['tradeSelection'] if 'tradeSelection' in request_payload else None
    calc_measure_selection = request_payload['calcMeasureSelection'] if 'calcMeasureSelection' in request_payload else None
    recording = request_payload['recording'] if 'recording' in request_payload else False
    unpack = request_payload['unpack'] if 'unpack' in request_payload else False

    dict(context_request_ref = request_ref, recording = recording, unpack = unpack,
                                            market_selection = market_selection, market_policy_name=market_policy_name,
                                            pricing_policy_name=pricing_policy_name, terms_instances=instance_selection, calc_measure_selection=calc_measure_selection)

#TODO to be filled
def run_calculation(context_manager, request:CalcRequest, context_request_ref=None, print_results=False):
    context_manager = context_manager if context_manager is not None else CAILContextMgr()
    #figure out which calculation type it is
    #_parse_request
    #call appropriate run_* function and return its output
    pass

# TODO gracefully handle server mode and non server mode calculations
def initialize_cail_client(context_manager=None, calculation_type=None,
                           market_selection=None,
                           market_policy_name='Standard', pricing_policy_name='Standard',
                           market_report_file_names = None,
                           recording=False,
                           unpack=False, debug=False, trace=False, profile=False,
                           markets_path='', mdk_schema_path='', validate_market_data=False,
                           context_request_ref='', fixings_path = '', read_fixings = False):
    cail_client_parameters = {}

    if not context_manager: context_manager = CAILContextMgr()

    log.info('Initializing {} calculation...'.format(calculation_type))
    cail_client_parameters['content_path'] = context_manager._content_path
    cail_client_parameters['repository'] = context_manager._repository
    cail_client_parameters['cail_context'] = context_manager.create_cail_context(
        key=context_request_ref, recording=recording,
        unpack=unpack,
        mdk_schema_path=mdk_schema_path)
    cail_client_parameters['cail_context_key'] = cail_client_parameters['cail_context'].key
    log.info('Built calculation context..')

    # Build calculation parameters
    cail_client_parameters['parameters'] = u.build_calc_parameters(
        content_path=cail_client_parameters['content_path'],
        repository=cail_client_parameters['repository'], debug=debug, trace=trace,
        profile=profile, pricing_policy_name=pricing_policy_name, market_reports = market_report_file_names,
        market_policy_name=market_policy_name, fixings_path = fixings_path, read_fixings=read_fixings)

    if 'marketName' in market_selection:

        cail_client_parameters['markets'] = u.build_market_instance_from_file(
            content_path=cail_client_parameters['content_path'], markets_path=markets_path,
            market_name=market_selection['marketName'])
    else:
        log.error(
            'Invalid market name. Unable to build calculation parameters. Aborting calculation')
        return None

    if not cail_client_parameters['parameters'] or cail_client_parameters['markets'] is None:
        log.error('Unable to build calculation parameters. Aborting calculation')
        return None

    log.debug('Built calculation parameters..')

    # Validate market_file_name
    if validate_market_data: cail_client_parameters['cail_context'].validate_market(
        cail_client_parameters['markets'])

    return cail_client_parameters

def run_pricing_calculation(context_manager=None, cail_client_parameters = None, market_selection=None,
                            calc_measure_selection=None, term_file_names=[],
                            terms_instances=[],
                            pricing_policy_name='Standard', market_policy_name='Standard',
                            recording=False, unpack=False,
                            debug=False, trace=False, profile=False,
                            severity='fatal', markets_path='', terms_path='', mdk_schema_path='',
                            validate_market_data=False, print_results=False,
                            context_request_ref=None, dispose_context=False):
    calculation_type = 'Pricing'
    context_manager = context_manager if context_manager is not None else CAILContextMgr()

    if cail_client_parameters:
        pass
    else:
        cail_client_parameters = initialize_cail_client(context_manager=context_manager,
                                                        calculation_type=calculation_type,
                                                        market_selection=market_selection,
                                                        market_policy_name=market_policy_name,
                                                        pricing_policy_name=pricing_policy_name,
                                                        recording=recording, unpack=unpack,
                                                        debug=debug, trace=trace, profile=profile,
                                                        markets_path=markets_path,
                                                        mdk_schema_path=mdk_schema_path,
                                                        validate_market_data=validate_market_data,
                                                        context_request_ref=context_request_ref)

    if cail_client_parameters:

        instances = u.build_trade_instances(content_path=cail_client_parameters['content_path'],
                                            repository=cail_client_parameters['repository'],
                                            calculation_type=calculation_type,
                                            terms_path=terms_path,
                                            term_file_names=term_file_names,
                                            terms_instances=terms_instances)

        if instances:
            # Run calculation

            if calc_measure_selection and 'resultScopeList' in calc_measure_selection and \
                    calc_measure_selection['resultScopeList']:
                calc_request = ','.join(calc_measure_selection['resultScopeList'])
            else:
                calc_request = 'all'

            (results, calc_time, diff_list) = context_manager.run_calculation(
                context_key=cail_client_parameters['cail_context_key'],
                parameters=cail_client_parameters['parameters'],
                instances=instances,
                market=cail_client_parameters['markets'][0],
                calc_request=calc_request)
            log.info('Finished calculation. Total time: {} sec'.format(calc_time))

            # Shut down the context.
            if dispose_context:
                context_manager.evict(cail_client_parameters['cail_context_key'])
            # Process Results
            pricing_calc_result = r.process_pricing_results(results=results, severity=severity,
                                                            print_results=print_results)
            if pricing_calc_result: pricing_calc_result['calculationTime'] = calc_time
            log.info('Finished processing pricing results..')

            if profile: r.print_cail_profiling_data(results.profiling_data)
            return pricing_calc_result
        else:
            return []
    else:
        return []


def run_sensitivity_calculation(context_manager=None, cail_client_parameters = None, market_selection=None,
                                calc_measure_selection=None, term_file_names=[],
                                terms_instances=[], market_report_file_names=[],
                                pricing_policy_name='Standard', market_policy_name='Standard',
                                recording=False, unpack=False,
                                debug=False, trace=False, profile=False,
                                severity='fatal', markets_path='', terms_path='',
                                mdk_schema_path='',
                                validate_market_data=False, print_results=False,
                                context_request_ref=None, dispose_context=False):
    calculation_type = 'Sensitivity'
    context_manager = context_manager if context_manager is not None else CAILContextMgr()
    if cail_client_parameters:
        pass
    else:
        cail_client_parameters = initialize_cail_client(context_manager=context_manager,
                                                        calculation_type=calculation_type,
                                                        market_selection=market_selection,
                                                        market_policy_name=market_policy_name,
                                                        recording=recording, unpack=unpack,
                                                        debug=debug, trace=trace, profile=profile,
                                                        markets_path=markets_path,
                                                        mdk_schema_path=mdk_schema_path,
                                                        validate_market_data=validate_market_data,
                                                        context_request_ref=context_request_ref)

    if cail_client_parameters:

        instances = u.build_trade_instances(content_path=cail_client_parameters['content_path'],
                                            repository=cail_client_parameters['repository'],
                                            calculation_type=calculation_type,
                                            terms_path=terms_path,
                                            term_file_names=term_file_names,
                                            terms_instances=terms_instances)

        if instances:

            if not cail_client_parameters['parameters'].market_reports:
                log.error('No market reports in calculation. Aborting calculation')
                return None

            if calc_measure_selection and 'resultScopeList' in calc_measure_selection and \
                    calc_measure_selection['resultScopeList']:
                calc_request = ','.join(calc_measure_selection['resultScopeList'])
            else:
                calc_request = 'all'

            # Run calculation
            (results, calc_time, diff_list) = context_manager.run_calculation(
                context_key=cail_client_parameters['cail_context_key'],
                parameters=cail_client_parameters['parameters'],
                instances=instances,
                market=cail_client_parameters['markets'][0],
                calc_request=calc_request)
            log.info('Finished calculation. Total time: {} sec'.format(calc_time))

            # Shut down the context.
            if dispose_context:
                context_manager.evict(cail_client_parameters['cail_context_key'])

            # Process Results
            sensitivity_calc_result = r.process_sensitivity_results(results=results, parameters=
            cail_client_parameters['parameters'], severity=severity,
                                                                    print_results=print_results)
            if sensitivity_calc_result: sensitivity_calc_result['calculationTime'] = calc_time
            log.info('Finished processing pricing results..')

            if profile: r.print_cail_profiling_data(results.profiling_data)

            return sensitivity_calc_result
        else:
            return None
    else:
        return None


def run_exposure_calculation(context_manager=None, cail_client_parameters = None, market_selection=None,
                             exposure_only=False, term_file_names=[],
                             terms_instances=[],
                             hybridspec='', hybridconfig='HYBRIDMODELBUILDER.DEFAULT',
                             obsdates='ObservationDates',
                             modeldates='ModelDates', numpaths=10,
                             pricing_policy_name='Standard', market_policy_name='Standard',
                             recording=False, unpack=False,
                             debug=False, trace=False, profile=False,
                             severity='fatal', markets_path='', terms_path='', mdk_schema_path='',
                             validate_market_data=False, print_results=False, plot_results=False,
                             context_request_ref=None, dispose_context=False):
    calculation_type = 'Exposure'
    context_manager = context_manager if context_manager is not None else CAILContextMgr()
    if cail_client_parameters:
        pass
    else:
        cail_client_parameters = initialize_cail_client(context_manager=context_manager,
                                                        calculation_type=calculation_type,
                                                        market_selection=market_selection,
                                                        market_policy_name=market_policy_name,
                                                        pricing_policy_name=pricing_policy_name,

                                                        recording=recording, unpack=unpack,
                                                        debug=debug, trace=trace, profile=profile,
                                                        markets_path=markets_path,
                                                        mdk_schema_path=mdk_schema_path,
                                                        validate_market_data=validate_market_data,
                                                        context_request_ref=context_request_ref)

    if cail_client_parameters:

        instances = u.build_trade_instances(content_path=cail_client_parameters['content_path'],
                                            repository=cail_client_parameters['repository'],
                                            calculation_type=calculation_type,
                                            terms_path=terms_path,
                                            term_file_names=term_file_names,
                                            terms_instances=terms_instances)

        if instances:
            # Configure the hybrid model and add to parameters

            cail_client_parameters['parameters'].markets = cail_client_parameters['markets']
            cail_client_parameters['parameters'].instances = instances
            parameters = u.configure_hybrid_model(
                content_path=cail_client_parameters['content_path'],
                repository=cail_client_parameters['repository'],
                parameters=cail_client_parameters['parameters'],
                hybridspec=hybridspec, hybridconfig=hybridconfig, numpaths=numpaths,
                obsdates=obsdates, modeldates=modeldates, trace=trace)
            log.info('Built hybrid model..')

            # termsStr = ",".join(term_file_names)
            calc_request = "ExposureOnly" if exposure_only else 'PV,Exposure,Exposure.Discount Factors,Exposure.Counterparty Hazard Rates,Exposure.Self Hazard Rates,Exposure.Collateral Asset Values,Exposure.FX Rates,Exposure.Exact Payment Distribution,Curves'

            # Run calculation
            (results, calc_time, diff_list) = context_manager.run_calculation(
                context_key=cail_client_parameters['cail_context_key'], parameters=parameters,
                instances=instances,
                market=cail_client_parameters['markets'][0],
                calc_request=calc_request)
            log.info('Finished calculation. Total time: {} sec'.format(calc_time))

            # Shut down the context.
            if dispose_context:
                context_manager.evict(cail_client_parameters['cail_context_key'])

            exposure_calc_result = r.process_exposure_results(results=results,
                                                              parameters=cail_client_parameters[
                                                                  'parameters'], severity=severity,
                                                              print_results=print_results)
            log.info('Finished processing exposure results..')

            obs_dates_output = exposure_calc_result[
                'obsDates'] if 'obsDates' in exposure_calc_result else None
            aggregate_exposure = exposure_calc_result[
                'aggregateExposure'] if 'aggregateExposure' in exposure_calc_result else None
            if plot_results and obs_dates_output is not None and aggregate_exposure is not None:
                try:
                    import matplotlib.pyplot as plt
                    plt.plot(obs_dates_output, aggregate_exposure)
                    plt.show()
                except:
                    log.error(
                        'Plotting results requires matplotlib library installed in the system.')

            # Remove matrix result as it is not json serializable

            exposure_calc_result['aggregateExposure'] = exposure_calc_result[
                'aggregateExposure'].tolist()

            # exposure_calc_result.pop('obsDates', None)
            # exposure_calc_result.pop('aggregateExposure', None)

            if profile: r.print_cail_profiling_data(results.profiling_data)

            return exposure_calc_result
        else:
            return None
    else:
        return None


def run_mde_calculation(context_manager=None, cail_client_parameters = None, market_selection=None,
                        calc_measure_selection=None, mde_file_names=None,
                        mde_instances=None, market_policy_name='Standard',
                        recording=False, unpack=False, debug=False, trace=False, profile=False,
                        severity='fatal', markets_path='', mde_path='', mdk_schema_path='',
                        validate_market_data=False,
                        print_results=False, context_request_ref=None, dispose_context=False):
    calculation_type = 'MDE'
    context_manager = context_manager if context_manager is not None else CAILContextMgr()
    if cail_client_parameters:
        pass
    else:
        cail_client_parameters = initialize_cail_client(context_manager=context_manager,
                                                    calculation_type=calculation_type,
                                                    market_selection=market_selection,
                                                    market_policy_name=market_policy_name,

                                                    recording=recording, unpack=unpack,
                                                    debug=debug, trace=trace, profile=profile,
                                                    markets_path=markets_path,
                                                    mdk_schema_path=mdk_schema_path,
                                                    validate_market_data=validate_market_data,
                                                    context_request_ref=context_request_ref)

    if cail_client_parameters:

        instances = u.build_mde_instances(content_path=cail_client_parameters['content_path'],
                                          repository=cail_client_parameters['repository'],
                                          calculation_type=calculation_type, mde_path=mde_path,
                                          mde_file_names=mde_file_names,
                                          mde_instances=mde_instances)

        if instances:
            # Run calculation
            if calc_measure_selection and 'resultScopeList' in calc_measure_selection and \
                    calc_measure_selection['resultScopeList']:
                calc_request = ','.join(calc_measure_selection['resultScopeList'])
            else:
                calc_request = 'all'

            (results, calc_time, diff_list) = context_manager.run_calculation(
                context_key=cail_client_parameters['cail_context_key'],
                parameters=cail_client_parameters['parameters'],
                instances=instances,
                market=cail_client_parameters['markets'][0],
                calc_request=calc_request)
            log.info('Finished calculation. Total time: {} sec'.format(calc_time))

            # Process Results
            mde_calc_result = r.process_market_data_element_results(results=results, cail_context=
            context_manager.get_cail_context(cail_client_parameters['cail_context_key']),
                                                                    severity=severity,
                                                                    print_results=print_results)
            if mde_calc_result: mde_calc_result['calculationTime'] = calc_time
            log.info('Finished processing MDE results..')

            # Shut down the context.
            if dispose_context:
                context_manager.evict(cail_client_parameters['cail_context_key'])

            if profile: r.print_cail_profiling_data(results.profiling_data)

            return mde_calc_result
        else:
            return None
    else:
        return None

def run_model_calculation(context_manager=None, cail_client_parameters = None, market_selection=None,
                        calc_measure_selection=None, model_file_names=None,
                        model_instances=None, market_policy_name='Standard',
                        recording=False, unpack=False, debug=False, trace=False, profile=False,
                        severity='fatal', markets_path='', model_path='', mdk_schema_path='',
                        validate_market_data=False,
                        print_results=False, context_request_ref=None, dispose_context=False):
    calculation_type = 'MODEL'
    context_manager = context_manager if context_manager is not None else CAILContextMgr()
    if cail_client_parameters:
        pass
    else:
        cail_client_parameters = initialize_cail_client(context_manager=context_manager,
                                                    calculation_type=calculation_type,
                                                    market_selection=market_selection,
                                                    market_policy_name=market_policy_name,

                                                    recording=recording, unpack=unpack,
                                                    debug=debug, trace=trace, profile=profile,
                                                    markets_path=markets_path,
                                                    mdk_schema_path=mdk_schema_path,
                                                    validate_market_data=validate_market_data,
                                                    context_request_ref=context_request_ref)

    if cail_client_parameters:

        instances = u.build_model_instances(content_path=cail_client_parameters['content_path'],
                                          repository=cail_client_parameters['repository'],
                                          calculation_type=calculation_type,
                                          model_file_names=model_file_names,
                                          model_instances=model_instances)

        if instances:
            # Run calculation
            if calc_measure_selection and 'resultScopeList' in calc_measure_selection and \
                    calc_measure_selection['resultScopeList']:
                calc_request = ','.join(calc_measure_selection['resultScopeList'])
            else:
                calc_request = 'all'

            (results, calc_time, diff_list) = context_manager.run_calculation(
                context_key=cail_client_parameters['cail_context_key'],
                parameters=cail_client_parameters['parameters'],
                instances=instances,
                market=cail_client_parameters['markets'][0],
                calc_request=calc_request)
            log.info('Finished calculation. Total time: {} sec'.format(calc_time))

            # Process Results
            mde_calc_result = r.process_model_calibration_results(results=results, cail_context=
            context_manager.get_cail_context(cail_client_parameters['cail_context_key']),
                                                                    severity=severity,
                                                                    print_results=print_results)
            if mde_calc_result: mde_calc_result['calculationTime'] = calc_time
            log.info('Finished processing MDE results..')

            # Shut down the context.
            if dispose_context:
                context_manager.evict(cail_client_parameters['cail_context_key'])

            if profile: r.print_cail_profiling_data(results.profiling_data)

            return mde_calc_result
        else:
            return None
    else:
        return None