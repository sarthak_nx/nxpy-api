import logging
import uuid
from datetime import datetime
from nxpy2.cail.cail_client.cail_context import CAILContext

log = logging.getLogger('CAILContextMgr')

try:
    from nxpy import integration
except ImportError as error:
    import sys

    log.exception("nxpy.integration is not currently available on this platform.")
    log.exception(error)
    raise SystemExit()


class CAILContextMgr(object):
    _content_path = None
    _repository = None
    _instance = None
    _recording_path = ''
    # Map of string key vs CAILContext
    _context_registry = dict()
    _calculation_log = list()

    def __new__(cls, singleton=True):
        if not singleton:
            # Do not read or set the cls._instance in non singleton case
            cls._context_registry = dict()
            cls._calculation_log = list()
            return super(CAILContextMgr, cls).__new__(cls)
        if not (cls._instance):
            # If instance doesnt yet exist, create it
            cls._context_registry = dict()
            cls._calculation_log = list()
            cls._instance = super(CAILContextMgr, cls).__new__(cls)
        return cls._instance

    def __init__(self, singleton=True):
        pass

    def initialize(self, content_path, recording_path=''):
        log.debug('Initializing repository')

        try:
            self._content_path = integration.find_data_directory(content_path)
            self._repository = integration.LocalRepository(self._content_path)
            self._recording_path = recording_path
            self.initialize_data_paths()
            log.info('Repository initialized')
            return True
        except Exception as error:
            log.error('Failed to initialize repository')
            log.exception(error)
            return None

    def initialize_data_paths(self):
        pass

    def reset_all(self):
        for key, cail_context in self._context_registry.items():
            cail_context.reset_context()

    def reset_key(self, key):
        if key in self._context_registry:
            self._context_registry[key].reset_context()

    #TODO: Add generate recording function using explicit write_nxt function

    def evict_all(self):
        for key, cail_context in self._context_registry.items():
            self._context_registry[key].dispose()
        self._context_registry = dict()

    def evict(self, key):
        if key in self._context_registry:
            self._context_registry[key].dispose()
            del self._context_registry[key]

    def create_default_context(self):
        return self.create_cail_context(CAILContextMgr.get_default_context_key())

    def create_cail_context(self, key, recording=False, unpack=False,
                            mdk_schema_path=''):
        if not key in self._context_registry:
            cail_context = CAILContext(key, self._repository, self._recording_path, recording,
                                       unpack,
                                       mdk_schema_path)
            key = cail_context.key  # after it has been defaulted to uuid incase not explicitly provided
            self._register(key, cail_context)
            return cail_context
        else:
            return self._context_registry[key]

    def run_calculation(self, parameters, instances, market, calc_request='PV', context_key=None,
                        request_ref=None,
                        context_resolution_function=''):
        # TODO also return the used context key so that callers know what context the request was executed on
        if not parameters or market is None or instances is None:
            print('Unable to build calculation parameters. Aborting calculation')
            return None
        if request_ref is None: request_ref = str(uuid.uuid1())
        target_context_key = context_key if context_key else self._resolve_context(market)
        # print('Routing calculation {} to context: {}, #trades: {}, market: {}_{}'.format(request_ref, target_context_key,
        #                                                                                  len(instances), market.name,
        #                                                                                  str(market.now_time)))

        # Run calculation in a context
        (results, calc_time, diff_list) = self.get_cail_context(target_context_key).run_calculation(
            request_ref=request_ref, parameters=parameters, instances=instances, market=market,
            calc_request=calc_request, callback=None)

        calc_entry = dict(request_ref=request_ref, context_key=target_context_key,
                          calc_time=calc_time,
                          num_trades=len(instances),
                          market='{}_{}'.format(market.name, str(market.now_time)),
                          end_time=str(datetime.now().time()),
                          new_object_count=len(diff_list))
        self._calculation_log.append(calc_entry)
        return (results, calc_time, diff_list)

    def _register(self, key, cail_context):

        if bool(key) == False:
            self._context_registry[self.get_default_context_key()] = cail_context
            log.info('Registering default context')
        elif not key in self._context_registry:
            self._context_registry[key] = cail_context
            log.info('Registering new context')
        else:
            log.info('{} already exists.'.format(key))

    def get_cail_context(self, key):
        if key in self._context_registry:
            return self._context_registry[key]
        else:
            return None

    def get_context_application_objects(self, context_key):
        if context_key in self._context_registry:
            cail_context = self._context_registry[context_key]
            return cail_context.get_context_objects()
        else:
            return None

    def get_context_summary(self, context_key):

        if context_key in self._context_registry:
            cail_context = self._context_registry[context_key]
            return cail_context.get_context_summary()
        else:
            return None

    def get_contexts_summary(self):
        summary = dict()
        for key, cail_context in self._context_registry.items():
            summary.update({key: cail_context.get_context_summary()})
        return summary

    def get_contexts_application_objects(self):
        summary = dict()
        for key, cail_context in self._context_registry.items():
            summary.update({key: cail_context.get_context_objects()})
        return summary

    def list_all_contexts(self):
        return list(self._context_registry.keys())

    def get_calculation_log(self):
        return self._calculation_log

    # Support other parameters like policies etc in future
    def _resolve_context(self, market):
        # Use default as fallback
        target_context_key = CAILContextMgr.get_default_context_key()
        generated_context_key = CAILContextMgr.generate_context_key(market)
        # If context already exists, use that
        if self.get_cail_context(generated_context_key):
            target_context_key = generated_context_key
        else:
            # Try to create a new context
            new_cail_context = self.create_cail_context(generated_context_key)
            if new_cail_context:
                target_context_key = generated_context_key

        return target_context_key

    @staticmethod
    def generate_context_key(market):
        return '{}_{}'.format(market.name, str(market.now_time))

    @staticmethod
    def get_default_context_key():
        return 'Default'
