from timeit import default_timer as timer
from datetime import datetime
import uuid, os, logging

log = logging.getLogger('CAILContext')
try:
    from nxpy import integration
except ImportError as error:
    import sys

    log.exception("nxpy.integration is not currently available on this platform.")
    log.exception(error)
    raise SystemExit()
try:
    from nxpy import nxpropy as nx
except ImportError as error:
    import sys

    log.exception("nxpy.nxpropy is not currently available on this platform.")
    log.exception(error)
    raise SystemExit()

class CAILContext:
    # _market_list = list() keep track of all markets that were used in this context

    # Keep track of number of calculations run on this context
    _calculation_count = 0
    # Keep track of CA objects at the end of every calculation
    _object_id_list = list()

    def __init__(self, key = None, repository = None, recording_path = '', recording=False, unpack=False, mdk_schema_path=''):
        self.key = key if key else str(uuid.uuid1())
        self.recording = recording
        self.unpack = unpack
        self.recording_path = recording_path
        self.repository = repository
        self.mdk_schema_path = mdk_schema_path
        self.initialize_context()

    def initialize_context(self):
        # Print out version information
        info = integration.numerix_info()
        # print('____________________________________________')
        # print('Initializing Context {}. Numerix Version {}'.format(self.key, info['Numerix Version']))
        # print()
        # Connect to the repository and create a calculation context.

        if False:
            mt_options = integration.MTOptions(max_parallelism=1, load_balance=True, min_batch_size=10,
                                               randomize_instance_order=False, reuse_local_contexts=True,
                                               retry_failed_marketreports=False)
            self.context = integration.MTCalculationContext(self.repository, mt_options)
        else:
            self.context = integration.LocalCalculationContext(repository = self.repository,
                                                               record = self.recording,
                                                               recording_file = str(os.path.join(self.recording_path,'record_{}_{}.nxt'.format(self.key, str( datetime.now().timestamp())))),
                                                               unpack = self.unpack,
                                                               unpack_file= str(os.path.join(self.recording_path,'unpack_{}_{}.nxt'.format(self.key, str( datetime.now().timestamp()))))
                                                               )

        # Set the market data key schema in the calculation context.
        # mdkSchema = mdk_schema_path if mdk_schema_path else os.path.join(content_path, 'Repository', 'Schema',
        #                                                                  'MarketDataKeySchema.nxrs')
        # self.context.set_market_data_key_schema(mdkSchema)
        self._calculation_count = 0
        self._object_id_list = list()

    def reset_context(self):
        self.dispose()
        self.initialize_context()

    def dispose(self):
        self.context.dispose()

    def get_context_application(self):
        return self.context.application

    def get_context_summary(self):
        app = self.context.application
        return dict(calculation_count=self._calculation_count, object_count=len(app.get_all_ids()),
                    data_object_count=len(app.get_data_ids()),
                    call_object_count=len(app.get_call_ids()), matrix_object_count=len(app.get_matrix_ids()))

    def get_context_objects(self):
        app = self.context.application
        return dict(object_count=len(app.get_all_ids()),
                    data_objects=app.get_data_ids(),
                    call_objects=app.get_call_ids(), matrix_objects=app.get_matrix_ids())

    def _progress(instance, market, results):
        """
        This is the default callback method for CAIL. Currently commented out to avoid too much printing
        :param market:
        :param results:
        :return:
        """
        # if instance is None and market is None and results is None:
        #     print('Calculating results...')
        # elif instance is not None and market is not None and results is None:
        #     print('Calculating instance {}, market {}...'.format(instance, market))
        # elif instance is None and market is None:
        #     print('Finished.')
        # else:
        #     print('Finished calculating instance {}, market {}.'.format(instance, market))
        return False

    # Place holder for parsing oneview format request
    def parse_request(self, request):
        pass

    def run_calculation(self, parameters, instances, market, calc_request='PV', callback=_progress, request_ref=None):
        #TODO add explicit lock on this function to restrict multiple threads from accessing it accidentally
        #requestRef = str(uuid.uuid1()) if request_ref is None else request_ref
        # Hook up progress callback.
        if callback:
            parameters.calculation_progress_callback = integration.CalculationProgressCallback(callback)

        # Parse the requests.
        # Unfortunately CAIL doesnt do any validation so there is no way to know if requested outputs are valid or not
        temp = integration.Request.parse(calc_request)
        if temp:
            requests = temp
        else:
            requests = integration.Request.all()
            log.error('Unable to parse request parameters: {}. Proceeding with all'.format(calc_request))

        if not instances or not market: return
        parameters.instances = instances
        parameters.markets = [market]

        # print('Running calculation {} in context: {}, #trades: {}, market: {}_{}'.format(request_ref, self.key, len(instances), market.name, str(market.now_time)))

        # Perform the calculation.
        calcStart = timer()
        results = self.context.calculate(parameters, requests)
        calcEnd = timer()
        calc_time = calcEnd - calcStart
        self._calculation_count += 1

        log.info('Finished calculation {}. Total time: {} sec'.format(request_ref, calc_time))

        temp_list = self.get_all_ids()
        diff_list = list(set(temp_list) - set(self._object_id_list))

        log.info('Calculation {}: {} new objects created in calculation, total: {}'.format(request_ref, len(diff_list),
                                                                                        len(temp_list)))

        self._object_id_list = [x for x in temp_list]

        return (results, calc_time, diff_list)

    def create_data_object(self, data_dict, id, container_prefix=''):
        return CAILContext.create_data_object_static(self.context.application, data_dict, id, container_prefix)

    def create_call_object(self, call_object, container_prefix=''):
        return CAILContext.create_call_object_static(self.context.application, call_object, container_prefix)

    def recreate_call_object(self, id, container_prefix=''):
        return CAILContext.recreate_call_object_static(self.context.application, id, container_prefix)

    def view_object(self, id, container_prefix='', headers=''):
        return CAILContext.view_object_static(self.context.application, id, container_prefix, headers)

    def view_implemented_object(self, id, container_prefix=''):

        static_object = CAILContext.view_object_static(self.context.application, id, container_prefix,
                                                       headers=['PARAMETER NAMES', 'PARAMETER VALUES'])
        if static_object:
            static_object_dict = CAILContext.generate_dict_from_data_object(static_object)
            id = static_object_dict['PARAMETER VALUES'][
                static_object_dict['PARAMETER NAMES'].index('ImplementedObject')].split('^')
            implemented_object = CAILContext.view_object_static(self.context.application,
                                                                id=str(id[2]) + '^' + str(id[3]),
                                                                container_prefix=str(id[0]) + '^' + str(id[1]))
            implemented_object_dict = CAILContext.generate_dict_from_data_object(implemented_object)
            return implemented_object_dict

    def get_all_ids(self):
        return self.context.application.get_all_ids()

    def validate_market(self, markets):
        for market in markets:
            (report, messages) = self.context.validate_market(market)
            log.info('Validation Report for Market {}:'.format(market))
            log.info(report)

    @staticmethod
    def create_id(id, container_prefix=''):
        if container_prefix:
            return '{}^{}'.format(container_prefix, id)
        else:
            return id

    @staticmethod
    def generate_data_object_from_dict(data_dict):
        data_object = nx.ApplicationData()
        for k, v in data_dict.items():
            data_object.add_values(k, v)
        return data_object

    @staticmethod
    def generate_dict_from_data_object(data_object):
        data_dict = dict()
        for h in data_object.headers():
            data_dict[h] = data_object.data(h)
        return data_dict

    @staticmethod
    def create_data_object_static(app, data_dict, id, container_prefix=''):
        warning = nx.ApplicationWarning()
        app.data(CAILContext.generate_data_object_from_dict(data_dict), CAILContext.create_id(id, container_prefix),
                 warning)
        if warning.count_fatals() > 0:
            raise Exception(str(warning))
        return

    @staticmethod
    def create_call_object_static(app, call_object, container_prefix=''):
        warning = nx.ApplicationWarning()
        if container_prefix: call_object.add_value('CONTAINER', container_prefix)
        app.call(call_object, warning)
        if warning.count_fatals() > 0:
            raise Exception(str(warning))
        return

    @staticmethod
    def recreate_call_object_static(app, id, container_prefix=''):
        copy_call = nx.ApplicationCall()
        app.get_application_call(CAILContext.create_id(id, container_prefix), copy_call)
        CAILContext.create_call_object_static(app, copy_call)
        return

    @staticmethod
    def view_object_static(app, id, container_prefix='', headers=''):
        view = nx.ApplicationData()
        warning = nx.ApplicationWarning()

        if headers:
            app.view(CAILContext.create_id(id, container_prefix), headers, view, warning)
        else:
            app.view(CAILContext.create_id(id, container_prefix), view, warning)
        if warning.count_fatals() > 0:
            raise Exception(str(warning))
        return view

    @staticmethod
    def convert_columns_to_dict():
        pass

    def read_market(self, market_path, market_name):

        log.debug(market_path)
        log.debug(market_name)
        markets = integration.read_markets(market_path, market_name)
        if markets:
            self._market = markets[0]
            log.info('Found market with name {}'.format(market_name))
            return dict(name = self._market.name, date = self._market.now_time, key = self._market.key, quotes_count = len(self._market.quotes))
        else:
            log.error('No market found with name {}'.format(market_name))
            return dict()

    def parse_market_object(self, market_path, market_name):

        log.debug(market_path)
        log.debug(market_name)
        markets = integration.read_markets(market_path, market_name)
        if markets:
            self._market = markets[0]
            log.info('Found market with name {}'.format(market_name))
            return self._market
        else:
            log.error('No market found with name {}'.format(market_name))
            return dict()
