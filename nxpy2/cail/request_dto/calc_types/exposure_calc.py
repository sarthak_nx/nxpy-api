from nxpy2.cail.request_dto.request import CalcTypeEnum
from nxpy2.cail.request_dto.calc_types.pricing_calc import PricingRequest

CALC_TYPE = CalcTypeEnum.EXPOSURE


class ExposureRequest(PricingRequest):
    def __init__(self, request_dict=None, request_ref=None):
        self.initialize_defaults()
        self.fromDict(request_dict)
        self.set_ref(request_ref)

    def initialize_defaults(self, align_margin_to_obs_dates=False,
                            num_paths=500, payout_currency='USD', precalibrate=False, model_dates_spec="2Y:6M,15Y:1Y",
                            observation_dates_spec="2Y:6M,15Y:1Y"):
        super().initialize_defaults()
        self.calcType = 'Exposure'
        self.requirements = False
        self.baseCurrency = 'USD'
        self.counterpartyRiskParams = {
            "alignMarginToObsDates": align_margin_to_obs_dates,
            "hybridSetupMethod": "UNDERLYINGS",
            "modelDatesSpec": model_dates_spec,
            "observationDatesSpec": observation_dates_spec,
            "numPaths": num_paths,
            # "observationDates": ["2015-01-01", "2016-01-01", "2017-01-01"],
            "payoutCurrency": payout_currency,
            "precalibrate": precalibrate,
            "selfCounterparty": "Self",
            "storeGroupExposures": False
        }

    def set_cpty_risk_params(self, **kwargs):
        """
        >>> {
          "selfCounterparty": "Self",
          "precalibrate": false,
          "alignMarginToObsDates": false,
          "hybridSetupMethod": "UNDERLYINGS",
          "numPaths": 500,
          "modelDatesSpec": "2Y:6M,15Y:1Y",
          "storeGroupExposures": false,
          "payoutCurrency": "USD",
          "observationDatesSpec": "2Y:6M,15Y:1Y"
        }
        :param kwargs:
        :return:
        """
        self.counterpartyRiskParams.update(**kwargs)
        return self
