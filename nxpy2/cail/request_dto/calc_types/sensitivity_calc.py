from nxpy2.cail.request_dto.request import CalcTypeEnum
from nxpy2.cail.request_dto.calc_types.pricing_calc import PricingRequest

CALC_TYPE = CalcTypeEnum.SENSITIVITY


class SensitivityRequest(PricingRequest):
    report_name_IRDeltaGammaByInstrument = "IRDELTAGAMMABYINSTRUMENT"
    report_name_IRDeltaGammaByCurve = "IRDELTAGAMMABYCURVE"
    report_name_IRDeltaGammaByBucket = "IRDELTAGAMMABYBUCKET"
    report_name_IRVolVegaVolgaBySurface = "IRVOLVEGAVOMMABYSURFACE"
    report_name_IRVolVegaVolgaByInstrument = "IRVOLVEGAVOMMABYINSTRUMENT"
    report_name_IRVolVegaVolgaByBucket = "IRVOLVEGAVOMMABYBUCKET"
    report_name_FXSpotDeltaGamma = "FXSPOTDELTAGAMMA"
    report_name_FXSpotVolVegaVommaBySurface = "FXSPOTVOLVEGAVOMMABYSURFACE"
    report_name_FXSpotVolVegaVommaByInstrument = "FXSPOTVOLVEGAVOMMABYINSTRUMENT"
    report_name_FXSpotVolVegaVommaByBucket = "FXSPOTVOLVEGAVOMMABYBUCKET"
    report_name_EQSpotDeltaGamma = "EQSPOTDELTAGAMMA"
    report_name_EQSpotVolVegaVommaBySurface = "EQSPOTVOLVEGAVOMMABYSURFACE"
    report_name_EQSpotVolVegaVommaByInstrument = "EQSPOTVOLVEGAVOMMABYINSTRUMENT"
    report_name_EQSpotVolVegaVommaByBucket = "EQSPOTVOLVEGAVOMMABYBUCKET"

    def __init__(self, request_dict=None, request_ref=None):
        self.initialize_defaults()
        self.fromDict(request_dict)
        self.set_ref(request_ref)

    def initialize_defaults(self):
        super().initialize_defaults()
        self.calcType = 'RiskSensitivity'
        self.reportConfigs = None

    def set_report(self, report_name, config_file='risk-sensitivity-report.json'):
        self.reportConfigs = SensitivityRequest.build_report_list([report_name], config_file)
        return self

    @staticmethod
    def get_report_config_from_file(config_file='risk-sensitivity-report.json'):
        import json
        processed_reports = dict()

        with open(config_file, 'r') as rpt:
            reports = json.load(rpt)
            for r in reports:
                new_rpt = r.copy()
                new_params = dict()
                rpt_name = new_rpt['name']
                new_rpt['reportName'] = rpt_name
                new_rpt['reportInstanceName'] = rpt_name
                if 'description' in new_rpt: del new_rpt['description']
                if 'dispName' in new_rpt: del new_rpt['dispName']
                if 'scenarioOutput' in new_rpt: del new_rpt['scenarioOutput']
                if 'marketElementLevelResult' in new_rpt: del new_rpt['marketElementLevelResult']
                if 'riskSensitivityReportFields' in new_rpt:
                    del new_rpt['riskSensitivityReportFields']
                    for p in r['riskSensitivityReportFields']:
                        new_params[p['fieldName']] = p['dfValue']
                        # TODO This is a temp hardcoding for tablular fields
                        # if 'TABLE' in p['fieldName'].upper() or 'SHIFTTENORS' in p['fieldName'].upper():
                        #     new_params[p['fieldName']] = json.loads(p['dfValue'])
                    new_rpt['reportParams'] = new_params
                # else:
                #     #TODO This is a temp hardcoding
                #     new_rpt['reportParams'] = dict(dummy='dummy')
                # TODO This is a temp hardcoding
                # if not 'IRReportType' in new_rpt['reportParams']: new_rpt['reportParams']['IRReportType'] = ''
                processed_reports[rpt_name] = new_rpt
        return processed_reports

    @staticmethod
    def build_report_list(report_name_list, config_file='risk-sensitivity-report.json',
                          request_fields=None):
        reports = []
        processed_reports = SensitivityRequest.get_report_config_from_file(config_file)
        for name in report_name_list:
            if name in processed_reports:
                reports.append(processed_reports[name])
            else:
                raise Exception("Unknown report name : {name}".format(name=name))
        return reports

    @staticmethod
    def build_report_list_old(report_name_list, request_fields=None):
        reports = []
        reportParams = dict(BCKTDEFSTABLE=dict(columnDefinitions=list(), rows=list()))
        reportParams['BCKTDEFSTABLE']['columnDefinitions'].append(
            dict(columnField="BucketName", columnFieldType="String"))
        reportParams['BCKTDEFSTABLE']['columnDefinitions'].append(
            dict(columnField="Bucket_Start", columnFieldType="Double"))
        reportParams['BCKTDEFSTABLE']['columnDefinitions'].append(
            dict(columnField="Bucket_End", columnFieldType="Double"))
        reportParams['BCKTDEFSTABLE']['rows'].append(
            dict(BucketName="o/n-5y", Bucket_Start="0", Bucket_End="5"))
        reportParams['BCKTDEFSTABLE']['rows'].append(
            dict(BucketName="5y-10y", Bucket_Start="5", Bucket_End="10"))
        reportParams['BCKTDEFSTABLE']['rows'].append(
            dict(BucketName="10y+", Bucket_Start="10", Bucket_End="100"))
        for name in report_name_list:
            if SensitivityRequest.report_name_IRDeltaGammaByInstrument == name:
                if bool(request_fields):
                    for f in request_fields:
                        reports.append(dict(reportInstanceName=name, reportCategory="IRDELTAGAMMA",
                                            sensitivityCalcType="BYINSTRUMENT", requestField=f))
                else:
                    reports.append(dict(reportInstanceName=name, reportCategory="IRDELTAGAMMA",
                                        sensitivityCalcType="BYINSTRUMENT", requestField="PV"))
            elif SensitivityRequest.report_name_IRDeltaGammaByCurve == name:
                reports.append(dict(reportInstanceName=name, reportCategory="IRDELTAGAMMA",
                                    sensitivityCalcType="BYCURVE", requestField="PV"))
            elif SensitivityRequest.report_name_IRDeltaGammaByBucket == name:
                reports.append(dict(reportInstanceName=name, reportCategory="IRDELTAGAMMA",
                                    sensitivityCalcType="BYBUCKET", requestField="PV",
                                    reportParams=reportParams))
            elif SensitivityRequest.report_name_IRVolVegaVolgaBySurface == name:
                reports.append(dict(reportInstanceName=name, reportCategory="IRVOLVEGAVOLGA",
                                    sensitivityCalcType="BYSURFACE", requestField="PV"))
            elif SensitivityRequest.report_name_IRVolVegaVolgaByInstrument == name:
                reports.append(dict(reportInstanceName=name, reportCategory="IRVOLVEGAVOLGA",
                                    sensitivityCalcType="BYINSTRUMENT", requestField="PV"))
            elif SensitivityRequest.report_name_IRVolVegaVolgaByBucket == name:
                reports.append(dict(reportInstanceName=name, reportCategory="IRVOLVEGAVOLGA",
                                    sensitivityCalcType="BYBUCKET", requestField="PV",
                                    reportParams=reportParams))
            elif SensitivityRequest.report_name_FXSpotDeltaGamma == name:
                reports.append(dict(reportInstanceName=name, reportCategory="FXSPOTDELTAGAMMA",
                                    sensitivityCalcType=None, requestField="PV"))
            elif SensitivityRequest.report_name_FXSpotVolVegaVommaBySurface == name:
                reports.append(dict(reportInstanceName=name, reportCategory="FXSPOTVOLVEGAVOMMA",
                                    sensitivityCalcType="BYSURFACE", requestField="PV"))
            elif SensitivityRequest.report_name_FXSpotVolVegaVommaByInstrument == name:
                reports.append(dict(reportInstanceName=name, reportCategory="FXSPOTVOLVEGAVOMMA",
                                    sensitivityCalcType="BYINSTRUMENT", requestField="PV"))
            elif SensitivityRequest.report_name_FXSpotVolVegaVommaByBucket == name:
                reports.append(dict(reportInstanceName=name, reportCategory="FXSPOTVOLVEGAVOMMA",
                                    sensitivityCalcType="BUCKETED", requestField="PV",
                                    reportParams=reportParams))
            elif SensitivityRequest.report_name_EQSpotDeltaGamma == name:
                reports.append(dict(reportInstanceName=name, reportCategory="EQSPOTDELTAGAMMA",
                                    sensitivityCalcType=None, requestField="PV"))
            elif SensitivityRequest.report_name_EQSpotVolVegaVommaBySurface == name:
                reports.append(dict(reportInstanceName=name, reportCategory="EQSPOTVOLVEGAVOMMA",
                                    sensitivityCalcType="BYSURFACE", requestField="PV"))
            elif SensitivityRequest.report_name_EQSpotVolVegaVommaByInstrument == name:
                reports.append(dict(reportInstanceName=name, reportCategory="EQSPOTVOLVEGAVOMMA",
                                    sensitivityCalcType="BYINSTRUMENT", requestField="PV"))
            elif SensitivityRequest.report_name_EQSpotVolVegaVommaByBucket == name:
                reports.append(dict(reportInstanceName=name, reportCategory="EQSPOTVOLVEGAVOMMA",
                                    sensitivityCalcType="BUCKETED", requestField="PV",
                                    reportParams=reportParams))
            else:
                raise Exception("Unknown report name : {name}".format(name=name))
        return reports


if __name__ == '__main__':
    import json
    # test = SensitivityRequest.build_report_list_old([SensitivityRequest.report_name_IRDeltaGammaByInstrument])
    # test = SensitivityRequest.build_report_list_old([SensitivityRequest.report_name_IRDeltaGammaByBucket])
    # #print(json.dumps(test, indent=2))
    # processed_reports = SensitivityRequest.get_report_config_from_file()
    # #print(json.dumps(processed_reports, indent=2))
    # final_reports = SensitivityRequest.build_report_list([SensitivityRequest.report_name_IRDeltaGammaByBucket, SensitivityRequest.report_name_EQSpotDeltaGamma, SensitivityRequest.report_name_IRDeltaGammaByInstrument, 'IRDELTAGAMMAZERORATESHIFT', 'THETA'])
    # print(json.dumps(final_reports, indent=2))

    # bucket_params = dict(columnDefinitions=list(), rows=list())
    # bucket_params['columnDefinitions'].append(
    #     dict(columnField="BucketName", columnFieldType="String"))
    # bucket_params['columnDefinitions'].append(
    #     dict(columnField="Bucket_Start", columnFieldType="Double"))
    # bucket_params['columnDefinitions'].append(
    #     dict(columnField="Bucket_End", columnFieldType="Double"))
    # bucket_params['rows'].append(dict(BucketName="0d - 2Y Bucket", Bucket_Start="0", Bucket_End="2"))
    # bucket_params['rows'].append(dict(BucketName="2Y - 7Y Bucket", Bucket_Start="2", Bucket_End="7"))
    # bucket_params['rows'].append(dict(BucketName="7Y - 15Y Bucket", Bucket_Start="7", Bucket_End="15"))
    # bucket_params['rows'].append(dict(BucketName="15Y - 50Y Bucket", Bucket_Start="15", Bucket_End="50"))
    # print(json.dumps(json.dumps(bucket_params)))
    #
    # bucket_params = dict(columnDefinitions=list(), rows=list())
    # bucket_params['columnDefinitions'].append(
    #     dict(columnField="BucketName", columnFieldType="String"))
    # bucket_params['columnDefinitions'].append(
    #     dict(columnField="Bucket_Start", columnFieldType="String"))
    # bucket_params['columnDefinitions'].append(
    #     dict(columnField="Bucket_End", columnFieldType="String"))
    # bucket_params['rows'].append(dict(BucketName="0d - 2Y Bucket", Bucket_Start="0d", Bucket_End="2Y"))
    # bucket_params['rows'].append(dict(BucketName="2Y - 7Y Bucket", Bucket_Start="2Y", Bucket_End="7Y"))
    # bucket_params['rows'].append(dict(BucketName="7Y - 15Y Bucket", Bucket_Start="7Y", Bucket_End="15Y"))
    # bucket_params['rows'].append(dict(BucketName="15Y - 50Y Bucket", Bucket_Start="15Y", Bucket_End="50Y"))
    # print(json.dumps(json.dumps(bucket_params)))
    #
    # bucket_params = dict(columnDefinitions=list(), rows=list())
    # bucket_params['columnDefinitions'].append(
    #     dict(columnField="BucketName", columnFieldType="String"))
    # bucket_params['columnDefinitions'].append(
    #     dict(columnField="Bucket_Start", columnFieldType="String"))
    # bucket_params['columnDefinitions'].append(
    #     dict(columnField="Bucket_End", columnFieldType="String"))
    # bucket_params['rows'].append(dict(BucketName="0d - 3M Bucket", Bucket_Start="0d", Bucket_End="3M"))
    # bucket_params['rows'].append(dict(BucketName="3M - 6M Bucket", Bucket_Start="3M", Bucket_End="6M"))
    # bucket_params['rows'].append(dict(BucketName="6M - 1Y Bucket", Bucket_Start="6M", Bucket_End="1Y"))
    # bucket_params['rows'].append(dict(BucketName="1Y - 5Y Bucket", Bucket_Start="1Y", Bucket_End="5Y"))
    # print(json.dumps(json.dumps(bucket_params)))
    #
    # zero_rate = dict(columnDefinitions=list(), rows=list())
    # zero_rate['columnDefinitions'].append(
    #     dict(columnField="Tenor", columnFieldType="String"))
    # zero_rate['rows'].append(dict(Tenor="2w"))
    # zero_rate['rows'].append(dict(Tenor="1m"))
    # zero_rate['rows'].append(dict(Tenor="3m"))
    # zero_rate['rows'].append(dict(Tenor="6m"))
    # zero_rate['rows'].append(dict(Tenor="1y"))
    # zero_rate['rows'].append(dict(Tenor="2y"))
    # zero_rate['rows'].append(dict(Tenor="3y"))
    # zero_rate['rows'].append(dict(Tenor="4y"))
    # zero_rate['rows'].append(dict(Tenor="5y"))
    # print(json.dumps(json.dumps(zero_rate)))
    #
    # theta = dict(columnDefinitions=list(), rows=list())
    # theta['columnDefinitions'].extend(
    #     [dict(columnField="RollPeriod", columnFieldType="String"),
    #      dict(columnField="Calendar", columnFieldType="String")])
    # theta['rows'].append(dict(RollPeriod="1bd", Calendar='NEWYORK'))
    # print(json.dumps(json.dumps(theta)))
