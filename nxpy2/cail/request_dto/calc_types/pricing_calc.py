from nxpy2.cail.request_dto.request import CalcTypeEnum, CalcRequest

CALC_TYPE = CalcTypeEnum.PRICING


class PricingRequest(CalcRequest):
    def __init__(self, request_dict=None, request_ref=None):
        self.initialize_defaults()
        self.fromDict(request_dict)
        self.set_ref(request_ref)

    def initialize_defaults(self):
        super().initialize_defaults()
        self.calcType = 'Pricing'
        self.requirements = True

        self.baseCurrency = '*'
        self.marketSelection = dict()
        self.marketSelection['marketName'] = None
        self.marketSelection['marketDate'] = None
        self.marketSelection['quotes'] = None
        self.tradeSelection = dict()
        self.tradeSelection['flattenedTradeList'] = list()
        self.marketPolicyName = 'Standard'
        self.pricingPolicyName = 'Standard'
        self.calcMeasureSelection = dict()
        # self.calcMeasureSelection['calcMeasures'] = list() #['pv']
        self.calcMeasureSelection['resultScopeList'] = ['pv']
        self.useRecording = False

    def set_market(self, date, name):
        """

        :param date: Can be datetime.date or ISO string. It will be converted to string
        :param name: market name
        :return:
        """
        import datetime
        if isinstance(date, datetime.date): date = str(date)
        self.marketSelection['marketName'] = name
        self.marketSelection['marketDate'] = date
        self.baseDate = date
        return self

    def set_trade_filters(self, filters=None):
        self.tradeSelection.update({
            "objectType": "TRADE",
            "criteria": {
                "filters": filters if filters else list()
            }
        })
        return self

    def set_trade_criteria(self, criteria=None):
        self.tradeSelection.update({
            "objectType": "TRADE",
            "criteria": criteria if criteria else dict()
        })
        return self

    def set_temp_trades(self, trades):
        self.tradeSelection['flattenedTradeList'] = trades
        return self

    def set_result_scope(self, scope_list):
        self.calcMeasureSelection['resultScopeList'] = scope_list
        return self
