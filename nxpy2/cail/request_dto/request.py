import uuid
from abc import ABC
import copy
import json
from enum import Enum


class CalcTypeEnum(Enum):
    PRICING = 'Pricing'
    SENSITIVITY = 'RiskSensitivity'
    # VAR = 'VaR'
    EXPOSURE = 'Exposure'
    MDE = 'MDE'
    MODEL = 'Model'


class CalcRequest(ABC):
    def __init__(self, request_dict=None, request_ref=None):
        self.initialize_defaults()
        self.fromDict(request_dict)
        self.set_ref(request_ref)

    def initialize_defaults(self):
        self.calcType = None
        self.priority = 0
        # Unfortulately need to rename this variable as 'async' has become a keyword in python from 3.7. Java variable in AG microservice and REST API interface requires 'async'.
        # Special transformation needs to be done for this variable when serializing to json
        self.async_ = False
        self.persistResults = False
        self.baseDate = None
        self.resultContainer = None
        self.description = 'Python Request'
        self.additionalInfo = dict()

    # Used by pickling
    def __getstate__(self):
        # info('{} got pickled'.format(self.requestRef))
        return self.__dict__

    # Used by pickling
    def __setstate__(self, state):
        # info('{} got unpickled'.format(state['requestRef']))
        self.__dict__.update(state)

    def __str__(self):
        return self.toJSON()

    def __repr__(self):
        return self.toJSON()

    def fromDict(self, request_dict):
        if request_dict:
            copy_dict = copy.deepcopy(request_dict)
            if 'async' in copy_dict:
                copy_dict['async_'] = copy_dict['async']
                del copy_dict['async']
            self.update(**copy_dict)
        return self

    def toDict(self):
        return self.__dict__

    def toJSON(self):
        temp = json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True)  # default=lambda o: o.__str__
        # to handle special case of async being a reserved keyword
        final = temp.replace("async_", "async")
        return final

    def clone(self, calc_request):
        """
        Copies the other request, except the requestRef
        :param calc_request: Other CalcRequest instance
        :return:
        """
        check = True if isinstance(calc_request, CalcRequest) else False
        if not check:
            raise Exception('Request is not of type CalcRequest')
        copy_dict = copy.deepcopy(calc_request.toDict())
        copy_dict.pop('requestRef', None)
        # Setting calcType to none to allow cloning pricing request to exposure
        copy_dict.pop('calcType', None)
        self.update(**copy_dict)
        return self

    def update(self, **kwargs):
        self.__dict__.update(**kwargs)
        return self

    def set_ref(self, request_ref=None):
        self.requestRef = str(uuid.uuid1()) if request_ref is None else request_ref
        return self

    def set_result_container(self, name, date=None):
        """
        :param date: Can be datetime.date or ISO string. It will be converted to string
        :param name: result container name
        """
        import datetime
        self.persistResults = True
        self.resultContainer = name
        if isinstance(date, datetime.date): date = str(date)
        if date:
            self.baseDate = date
        elif hasattr(self, 'marketSelection') and 'marketDate' in self.marketSelection:
            self.baseDate = self.marketSelection['marketDate']
        return self
