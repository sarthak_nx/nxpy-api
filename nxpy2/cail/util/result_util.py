import datetime
import traceback
import logging, re
from itertools import groupby
from operator import itemgetter

log = logging.getLogger('ResultUtil')

try:
    from nxpy import integration
except ImportError as error:
    import sys

    log.exception("nxpy.integration is not currently available on this platform.")
    log.exception(error)
    raise SystemExit()


def print_cail_profiling_data(profiling_data):
    # Print out profiling data.
    log.info('CAIL Profiling information:')
    log.info(integration.pretty_print(profiling_data))


# Private
def convert_to_ov_unit_result(instance, result, objectType = None, printMsg=False):
    unit_calc_result = {}
    unit_calc_result['ref'] = str(instance)
    unit_calc_result['objectType'] = objectType
    unit_calc_result['pv'] = None
    unit_calc_result['resultCurrency'] = None
    unit_calc_result['error'] = False
    unit_calc_result['calculationTime'] = None
    unit_calc_result['amountOutput'] = {}
    unit_calc_result['scalarOutput'] = {}
    unit_calc_result['cashflow'] = {}
    unit_calc_result['tabularResults'] = {}

    for ob in result:
        # print('name: {} type: {}'.format(ob.name, type(ob.value)))
        if ob.name.upper() == 'MESSAGES':
            error, messages = process_messages(ob.value)
            unit_calc_result['messages'] = messages
            unit_calc_result['error'] = error
            if printMsg and messages: print(messages)
            continue

        if ob.name.upper() == 'PV':
            unit_calc_result['pv'] = ob.value
            unit_calc_result['resultCurrency'] = ob.currency
            if printMsg: print('Processed PV: %s %s' % (ob.currency, str(ob.value)))

        if ob.name.upper() == 'EXPOSURE':
            if ob.value:
                unit_calc_result['pv'] = ob.value.values[0][0]
                # unit_calc_result['resultCurrency'] = ob.currency
                if printMsg: print('Processed Exposure PV: %s %s' % (ob.currency, str(ob.value.values[0][0])))
            else:
                unit_calc_result['error'] = True

        # if ob.category.upper() == 'CASHFLOW':
        if 'integration.Table' in str(type(ob.value)):
            if printMsg: print('Processing table result %s' % ob.name)
            unit_calc_result['tabularResults'][ob.name] = convert_tabular_result(ob.name, ob.value)
            continue

        if 'integration.Matrix' in str(type(ob.value)):
            if printMsg: print('Ignoring matrix result %s' % ob.name)
            continue

        if ob.category.upper() == 'REPORT':
            # print(json.dumps(ob.value, default=obj_dict))
            if printMsg: print('Ignoring report result %s' % ob.name)
            continue

        if ob.currency:  # treat as amount
            unit_calc_result['amountOutput'][ob.name] = {'value': ob.value, 'currency': ob.currency}
            # unit_calc_result['amountOutput'][ob.name]['category'] = ob.category

        else:  # treat as scalar
            unit_calc_result['scalarOutput'][ob.name] = ob.value

    if unit_calc_result['error']:
        log.error('Instance {} did not successfully calculate.'.format(instance))

    return unit_calc_result


# Private
def convert_tabular_result(name, cail_table):
    if not cail_table.headers or not cail_table.values or not cail_table.values[0]:
        log.error('Invalid table: {}'.format(name))
        return {}
    ov_table = {}
    rows = []
    ov_table['columnDefinitions'] = []
    ov_table['rows'] = rows
    try:
        for h in cail_table.headers:
            header_name = str(h).strip()
            # Setting columns dont have the same column length. ignore. assumption is that setting* columns appear at the end of the table
            if header_name == 'SETTINGHEADERS' or header_name == 'SETTINGVALUES': continue
            ov_table['columnDefinitions'].append({'columnField': header_name})

        column_length = len(cail_table.values[0])
        for r in range(0, column_length):
            row = dict()
            for index, name in zip(list(range(0, len(ov_table['columnDefinitions']))),
                                   [column['columnField'] for column in ov_table['columnDefinitions']]):
                v = cail_table.values[index][r]
                if isinstance(v, datetime.date):
                    v = v.strftime('%Y-%m-%d')
                row[name] = v
            rows.append(row)
    except Exception as error:
        log.error('Error in processing table: {}'.format(name))
        log.exception(error)
        traceback.print_exc()
    return ov_table


# Private
def process_messages(value):
    messages = []
    error = False
    if value:
        for m in value:
            # If any fatal error, mark the trade as error
            if m.severity == integration.MessageSeverityEnum.FatalError:
                # only include fatal errors for now to avoid flooding of messages. TODO drive through property
                messages.append({'severity': str(m.severity), 'description': m.description})
                error = True
                # print('Instance {} did not successfully price. Error: {}'.format(instance, m.description))
    return error, messages


def process_pricing_results(results, market_reports=list(), severity='fatal', print_results=False):
    if not results:
        log.error('No results found. Stopping.')
        return None

    severity = integration.parse_severity(severity)
    # if trace:
    #     severity = integration.MessageSeverityEnum.Normal

    # Parse and print out results.
    print()
    pricing_calc_result = {}
    pricing_calc_result['unitCalcResults'] = []
    for market in results.markets:
        for instance in results.instances:
            if print_results:
                log.info('Processing trade: {}'.format(instance))

            result = results.get_result(instance, market)

            if print_results:
                log.info('Instance {}, market {} results:'.format(instance, market))
                log.info(integration.pretty_print(result, severity))

            unit_calc_result = convert_to_ov_unit_result(instance, result,'TRADE', print_results)
            unit_calc_result['requiredFixings'] = results.get_required_fixings(instance, market)
            unit_calc_result['requiredMarketDataElements'] = results.get_required_market_data_elements(instance, market)
            unit_calc_result['requiredModels'] = results.get_required_models(instance, market)

            pricing_calc_result['unitCalcResults'].append(unit_calc_result)

        pricing_calc_result['calcType'] = 'Pricing'
        pricing_calc_result['calculateStatus'] = 'COMPLETED'
        pricing_calc_result['financialObjectType'] = 'TRADE'
        pricing_calc_result['marketName'] = market.name
        try:
            pricing_calc_result['marketDate'] = market.now_time.strftime('%Y-%m-%d')
        except:
            log.error('Invalid date format in market: {}'.format(str(market.now_time)))

        break;  # just process 1 market

    # Print out market report results.
    for marketReport in market_reports:
        for market in result.markets:
            log.info('Market report {}, market {} results:'.format(marketReport, market))
            log.info(integration.pretty_print(results.get_result(marketReport, market), severity))

    return pricing_calc_result


def group_sensitivity_table_outputs(sensitivity_table_result):
    market_elements = []
    measure_currency = {}
    results = []
    parent_flag = False

    for greek in sensitivity_table_result:
        if 'RISK_FACTOR_LEVEL_PARENT' in greek:
            parent_flag = True
            if greek['RISK_FACTOR_LEVEL_PARENT'] not in market_elements:
                measure_currency[greek['RISK_FACTOR_LEVEL_PARENT']] = greek['MEASURE_CURRENCY']
                market_elements.append(greek['RISK_FACTOR_LEVEL_PARENT'])
        else:
            parent_flag = False
            if greek['RISK_FACTOR_ID_1'] not in market_elements:
                measure_currency[greek['RISK_FACTOR_ID_1']] = greek['MEASURE_CURRENCY']
                market_elements.append(greek['RISK_FACTOR_ID_1'])
    # print(sensitivity_table_result)
    # print(market_elements)
    if parent_flag:
        for parent_rf, grp1 in groupby(sensitivity_table_result, itemgetter('RISK_FACTOR_LEVEL_PARENT')):
            result = {}
            result['marketElement'] = parent_rf
            result['measureCurrency'] = measure_currency[parent_rf]
            result['marketElementSensitivities'] = []
            for rf, grp2 in groupby(list(grp1), itemgetter('RISK_FACTOR_ID_1')):
                marketElementSensitivity = {}
                marketElementSensitivity['riskFactorRef'] = rf
                marketElementSensitivity['sensitivities'] = {}
                for grp3 in list(grp2):
                    marketElementSensitivity['sensitivities'][grp3['TYPE']] = grp3['VALUE']
                result['marketElementSensitivities'].append(marketElementSensitivity)

            results.append(result)
    else:
        for parent_rf, grp1 in groupby(sensitivity_table_result, itemgetter('RISK_FACTOR_ID_1')):
            result = {}
            result['marketElement'] = parent_rf
            result['measureCurrency'] = measure_currency[parent_rf]
            result['marketElementSensitivities'] = []
            for rf, grp2 in groupby(list(grp1), itemgetter('RISK_FACTOR_ID_1')):
                marketElementSensitivity = {}
                marketElementSensitivity['riskFactorRef'] = rf
                marketElementSensitivity['sensitivities'] = {}
                for grp3 in list(grp2):
                    marketElementSensitivity['sensitivities'][grp3['TYPE']] = grp3['VALUE']
                result['marketElementSensitivities'].append(marketElementSensitivity)

            results.append(result)
    # print(results)
    return results


# TODO organize trade level sensitivity results in 1v like JSON
def process_sensitivity_results(results, parameters, severity='fatal', print_results=False):
    if not results:
        log.error('No results found. Stopping.')
        return None

    if not parameters:
        log.error('No request parameters found. Stopping.')
        return None

    severity = integration.parse_severity(severity)
    # if trace:
    #     severity = integration.MessageSeverityEnum.Normal

    # Parse and print out results.

    pricing_calc_result = {}
    pricing_calc_result['unitCalcResults'] = []
    for market in parameters.markets:

        for instance_id, instance in enumerate(parameters.instances):
            trade_id = instance.find_parameter_value('TRADEID')
            if print_results:
                print('Processing trade: {}'.format(trade_id))

            result = results.get_result(instance, market)

            if print_results:
                print('Instance {}, market {} results:'.format(instance, market))
                print(integration.pretty_print(result, severity))

            unit_calc_result = convert_to_ov_unit_result(instance, result, 'TRADE', print_results)
            unit_calc_result['requiredFixings'] = results.get_required_fixings(instance, market)
            unit_calc_result['requiredMarketDataElements'] = results.get_required_market_data_elements(instance, market)
            unit_calc_result['requiredModels'] = results.get_required_models(instance, market)
            unit_calc_result['results'] = {}
            unit_calc_result['sensitivityReportOutput'] = {}
            for marketReport in parameters.market_reports:
                unit_calc_result['sensitivityReportOutput'][str(marketReport)] = []
                for market in parameters.markets:
                    sensitivity_result_tables = results.get_result(marketReport, market)

                    for res in sensitivity_result_tables:
                        if res.name.upper() in ['PIVOTTABLE']:
                            table = convert_tabular_result(res.name, res.value)

                            sensitivity = []
                            if table:
                                for rf in table['rows']:
                                    if rf['TRADE ID'].upper().endswith(trade_id.upper()):
                                        if market.name.replace(' ', '').upper() in rf['MARKET']:
                                            if 'BASEPV' in rf.keys():
                                                if isinstance(rf['BASEPV'], str):
                                                    rf['BASEPV'] = float(re.search('{(.+?)}', rf['BASEPV']).group(1).split(',')[instance_id])
                                            sensitivity.append(rf)

                            unit_calc_result['sensitivityReportOutput'][str(marketReport)] = group_sensitivity_table_outputs(sensitivity)
                            unit_calc_result['results'][res.name.upper()] = sensitivity
                        elif res.name.upper() == 'MESSAGES':
                            error, messages = process_messages(res.value)
                            unit_calc_result['messages'] = messages
                            unit_calc_result['error'] = error
                        elif res.name.upper() in ['GREEKDEFINITIONS','RISKFACTORDEFINITIONS']:
                            table = convert_tabular_result(res.name, res.value)

                            unit_calc_result['results'][res.name.upper()] = table
                        else:
                            print('Result parser for {} not implemented'.format(res.name.upper()))


            pricing_calc_result['unitCalcResults'].append(unit_calc_result)

        pricing_calc_result['calcType'] = 'Sensitivity'
        pricing_calc_result['calculateStatus'] = 'COMPLETED'
        pricing_calc_result['financialObjectType'] = 'TRADE'
        pricing_calc_result['marketName'] = market.name
        try:
            pricing_calc_result['marketDate'] = market.now_time.strftime('%Y-%m-%d')
        except:
            log.error('Invalid date format in market: {}'.format(str(market.now_time)))

        break;  # just process 1 market

    # Print out market report results.

    for marketReport in parameters.market_reports:

        for market in parameters.markets:
            if print_results:
                log.info('Market report {}, market {} results:'.format(marketReport, market))
                log.info(integration.pretty_print(results.get_result(marketReport, market), severity))

    # print(json.dumps(pricing_calc_result, default = str))
    return pricing_calc_result


def process_exposure_results(results, parameters, severity='fatal', print_results=False):
    if not results:
        log.error('No results found. Stopping.')
        return None

    if not parameters:
        log.error('No request parameters found. Stopping.')
        return None

    severity = integration.parse_severity(severity)
    # if trace:
    #     severity = integration.MessageSeverityEnum.Normal

    # Loop over markets, then instances.
    # For each instance, look through the results to find the exposure matrix.
    # Aggregate all of the exposures for each market and plot.

    # Parse and print out results.

    exposure_calc_result = {}
    exposure_calc_result['unitCalcResults'] = []
    for market in parameters.markets:
        aggregate_exposure = None
        obs_dates = None
        transpose = False
        for instance in parameters.instances:
            if print_results:

                log.info('Processing trade: {}'.format(instance))

            result = results.get_result(instance, market)

            # Dont print exposure matrices
            # if print_results:
            #     print('Instance {}, market {} results:'.format(instance, market))
            #     print(integration.pretty_print(result, severity))
            #     print()

            unit_calc_result = convert_to_ov_unit_result(instance, result, 'TRADE', print_results)
            exposure = [r for r in result if r.name == 'Exposure'][:1]
            if exposure:
                if aggregate_exposure is None:
                    aggregate_exposure = exposure[0].value.values
                    transpose = True if type(exposure[0].value.row_headers[0]) is str else False
                    if transpose:
                        obs_dates = exposure[0].value.column_headers
                    else:
                        obs_dates = exposure[0].value.row_headers
                else:
                    transpose = True if type(exposure[0].value.row_headers[0]) is str else False
                    aggregate_exposure += exposure[0].value.values.T if transpose else \
                        exposure[0].value.values
            else:
                # print('Instance {}, in market {} did not return exposure.'.format(instance, market))
                # Error handling taken care of in convert_to_ov_unit_result()
                pass

            exposure_calc_result['unitCalcResults'].append(unit_calc_result)

        exposure_calc_result['calcType'] = 'Exposure'
        exposure_calc_result['calculateStatus'] = 'COMPLETED'
        exposure_calc_result['financialObjectType'] = 'TRADE'
        exposure_calc_result['marketName'] = market.name
        try:
            exposure_calc_result['marketDate'] = market.now_time.strftime('%Y-%m-%d')
        except:
            log.error('Invalid date format in market: {}'.format(str(market.now_time)))

        exposure_calc_result['obsDates'] = obs_dates
        exposure_calc_result['aggregateExposure'] = aggregate_exposure  # .T if transpose else aggregate_exposure

        break  # just process 1 market
    return exposure_calc_result



def process_market_data_element_results(results, cail_context = None, market_reports=list(), severity='fatal', print_results=False):
    if not results:

        log.error('No results found. Stopping.')
        return None

    severity = integration.parse_severity(severity)
    # if trace:
    #     severity = integration.MessageSeverityEnum.Normal

    # Parse and print out results.

    pricing_calc_result = {}
    pricing_calc_result['unitCalcResults'] = []
    for market in results.markets:
        for instance in results.instances:

            log.debug('Processing instance: {}'.format(instance))

            if print_results:

                log.info('Processing trade: {}'.format(instance))

            result = results.get_result(instance, market)

            if print_results:
                log.info('Instance {}, market {} results:'.format(instance, market))
                log.info(integration.pretty_print(result, severity))
                # print(json.dumps(result,default=obj_dict))

            unit_calc_result = convert_to_ov_unit_result(instance, result, 'MARKETELEMENT', print_results)

            unit_calc_result['requiredFixings'] = results.get_required_fixings(instance, market)
            unit_calc_result['requiredMarketDataElements'] = results.get_required_market_data_elements(instance, market)
            unit_calc_result['requiredModels'] = results.get_required_models(instance, market)

            pricing_calc_result['unitCalcResults'].append(unit_calc_result)

        pricing_calc_result['calcType'] = 'Bootstrapping'
        pricing_calc_result['calculateStatus'] = 'COMPLETED'
        pricing_calc_result['financialObjectType'] = 'MARKETELEMENT'
        pricing_calc_result['marketName'] = market.name
        try:
            pricing_calc_result['marketDate'] = market.now_time.strftime('%Y-%m-%d')
        except:
            log.error('Invalid date format in market: {}'.format(str(market.now_time)))

        break;  # just process 1 market

    return pricing_calc_result



def process_model_calibration_results(results, cail_context = None, market_reports=list(), severity='fatal', print_results=False):
    if not results:

        log.error('No results found. Stopping.')
        return None

    severity = integration.parse_severity(severity)
    # if trace:
    #     severity = integration.MessageSeverityEnum.Normal

    # Parse and print out results.

    pricing_calc_result = {}
    pricing_calc_result['unitCalcResults'] = []
    for market in results.markets:
        for instance in results.instances:

            log.debug('Processing instance: {}'.format(instance))

            if print_results:

                log.info('Processing trade: {}'.format(instance))

            result = results.get_result(instance, market)

            if print_results:
                log.info('Instance {}, market {} results:'.format(instance, market))
                log.info(integration.pretty_print(result, severity))
                # print(json.dumps(result,default=obj_dict))

            unit_calc_result = convert_to_ov_unit_result(instance, result, 'MODEL', print_results)

            unit_calc_result['requiredFixings'] = results.get_required_fixings(instance, market)
            unit_calc_result['requiredMarketDataElements'] = results.get_required_market_data_elements(instance, market)
            unit_calc_result['requiredModels'] = results.get_required_models(instance, market)

            pricing_calc_result['unitCalcResults'].append(unit_calc_result)

        pricing_calc_result['calcType'] = 'ModelCalibration'
        pricing_calc_result['calculateStatus'] = 'COMPLETED'
        pricing_calc_result['financialObjectType'] = 'MODEL'
        pricing_calc_result['marketName'] = market.name
        try:
            pricing_calc_result['marketDate'] = market.now_time.strftime('%Y-%m-%d')
        except:
            log.error('Invalid date format in market: {}'.format(str(market.now_time)))

        break;  # just process 1 market

    return pricing_calc_result
#
# def get_market_data_object_result(cail_context, instance, market):
#
#     unit_calc_result = {}
#     unit_calc_result['ref'] = str(instance)
#     unit_calc_result['objectType'] = 'TRADE'
#     unit_calc_result['pv'] = None
#     unit_calc_result['resultCurrency'] = None
#     unit_calc_result['error'] = False
#     unit_calc_result['calculationTime'] = None
#     unit_calc_result['amountOutput'] = {}
#     unit_calc_result['scalarOutput'] = {}
#     unit_calc_result['cashflow'] = {}
#     unit_calc_result['tabularResults'] = {}
#
#
#     mde_object = search.market_data_objects(cail_context.application, instance.find_parameter_value('MDEID'))
#     mde_id, data = cail.get_mde_implemented_object(cail_context.application, mde_object[0])
#     log.debug('Found MDE Implemented Object {}'.format(mde_id))
#     mde_dict =cail.get_object_dict_from_id(cail_context.application, mde_id)
#     mde_type = cail.get_object_type(cail_context.application, mde_id)
#
#     log.debug(mde_type)
#
#     mde_fields = {}
#     mde_fields['ref'] = instance.find_parameter_value('MDEID')
#     mde_fields['type'] = mde_type.upper()
#
#     if messages:
#         mde_fields['messages'] = [x['message'] for x in messages if x['ref'] == mde_fields['ref']][0]
#     else:
#         mde_fields['messages'] = []
#
#     if mde_type.upper() == 'YIELD' or mde_type.upper() == 'LIBOR BASIS CURVE' or mde_type.upper() == 'CMDTY FORWARD CURVE' or mde_type.upper() == 'BASIS CURVE':
#
#         mde_fields['results'] = get_discount_curve_dict(mde_dict)
#
#     elif 'VOL' in mde_type.upper():
#         mde_fields['results'] = get_vol_surface_dict(mde_dict)
#
#
#
# def get_discount_curve_dict(mde_dict):
#
#     yield_curve = []
#     for name, date, df in zip(mde_dict['NAME'], mde_dict['DATE'], mde_dict['DISCOUNT FACTOR']):
#         yield_dict = {}
#         yield_dict['NAME'] = name
#         yield_dict['DATE'] = date
#         yield_dict['DISCOUNT FACTOR'] = df
#         yield_curve.append(yield_dict)
#
#     return yield_curve
#
#
# def get_vol_surface_dict(mde_dict):
#
#     surface = []
#
#     for i, date in enumerate(mde_dict['Dates']):
#         surface_dict = {}
#         surface_dict['DATE'] = date
#         for k, v in mde_dict.items():
#             try:
#                 strike = float(k)
#                 surface_dict[str(strike)] = mde_dict[str(strike)][i]
#             except:
#                 pass
#         surface.append(surface_dict)
#
#     return surface