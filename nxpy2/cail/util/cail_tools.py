import logging

log = logging.getLogger('CAILTools')

try:
    from nxpy import nxpropy as nx
except ImportError as error:
    import sys

    log.exception("nxpy.nxpropy is not currently available on this platform.")
    log.exception(error)
    raise SystemExit()

from nxpy2.nxtools import search


def get_object_dict_from_id(app, id):
    data = nx.ApplicationData()
    app.view(id, data, nx.ApplicationWarning())
    data_dict = convert_object_to_dict(data)

    return data_dict


def convert_object_to_dict(data):
    data_dict = dict()
    for h in data.headers():
        try:
            data_dict[h] = data.data(h)
        except Exception as error:
            log.error('Could not parse header {} from object'.format(str(h)))
            log.exception(error)
            continue

    return data_dict


def get_terms_inputs_dict(app):
    terms = search.terms_object(app)

    warning = nx.ApplicationWarning()
    data = nx.ApplicationData()

    app.view(terms, data, warning)

    data_dict = convert_object_to_dict(data)

    container = data_dict['CALL VALUES'][data_dict['CALL HEADERS'].index('CONTAINER')]
    terms_input = data_dict['CALL VALUES'][data_dict['CALL HEADERS'].index('INPUTS')]
    app.view(container + '^' + terms_input, data, warning)

    data_dict = convert_object_to_dict(data)

    return container, data_dict, terms


def get_pricer_implemented_object(app):
    pricer = search.pricer_object(app)

    warning = nx.ApplicationWarning()
    data = nx.ApplicationData()
    app.view(pricer, data, warning)
    data_dict = convert_object_to_dict(data)

    imp_pricer = data_dict['PARAMETER VALUES'][
        data_dict['PARAMETER NAMES'].index('ImplementedObject')]
    app.view(imp_pricer, data, warning)

    return imp_pricer, data


def get_pricer_quality(app):
    data = nx.ApplicationData()
    warning = nx.ApplicationWarning()
    pricer_imp_id, pricer_data = get_pricer_implemented_object(app)
    data_dict = convert_object_to_dict(pricer_data)

    if 'QUALITY' in data_dict['CALL HEADERS']:
        if isinstance(data_dict['CALL VALUES'][data_dict['CALL HEADERS'].index('QUALITY')], str):
            quality_object_id = data_dict['CALL VALUES'][data_dict['CALL HEADERS'].index('QUALITY')]
            app.view(quality_object_id, data, warning)
            quality_dict = convert_object_to_dict(data)

            return quality_dict

        elif isinstance(data_dict['CALL VALUES'][data_dict['CALL HEADERS'].index('QUALITY')],
                        float):
            quality = data_dict['CALL VALUES'][data_dict['CALL HEADERS'].index('QUALITY')]

            return quality

    elif 'quality' in data_dict['CALL HEADERS']:
        if isinstance(data_dict['CALL VALUES'][data_dict['CALL HEADERS'].index('quality')], str):
            quality_object_id = data_dict['CALL VALUES'][data_dict['CALL HEADERS'].index('quality')]
            app.view(quality_object_id, data, warning)
            quality_dict = convert_object_to_dict(data)

            return quality_dict

        elif isinstance(data_dict['CALL VALUES'][data_dict['CALL HEADERS'].index('quality')],
                        float):
            quality = data_dict['CALL VALUES'][data_dict['CALL HEADERS'].index('quality')]

            return quality
    else:
        print('Pricer does not have QUALITY as input')

        return None


def get_model_implemented_object(app, ref):
    warning = nx.ApplicationWarning()
    data = nx.ApplicationData()
    app.view(ref, data, warning)
    data_dict = convert_object_to_dict(data)

    model_imp_object = data_dict['PARAMETER VALUES'][
        data_dict['PARAMETER NAMES'].index('ImplementedObject')]
    app.view(model_imp_object, data, warning)

    return model_imp_object, data


def get_mde_implemented_object(app, ref):
    warning = nx.ApplicationWarning()
    data = nx.ApplicationData()
    app.view(ref, data, warning)

    data_dict = convert_object_to_dict(data)

    mde_imp_object = data_dict['PARAMETER VALUES'][
        data_dict['PARAMETER NAMES'].index('ImplementedObject')]
    app.view(mde_imp_object, data, warning)

    return mde_imp_object, data


def get_object_type(app, ref):
    warning = nx.ApplicationWarning()
    data = nx.ApplicationData()
    app.view(ref, data, warning)
    data_dict = convert_object_to_dict(data)

    type = data_dict['CALL VALUES'][data_dict['CALL HEADERS'].index('TYPE')]

    return type


def get_object_parameters(app, ref):
    pricer_dict = get_object_dict_from_id(app, ref)
    pricer_fields = {}
    for k, v in zip(pricer_dict['PARAMETER NAMES'], pricer_dict['PARAMETER VALUES']):
        if 'AUTOGEN' not in str(v) and 'CONTEXT' not in str(v):
            pricer_fields[k] = v

    return pricer_fields

def convert_market_element_to_mde_instance(market_element, key_map):

    value_map = dict()
    for key in key_map:
        if key['isAux'] and 'auxDetails' in market_element:
            for k in market_element['auxDetails']:
                if k['field'] == key['intFieldName']:
                    value_map[k['field']] = k['value']
        else:
            for k, v in market_element.items():
                if k == key['intFieldName']:
                    value_map[k] = v

    mde_instance = dict()
    for key in key_map:
        try:
            if key['mdeFieldProcessingMethod']:
                x = value_map[key['intFieldName']]
                mde_instance[key['mdeField']] = eval(key['mdeFieldProcessingMethod'])
            else:
                mde_instance[key['mdeField']] = value_map[key['intFieldName']]
        except:
            continue

    return mde_instance
#
# if __name__ == '__main__':
#
#     market_element = {
#         "baseCurrency": "USD",
#         "obsIndex1": "USD-LIBOR-3M",
#         "obsIndex2": "JPY-LIBOR-3M",
#         "termCurrency": "JPY",
#         "underlyingRef": "IR.JPY-LIBOR-3M/USD-LIBOR-3M.BASIS",
#         "name": "IR.JPY-LIBOR-3M/USD-LIBOR-3M.BASIS",
#         "dispName": "IR.JPY-LIBOR-3M/USD-LIBOR-3M.BASIS",
#         "auxDetails": [
#             {
#                 "field": "basis",
#                 "value": "ACT/365"
#             },
#             {
#                 "field": "useCcBasisInstrument",
#                 "value": True
#             },
#             {
#                 "field": "useFxFwdInstrument",
#                 "value": False
#             },
#             {
#                 "field": "strippingInstrumentsCutoffTenor",
#                 "value": "1Y"
#             }
#         ],
#         "marketElementType": "CCBasisCurve",
#         "underlyingType": "Curve",
#         "marketPolicy": "Standard",
#         "marketElementMembers": []
#     }
#
#     key_map = [
#         {
#             "mktElementType": "CCBasisCurve",
#             "intFieldName": "baseCurrency",
#             "mdeField": "KnownCurrency",
#             "isAux": False,
#             "mdeFieldType": "str",
#             "mdeFieldProcessingMethod": None
#         },
#         {
#             "mktElementType": "CCBasisCurve",
#             "intFieldName": "basis",
#             "mdeField": "Basis",
#             "isAux": True,
#             "mdeFieldType": "bool",
#             "mdeFieldProcessingMethod": None
#         },
#         {
#             "mktElementType": "CCBasisCurve",
#             "intFieldName": "fxFwdMaturityType",
#             "mdeField": "FXFwdMaturityType",
#             "isAux": True,
#             "mdeFieldType": "str",
#             "mdeFieldProcessingMethod": None
#         },
#         {
#             "mktElementType": "CCBasisCurve",
#             "intFieldName": "obsIndex1",
#             "mdeField": "KnownIRIndex",
#             "isAux": False,
#             "mdeFieldType": "str",
#             "mdeFieldProcessingMethod": "x.split('-')[1]"
#         },
#         {
#             "mktElementType": "CCBasisCurve",
#             "intFieldName": "obsIndex1",
#             "mdeField": "KnownIRIndexTenor",
#             "isAux": False,
#             "mdeFieldType": "str",
#             "mdeFieldProcessingMethod": "x.split('-')[2]"
#         },
#         {
#             "mktElementType": "CCBasisCurve",
#             "intFieldName": "obsIndex2",
#             "mdeField": "TargetIRIndex",
#             "isAux": False,
#             "mdeFieldType": "str",
#             "mdeFieldProcessingMethod": "x.split('-')[1]"
#         },
#         {
#             "mktElementType": "CCBasisCurve",
#             "intFieldName": "obsIndex2",
#             "mdeField": "TargetIRIndexTenor",
#             "isAux": False,
#             "mdeFieldType": "str",
#             "mdeFieldProcessingMethod": "x.split('-')[2]"
#         },
#         {
#             "mktElementType": "CCBasisCurve",
#             "intFieldName": "strippingInstrumentsCutoffTenor",
#             "mdeField": "StrippingInstrumentsCutoffTenor",
#             "isAux": True,
#             "mdeFieldType": "str",
#             "mdeFieldProcessingMethod": None
#         },
#         {
#             "mktElementType": "CCBasisCurve",
#             "intFieldName": "termCurrency",
#             "mdeField": "TargetCurrency",
#             "isAux": False,
#             "mdeFieldType": "str",
#             "mdeFieldProcessingMethod": None
#         },
#         {
#             "mktElementType": "CCBasisCurve",
#             "intFieldName": "useCcBasisInstrument",
#             "mdeField": "useXCCYBasisInstruments",
#             "isAux": True,
#             "mdeFieldType": "bool",
#             "mdeFieldProcessingMethod": None
#         },
#         {
#             "mktElementType": "CCBasisCurve",
#             "intFieldName": "useFxFwdInstrument",
#             "mdeField": "UseFXFwdInstr",
#             "isAux": True,
#             "mdeFieldType": "bool",
#             "mdeFieldProcessingMethod": None
#         }
#     ]
#
#     convert_market_element_to_mde_instance(market_element, key_map)