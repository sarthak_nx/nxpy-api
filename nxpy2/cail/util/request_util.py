import os, logging
log = logging.getLogger('RequestUtil')

try:
    from nxpy import integration
except ImportError as error:
    import sys

    log.exception("nxpy.integration is not currently available on this platform.")
    log.exception(error)
    raise SystemExit()

    # Process command line. NOT USED
    # cline = argparse.ArgumentParser(description='A generic CAIL driver program that reads data from CSV files.')
    # cline.add_argument('-path', default='', help='the full path to the content directory')
    # cline.add_argument('-quotes', default='PV',
    #                    help='a list of quote names or groups, separated by commas, no whitespace (by default this is PV)')
    # cline.add_argument('-terms', default='',
    #                    help='a list of trade terms file names, separated by commas, no whitespace, no file extension; if not empty, only these trade terms are calculated')
    # cline.add_argument('-trades', default='',
    #                    help='a list of trade file names, separated by commas, no whitespace, no file extension; if not empty, only these trades are calculated')
    # cline.add_argument('-mdes', default='',
    #                    help='a list of market data element file names, separated by commas, no whitespace, no file extension; if not empty, only these market data elements are calculated; if empty, none are calculated')
    # cline.add_argument('-models', default='',
    #                    help='a list of model file names, separated by commas, no whitespace, no file extension; if not empty, only these models are calculated; if empty, none are calculated')
    # cline.add_argument('-markets', default='@1',
    #                    help='a list of market file names, separated by commas, no whitespace, no file extension; if not empty, only these markets are calculated')
    # cline.add_argument('-policies', default='',
    #                    help='a list of policy file names, separated by commas, no whitespace, no file extension; if not empty, only these policies are passed to the calculation; if empty, none are passed')
    # cline.add_argument('-marketpolicy', default='', help='the market policy file name, no file extension')
    # cline.add_argument('-marketreports', default='',
    #                    help='a list of market report file names, separated by commas, no whitespace, no file extension; if not empty, only these market reports are calculated; if empty, none are calculated')
    # cline.add_argument('-pricingpolicy', default='Standard', help='the pricing policy file name, no file extension')
    # cline.add_argument('-exposureonly', action='store_true',
    #                    help='whether to only compute exposure or to include discount factors, survival probabilities, etc')
    # cline.add_argument('-hybridspec', default='',
    #                    help='the file prefix of a file (assumed to be in CSV format - do not include .csv in name) holding a set of model specifications')
    # cline.add_argument('-hybridconfig', default='HYBRIDMODELBUILDER.DEFAULT',
    #                    help='the key used to identify a hybrid model builder configuration file in the repository')
    # cline.add_argument('-obsdates', default='ObservationDates',
    #                    help='the file prefix of a file (assumed to be in CSV format - do not include .csv in name) holding a set of observation dates')
    # cline.add_argument('-modeldates', default='ModelDates',
    #                    help='the file prefix of a file (assumed to be in CSV format - do not include .csv in name) holding a set of model dates')
    # cline.add_argument('-numpaths', type=int, default=10,
    #                    help='the number of monte carlo paths to use in the calculation')
    # cline.add_argument('-record', action='store_true', help='whether to generate a recording')
    # cline.add_argument('-recordfile', default='', help='the full path of the recording file')
    # cline.add_argument('-unpack', action='store_true', help='whether to dump unpacked instantiations')
    # cline.add_argument('-unpackfile', default='', help='the full path of the unpacked instantiations file')
    # cline.add_argument('-debug', action='store_true',
    #                    help='whether the CrossAsset debugger should be invoked (Windows only)')
    # cline.add_argument('-trace', action='store_true',
    #                    help='whether tracing messages should be included in the messages from a calculation')
    # cline.add_argument('-profile', action='store_true',
    #                    help='whether the CAIL profiler should be run for the calculation')
    # cline.add_argument('-severity', default='fatal',
    #                    help='the minimum severity level of messages to display or none if no messages should be emitted')
    # cline.add_argument('-mt', action='store_true', help='whether to use a multi-threaded calculation context')
    # cline.add_argument('-mtoptionsname', default='DefaultMTOptions',
    #                    help='the multi-thread options file name, no file extension')


def obj_dict(obj):
    return obj.__dict__


def progress(instance, market, results):
    if instance is None and market is None and results is None:
        log.debug('Calculating results...')
    elif instance is not None and market is not None and results is None:
        log.debug('Calculating instance {}, market {}...'.format(instance, market))
    elif instance is None and market is None:
        log.debug('Finished.')
    else:
        log.debug('Finished calculating instance {}, market {}.'.format(instance, market))
    return False


def create_hybrid_model_from_config(repository, parameters, hybridconfig):
    # Find the hybrid model configuration in the repository and use it to instantiate a
    # hybrid model builder. Use it to return the global specifications as well.

    config = repository.find_hybrid_model_configuration(hybridconfig)
    builder = integration.HybridModelBuilder(config, repository, parameters)
    model_specifications = builder.global_specifications
    return builder, model_specifications


def create_hybrid_model_from_spec(hybrid_models_path, repository, parameters, hybridspec):
    # Read the global model specifications from data and use them to initialize the hybrid model builder.
    # Create the local model specifications from the portfolio.

    global_specifications = integration.read_model_specifications(hybrid_models_path, hybridspec)
    builder = integration.HybridModelBuilder(global_specifications)
    model_specifications = builder.create_hybrid_model_specifications(repository, parameters)
    return builder, model_specifications


def get_hybrid_model_schedules(hybrid_models_path, parameters, obsdates, modeldates):
    # Get the observation dates and model dates from the data.
    obs_dates = integration.read_hybrid_model_dates(hybrid_models_path, obsdates)
    model_dates = integration.read_hybrid_model_dates(hybrid_models_path, modeldates)

    # The observation dates should contain the dates of all of the markets we're using.
    for market in parameters.markets:
        obs_dates.append(market.now_time)

    # The model dates should include the observation dates.
    model_dates.extend(obs_dates)

    # Store into observation schedules and return.
    # The factory will sort and remove duplicates.
    return integration.create_observation_schedule(
        obs_dates), integration.create_observation_schedule(model_dates)


def configure_hybrid_model(content_path, repository, parameters, hybridspec, hybridconfig, numpaths,
                           obsdates,
                           modeldates, trace):
    # Create the hybrid model builder using either a hybrid model configuration
    # from the repository or a set of specifications from data.
    hybrid_models_path = os.path.join(content_path, 'Data', 'HybridModels')
    if not hybridspec:
        (builder, model_specifications) = create_hybrid_model_from_config(repository, parameters,
                                                                          hybridconfig)
    else:
        (builder, model_specifications) = create_hybrid_model_from_spec(hybrid_models_path,
                                                                        repository, parameters,
                                                                        hybridspec)

    # Get the model specifications from the builder and, in turn, the base set of hybrid model parameters.
    hybrid_model_parameters = builder.create_hybrid_model_parameters(model_specifications, trace)

    # Get the observation and model dates.
    (obsDates, model_dates) = get_hybrid_model_schedules(hybrid_models_path, parameters, obsdates,
                                                         modeldates)

    # Add the model dates to the hybrid model parameters.
    hybrid_model_parameters['Model Dates'] = model_dates

    # Set up the parameters used with the exposure report, to note the ID
    # of the hybrid model object, and the parameters for it. The ID of
    # hybrid model object should be "HYBRID" to match CAIL and template
    # expectations.

    exposure_parameters = {'Model ID': 'HYBRID', 'Observation Dates': obsDates, 'Paths': numpaths}
    exposure_only_parameters = {'Model ID': 'HYBRID', 'Compute Exposure Only': True,
                                'Observation Dates': obsDates,
                                'Paths': numpaths}

    # Add the self model ID if we have one.

    self_id = integration.get_self_model_id(model_specifications)
    if self_id:
        exposure_parameters['Self Credit Model'] = self_id

    # Add the maps of counterparty model IDs and collateral assets to the main parameter object.
    parameters.counterparty_model_map = integration.get_counterparty_model_map(model_specifications,
                                                                               parameters.instances)
    parameters.collateral_asset_map = integration.get_collateral_asset_map(model_specifications,
                                                                           parameters.instances)

    # Put these in lookup rules that we provide to the calculation context to
    # apply in addition to the default lookup rules. The three additional rules
    # here are what we need to provide to get exposures out. Note these have
    # to go at the start so they have the highest priority.

    lookups = []
    lookups.append(
        integration.ExactLookupRule('HYBRID', 'MODEL.HY.UNIVERSAL2', hybrid_model_parameters))
    lookups.append(
        integration.ExactLookupRule('RISK.REPORT.EXPOSURE', 'REPORT.EXPOSURE', exposure_parameters))
    lookups.append(integration.ExactLookupRule('RISK.REPORT.EXPOSUREONLY', 'REPORT.EXPOSURE',
                                               exposure_only_parameters))
    lookups.extend(parameters.lookups)
    parameters.lookups = lookups
    return parameters


def get_market_data_key(content_path, market_csv_name):
    markets = build_market_instance_from_file(content_path=content_path, market=market_csv_name)
    if markets:
        for m in markets:
            return str(m.key)
    else:
        log.error('No valid markets found.')
        return None


def build_calc_parameters(content_path, repository=None, fixings_path='', market_report_path='',
                          market_reports=None,
                          pricing_policy_name='Standard', market_policy_name='Standard',
                          debug=False, trace=False, profile=False, read_fixings=False):
    if market_reports == None:
        market_reports = []
    content_path = integration.find_data_directory(content_path)

    if not repository:
        repository = integration.LocalRepository(content_path)

    # Create calculation parameters.
    parameters = integration.CalculationParameters(repository, trace, debug, profile)

    # Calculate profiling data.
    parameters.profiling_data = profile;

    # Read custom lookups. If none are found, this is OK.
    lookupsPath = os.path.join(content_path, 'Data', 'LookupRules')
    parameters.lookups = integration.read_lookup_rules(lookupsPath)

    # Read reference data. If none are found, this is OK.
    referenceDataPath = os.path.join(content_path, 'Data', 'ReferenceData')
    parameters.reference_data = integration.read_reference_data(referenceDataPath, repository)

    # Read pricing policy.
    pricingPolicyPath = os.path.join(content_path, 'Data', 'PricingPolicies')
    parameters.pricing_policy = integration.read_pricing_policy(pricingPolicyPath, repository,
                                                                pricing_policy_name)

    # Read market policy.
    marketPolicyPath = os.path.join(content_path, 'Data', 'MarketPolicies')
    parameters.market_policy = integration.read_market_policy(marketPolicyPath, repository,
                                                              market_policy_name)

    # Read general policies.
    policyPath = os.path.join(content_path, 'Data', 'Policies')
    parameters.policies = integration.read_policies(policyPath, repository, '')

    # Read market reports.
    if not market_report_path:
        marketReportPath = os.path.join(content_path, 'Data', 'MarketReports')
    else:
        marketReportPath = market_report_path
    mrStr = ",".join(market_reports)
    if mrStr:
        log.debug('Reading Market Reports from content_path %s: %s' % (marketReportPath, mrStr))
        # print(integration.read_market_reports(marketReportPath, repository, mrStr))
        parameters.market_reports = integration.read_market_reports(marketReportPath, repository,
                                                                    mrStr)

    if read_fixings:
        parameters.fixings = build_fixings_from_file(content_path=content_path, fixings_path=fixings_path)


    parameters.reuse_policy = integration.ReusePolicyEnum.ReuseMarketsAndFixings
    parameters.deletion_policy = integration.DeletionPolicyEnum.DeleteNothing

    return parameters


def build_fixings_from_file(content_path, fixings_path=''):
    # Read fixings data. If none are found, this is OK.
    if not fixings_path:
        fixingsPath = os.path.join(content_path, 'Data', 'Fixings')
    else:
        fixingsPath = fixings_path
    log.debug('Reading Fixings from fixings_path %s' % (fixingsPath))
    fixings = integration.read_fixings(fixingsPath)
    return fixings


def build_market_instance_from_file(content_path, markets_path='', market_name='@1'):
    # Read market data.
    if not markets_path:
        marketsPath = os.path.join(content_path, 'Data', 'Markets')
    else:
        marketsPath = markets_path
    log.debug('Reading markets from content_path %s: %s' % (marketsPath, market_name))

    try:
        markets = integration.read_markets(marketsPath, market_name)
    except Exception as  e:
        log.error('Unable to read market %s from content_path : %s' % (market_name, marketsPath))
        log.exception(e)
        return None

    if not markets:
        log.error('No valid markets found. Stopping.')
        return None
    return markets


def build_calc_instance(content_path='', repository=None, type='Pricing', use_file_instance=True,
                        terms_path='',
                        mde_path='',
                        model_path='',
                        terms=[], mdes=[],models=[]):
    if not repository:
        repository = integration.LocalRepository(content_path)

    instances = []
    if use_file_instance and (
            type.upper() == 'PRICING' or type.upper() == 'SENSITIVITY' or type.upper() == 'EXPOSURE'):
        # Read trade terms instances.
        if not terms_path:
            termsPath = os.path.join(content_path, 'Data', 'Terms')
        else:
            termsPath = terms_path
        termsStr = ",".join(terms)
        # print('Reading terms from content_path %s: %s' % (termsPath, termsStr if termsStr else 'ALL'))
        instances.extend(integration.read_trade_terms(termsPath, repository, termsStr))

        # # Read trade instances.
        # tradesPath = os.path.join(content_path, 'Data', 'Trades')
        # instances.extend(integration.read_trades(tradesPath, repository, ''))

    elif use_file_instance and type.upper() == 'MDE':
        # Read market data element instances.
        if not mde_path:
            mdesPath = os.path.join(content_path, 'Data', 'MDEs')
        else:
            mdesPath = mde_path
        mdeStr = ",".join(mdes)
        if mdeStr: log.info('Reading MDEs from content_path %s: %s' % (mdesPath, mdeStr))
        instances.extend(integration.read_market_data_elements(mdesPath, repository, mdeStr))

    elif use_file_instance and type.upper() == 'MODEL':
        # Read model instances.
        if not model_path:
            modelsPath = os.path.join(content_path, 'Data', 'Models')
        else:
            modelsPath = mde_path
        modelsStr = ",".join(models)
        if modelsStr: print('Reading Models from content_path %s: %s' % (modelsPath, modelsStr))
        instances.extend(integration.read_models(modelsPath, repository, modelsStr))

    # Let instances be null if not reading from files
    if use_file_instance and not instances:
        log.error('No valid instances found. Stopping.')
        return None
    if use_file_instance:
        log.info('# of file instances: {}'.format(str(len(instances))))
    return instances


def build_instances(repository, json_instances, type='Terms'):
    cail_instances = []
    for json_instance in json_instances:
        if not 'ref' in json_instance or not 'template' in json_instance or not 'data' in json_instance:
            log.error('Incomplete data. Skipping instance creation')
            continue
        template = None
        if type.upper() == 'MDE':
            try:
                template = repository.find_mde_template(json_instance['template'])
            except Exception as e:
                log.exception(e)
        elif type.upper() == 'MODEL':
            try:
                template = repository.find_model_template(json_instance['template'])
            except Exception as e:
                log.exception(e)
        else:
            try:
                template = repository.find_terms_template(json_instance['template'])
            except Exception as e:
                log.exception(e)


        if not template:
            log.error('Template {} not found. Skipping instance creation'.format(
                json_instance['template']))
            continue

        parameters = normalize_dict(json_instance['data'])
        if type.upper() == 'MDE':
            parameters['MDEID'] = str(json_instance['ref'])
        elif type.upper() == 'MODEL':
            parameters['MODELID'] = str(json_instance['ref'])
        else:
            parameters['TRADEID'] = str(json_instance['ref'])

        # print(parameters)
        instance = template.create_instance(parameters)
        cail_instances.append(instance)
    log.info('# of json instances: {}'.format(str(len(cail_instances))))
    return cail_instances


def build_trade_instances(content_path=None, repository=None, calculation_type=None,
                          terms_path=None, term_file_names=None, terms_instances=None):
    use_file_instance = False if terms_instances else True
    instances = None
    # keep compatibility for both modes
    terms_instances = terms_instances[
        'flattenedTradeList'] if 'flattenedTradeList' in terms_instances else terms_instances
    if use_file_instance:
        instances = build_calc_instance(type=calculation_type, content_path=content_path,
                                        repository=repository,
                                        terms_path=terms_path, terms=term_file_names,
                                        use_file_instance=use_file_instance)
    else:
        instances = build_instances(repository=repository,
                                    json_instances=terms_instances,
                                    type='Terms')

    if instances is None:
        log.error('Unable to build instance(s). Aborting calculation')
        return None

    return instances


def build_mde_instances(content_path=None, repository=None, calculation_type=None, mde_path=None,
                        mde_file_names=None, mde_instances=None):
    use_file_instance = False if mde_instances else True

    # keep compatibility for both modes

    if use_file_instance:
        instances = build_calc_instance(type=calculation_type, content_path=content_path,
                                        repository=repository,
                                        mde_path=mde_path, mdes=mde_file_names,
                                        use_file_instance=use_file_instance)
    else:
        mde_instances = mde_instances[
            'flattenedInstanceList'] if 'flattenedInstanceList' in mde_instances else mde_instances
        instances = build_instances(repository=repository,
                                    json_instances=mde_instances,
                                    type='MDE')

    if instances is None:
        log.error('Unable to build instance(s). Aborting calculation')
        return None

    return instances


def build_model_instances(content_path=None, repository=None, calculation_type=None, model_path=None,
                        model_file_names=None, model_instances=None):
    use_file_instance = False if model_instances else True

    # keep compatibility for both modes

    if use_file_instance:
        instances = build_calc_instance(type=calculation_type, content_path=content_path,
                                        repository=repository,
                                       models=model_file_names,
                                        use_file_instance=use_file_instance)
    else:
        mde_instances = model_instances[
            'flattenedInstanceList'] if 'flattenedInstanceList' in model_instances else model_instances
        instances = build_instances(repository=repository,
                                    json_instances=mde_instances,
                                    type='MDE')

    if instances is None:
        log.error('Unable to build instance(s). Aborting calculation')
        return None

    return instances

def normalize_dict(original):
    normalized = dict()
    if not original: return normalized

    for k in original:
        normalized.update({k.upper().strip(): original[k]})

    return normalized


# This function has a bug most likely as it is not creating n number of unique instances
def multiply_instances(base_instances, num=1):
    final_instances = list()
    for j in range(0, num):
        for i in base_instances:
            final_instances.append(i)
    log.info('Generated {} instances from {} instances'.format(len(final_instances),
                                                               len(base_instances)))
    return final_instances


def multiply_instance_payload(base_instances, iter=1):
    final_instances = list()
    for j in range(0, iter):
        for base_instance in base_instances:
            terms_instance = {}
            terms_instance['ref'] = '{}_iter{}'.format(base_instance['ref'], j + 1)
            terms_instance['template'] = base_instance['template']
            terms_instance['data'] = {}
            terms_instance['data'].update(base_instance['data'])
            final_instances.append(terms_instance)
    log.info('Generated {} instances from {} instances'.format(len(final_instances),
                                                               len(base_instances)))
    return final_instances


def generate_instance_payload(base_instance, key='', value=[]):
    permutations = []
    scenarios = []

    if bool(key) and bool(value) and key in base_instance['data']:
        for val in value:
            scenario = {}
            scenario['key'] = key
            scenario['value'] = val
            scenarios.append(scenario)
            terms_instance = {}
            terms_instance['ref'] = '{}_sc{}_{}'.format(base_instance['ref'], key, str(val))
            terms_instance['template'] = base_instance['template']
            terms_instance['data'] = {}
            terms_instance['data'].update(base_instance['data'])
            terms_instance['data'][key] = val
            permutations.append(terms_instance)

        return permutations, scenarios
    else:
        return [base_instance], scenarios


def create_table_from_list(data):
    table_headers = list(data[0].keys())

    table_values = []
    for head in table_headers:
        table_values.append([x[head] for x in data])

    return integration.Table(table_headers, table_values)



if __name__ == '__main__':
    # Test
    from market_scenario_service.ov_cail import test_data as t
    import json

    # def test_generate_instance_payload():
    all_permutations = list()
    permutations, scenarios = generate_instance_payload(t.CCBASIS_GBPUSD, 'FOREIGNSPREAD',
                                                        [0.01, 0.011, 0.012])
    all_permutations.extend(permutations)
    permutations, scenarios = generate_instance_payload(t.FXFWD_EURUSD, 'ContractFXForwardRate',
                                                        [1.35, 1.36])
    all_permutations.extend(permutations)
    permutations, scenarios = generate_instance_payload(t.FXOPT_EURUSD, 'Strike', [1.35, 1.36])
    all_permutations.extend(permutations)
    permutations, scenarios = generate_instance_payload(t.IRSWAP_USD, 'Rate', [0.01, 0.011, 0.012])
    all_permutations.extend(permutations)
    print(json.dumps(all_permutations, default=str))
    multiply = multiply_instance_payload(all_permutations, 2)
    print(json.dumps(multiply, default=str))

    data = [dict(Maturity='1W'),
            dict(Maturity='1M'),
            dict(Maturity='2M'),
            dict(Maturity='3M'),
            dict(Maturity='6M')]

    data = [dict(Name='IR.USD-LIBOR-3M.SWAP-1Y.MID', Value=0.006),
            dict(Name='IR.USD-LIBOR-3M.SWAP-10Y.MID', Value=0.022),
            dict(Name='IR.USD-LIBOR-3M.SWAP-20Y.MID', Value=0.028),
            dict(Name='IR.USD-FEDFUNDS-ON.SWAP-1Y.MID', Value=0.005),
            dict(Name='IR.USD-FEDFUNDS-ON.SWAP-10Y.MID', Value=0.020),
            dict(Name='IR.USD-FEDFUNDS-ON.SWAP-20Y.MID', Value=0.025)]
    create_table_from_list(data)
