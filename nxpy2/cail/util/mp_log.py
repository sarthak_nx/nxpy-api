import os
import logging
log = logging.getLogger('MPLog')

def info(message):
    import threading
    log.info('module name {}, Parent process {} Process id {}  Thread {} - {}'.format(__name__,
                                                                                   os.getppid(),
                                                                                   os.getpid(),
                                                                                   threading.current_thread().ident,
                                                                                   message))

def debug(message):
    import threading
    log.debug('module name {}, Parent process {} Process id {}  Thread {} - {}'.format(__name__,
                                                                                      os.getppid(),
                                                                                      os.getpid(),
                                                                                      threading.current_thread().ident,
                                                                                      message))