import random
import time
from datetime import date
from timeit import default_timer as timer

from nxpy2.cail.cail_client.cail_context_manager import CAILContextMgr
from nxpy2.cail.cail_client.cail_client import run_calculation as client_run_calculation
from nxpy2.cail.request_dto.request import CalcRequest
from nxpy2.cail.util import request_util as u
from nxpy2.cail.util import result_util as r
from nxpy2.cail.util.mp_log import info, debug
import logging

log = logging.getLogger('nxpy')

MARKETS_PATH = r'C:\_Dev\Code\market-scenario-service\tests\data\Content\Data\Markets'
MARKET_FILE_LIST = ['CVADemo_2014-01-10_EOD', 'CVADemo_2014-01-06_EOD']
FXFWD_EURUSD = {'ref': 'FXFWD_EURUSD', 'template': 'TERMS.FX.FORWARD'}
FXFWD_EURUSD['data'] = {
    "ContractFXForwardRate": 1.3628575,
    "Delivery": date(2015, 1, 8),
    "Base Notional": 1000000,
    "Fixing Date": date(2015, 1, 5),
    "FX.BASECURRENCY": "EUR",
    "FX.SPOTLAG": "2bd",
    "FX.TERMCURRENCY": "USD",
    "Counterparty": "Bank Of America",
    "LegalEntity": "Bank Of America US",
    "NettingSet": "NettingSet 1",
    "MarginSet": "MarginSet 1"
}
RESULTS_DIR = r'C:\_Dev\Code\market-scenario-service\temp\CAILServerResults'

try:
    from nxpy import integration
except ImportError:
    import sys

    print("nxpy.integration is not currently available on this platform.", file=sys.stderr)
    raise SystemExit()


class Compute:
    def default_results_callback(self, request_ref, results):
        output_file = request_ref
        from pathlib import Path
        import json
        if not Path(RESULTS_DIR).exists():
            Path(RESULTS_DIR).mkdir()
        with Path(RESULTS_DIR).joinpath(output_file + '.json').open('w') as fout:
            json.dump(results, fout, indent=4, default=str)
        print('Generated file: {}\\{}.json'.format(RESULTS_DIR, output_file))

    _num = None
    _some_variable_from_server_init = None
    _context_mgr = CAILContextMgr()

    # These 3 attributes are cached locally for now to avoid IPC. In java it would be coming from configs and distributed cache
    _parameters = None
    _instances = None
    _markets = None
    _results_callback = default_results_callback

    def __init__(self, content_path, request_queue, result_queue, lock_for_some_use, num,
                 some_variable_from_server_init, results_callback=None):
        info('Compute: {}. Compute Process Initialization called.'.format(num))
        self._context_mgr = CAILContextMgr(singleton=True)
        self._context_mgr.initialize(content_path)
        self._num = num

        self._some_variable_from_server_init = some_variable_from_server_init
        # self._results_callback = results_callback
        self._parameters = u.build_calc_parameters(content_path)
        # self._instances = u.build_calc_instance(None, self._repository, terms_path=TERMS_PATH,
        #                                   terms=TERMS_LIST)
        self._instances = [FXFWD_EURUSD]
        self._markets = u.build_market_instance_from_file(content_path=None,
                                                          markets_path=MARKETS_PATH,
                                                          market_name=",".join(MARKET_FILE_LIST))
        self.wait_for_request(request_queue, result_queue, lock_for_some_use)

    # Can potentially listen on multiple queues - generic vs context specific
    def wait_for_request(self, request_queue, result_queue, lock_for_some_use):
        while True:
            debug("Compute: {}. Waiting for Requests... ".format(self._num))
            request = request_queue.get()
            if request is None:
                break
            debug("Compute: {}. Consuming Request: {}. Outstanding queue size. {}".format(self._num,
                                                                                          request.requestRef,
                                                                                          request_queue.qsize()))
            # Switch between these execution methods
            # result = self.test_timer(request)
            result = self.run_calculation(request)
            # result = self.test_random(request)
            info("Compute: {}. Request: {} completed in {} sec".format(self._num,
                                                                       result['requestRef'],
                                                                       result['calcTime']))
            # How to publish results back to server?
            result_queue.put(result)
            request_queue.task_done()
            try:
                lock_for_some_use.acquire()
                # info('Test acquiring lock')
            finally:
                # info('Test releasing lock')
                lock_for_some_use.release()

    def run_calculation_new(self, request):
        if type(request) == dict:
            request = CalcRequest(request)
        result = client_run_calculation(request)
        calc_time = result['calcTime']
        diff_list = list()

        self._results_callback(request.requestRef, result)
        result = dict()
        result['requestRef'] = request.requestRef
        result['calcTime'] = calc_time
        result['newObjectCount'] = len(diff_list)
        # result['results'] = result
        return result

    def run_calculation(self, request):
        #TODO to be replaced by run_calculation_new
        if type(request) == dict:
            request = CalcRequest(request)
        multiply_factor = request.multiplyFactor if 'multiplyFactor' in request.toDict() else 1
        json_instances = u.multiply_instance_payload(self._instances,
                                                     multiply_factor) if multiply_factor > 1 else self._instances
        instances = u.build_instances(self._context_mgr._repository, json_instances=json_instances)

        market = self._markets[0]
        (raw_results, calc_time, diff_list) = self._context_mgr.run_calculation(
            parameters=self._parameters, instances=instances, market=market, calc_request='pv',
            request_ref=request.requestRef)

        processed_results = r.process_pricing_results(results=raw_results, severity='fatal',
                                                      print_results=False)
        self._results_callback(request.requestRef, processed_results)
        result = dict()
        result['requestRef'] = request.requestRef
        result['calcTime'] = calc_time
        result['newObjectCount'] = len(diff_list)
        # result['results'] = processed_results
        return result

    # --------- Below methods for multiprocessing test only----------#
    # Wait for specified number of seconds
    def test_timer(self, request):
        if type(request) == dict:
            request = CalcRequest(request)
        debug("Compute: {}. Now waiting for {} seconds. ".format(self._num, request.timeDelay))
        time.sleep(int(request.timeDelay))
        # info("Process: {}. Completed {} seconds' wait".format(self._num, request['timeDelay']))
        result = dict()
        result['requestRef'] = self._num
        result['calcTime'] = int(request.timeDelay)
        return result

    def test_random(self, request):
        mylist = list()
        size = request['size'] if 'size' in request.__dict__.keys() else 10000000
        calcStart = timer()
        for i in range(size):
            mylist.append(random.random())
        calcEnd = timer()
        calc_time = calcEnd - calcStart
        result = dict()
        result['requestRef'] = self._num
        result['calcTime'] = calc_time
        return result
