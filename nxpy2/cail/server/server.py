from multiprocessing import JoinableQueue, Lock
from multiprocessing.context import Process
from nxpy2.cail.server.compute import Compute
from nxpy2.cail.util.mp_log import info, debug

import logging

log = logging.getLogger('nxpy')


class CAILServer(object):
    _instance = None
    _request_queue = None  # generic queue
    _context_queues = dict()  # hold a map of contextKey vs queue
    _result_queue = None
    _lock_for_some_use = None
    _processes = dict()
    _results = list()
    _content_path = None
    _repository = None
    _initialized = False
    _result_listener_thread = None
    _calculation_log = list()

    def __new__(cls):
        if not cls._instance:
            cls._instance = super(CAILServer, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        pass

    def initialize(self, content_path, num_computes=2):
        if self._initialized:
            log.info('Already initialized')
            return False, 'Already initialized'
        self._request_queue = JoinableQueue()  # Convert to priority queue
        self._result_queue = JoinableQueue()  # Maybe different type of queue for results
        self._lock_for_some_use = Lock()
        self._calculation_log = list()
        self._content_path = content_path
        for i in range(1, num_computes + 1):
            self._processes[i] = Process(target=Compute, args=(
                self._content_path, self._request_queue, self._result_queue,
                self._lock_for_some_use, i, dict(a=1)))
            self._processes[i].start()
        # Some processes can also be reserved for context specific use cases
        self._initialized = True
        message = 'Server initialized Successfully with {} computes'.format(num_computes)
        info(message)
        self.listen_results_background()
        return True, message

    @classmethod
    def kill(cls):
        # Reset the singleton instance
        info('Server killing itself')
        cls._instance = None
        return

    def submit(self, request):
        # TODO validate that this is of type CalcRequest
        # break request into batches before putting on queue
        self._request_queue.put(request)

    def listen_results_background(self):
        log.info("Server starting background thread for listening to results")
        import threading
        self._result_listener_thread = threading.Thread(target=self.listen_results,
                                                        kwargs=dict(block=True))
        self._result_listener_thread.start()
        return

    def listen_results(self, block=True, timeout=None):
        log.info("Server listening for results")
        while True:
            try:
                result = self._result_queue.get(block, timeout)
                if result is None:
                    break
                log.debug("Server consuming Result: {}. Outstanding queue size: {}".format(
                    result['requestRef'],
                    self._result_queue.qsize()))
                self._calculation_log.append(
                    result)  # should do without result payload, just the stats
                self._results.append(result)
                self._result_queue.task_done()
            except Exception as e:
                log.info('Server stopping results listening')
                return

    def wait_till_completion(self):
        """ wait until queue is empty """
        log.info('Server waiting until all requests are processed')
        self._request_queue.join()
        return

    def terminate_processes(self):
        self.wait_till_completion()
        for id, p in self._processes.items():
            log.info('Compute: {}. Terminating'.format(id))
            p.terminate()
        # Thread wont be killed this way as its in a while True loop
        # self._result_listener_thread.join()

    def get_request_queue_size(self):
        return self._request_queue.qsize()

    def get_result_queue_size(self):
        # Should always be 0 as results need to be processed and then items evicted from queue
        return self._result_queue.qsize()

    def get_results(self):
        return self._results

    def get_calculation_log(self):
        return self._calculation_log

    def clear_calculation_log(self):
        self._calculation_log = list()

    def clear_results(self):
        self._results = list()

    def summarize_server_state(self):
        log.info('Outstanding results {}'.format(self.get_result_queue_size()))
        calculation_log = self.get_calculation_log()
        # print('Calculation Log: {}'.format(json.dumps(calculation_log)))
        total_time = sum([x['calcTime'] for x in calculation_log])
        average_time = total_time / float(len(calculation_log)) if calculation_log else 0.0
        log.info('Average Time: {} sec for {} calculations'.format(average_time,
                                                                   len(
                                                                       calculation_log) if calculation_log else 0))
        return (average_time, total_time, calculation_log)
