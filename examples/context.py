from ov_client.ov_settings import OvSettings
from nxpy2.base.base_web_app import BaseWebApp, BaseSettings

from nxpy2.cail.cail_client.cail_context_manager import CAILContextMgr

def initialize_context(initialize_ov: bool=True, parser=None, root_dir_path: str =None, config_dir_path: str = None, override_profile:str=None, logconfig_file_path: str='', disable_logs: bool=False, initialize_cail: bool = True):
    """

    :param initialize_ov: Flag whether to initialize Oneview with the settings or not
    :param parser: parser = argparse.ArgumentParser()
    :param root_dir_path: ROOT_DIR is defaulted assuming {root}/{parent}/context.py
    :param config_dir_path:
    :param override_profile:
    :param logconfig_file_path:
    :return:
    """
    print('Initializing Context...')
    try:
        initialize_base_settings(root_dir_path)
        settings_dict=BaseSettings().get('ONEVIEW', dict())
        settings_dict['ROOT_DIR'] = BaseSettings().ROOT_DIR
        settings = OvSettings(singleton=True, settings_dict=settings_dict)

        if parser:
            OvSettings().initialize_from_args(parser)
        elif settings_dict:
            OvSettings().initialize_from_dict(settings_dict)
        else:
            OvSettings().initialize_from_profile(root_dir_path=root_dir_path, config_dir_path=config_dir_path, override_profile=override_profile)

        # TODO add provision for initializing from profile but overriding from argparse
        if not disable_logs:
            OvSettings().configure_logs(logconfig_file_path=logconfig_file_path)

        BaseSettings().OUTPUT_DIR= BaseSettings().find_replace_root(BaseSettings().get('OUTPUT_DIR'))
        BaseSettings().create_path_if_not_exists(BaseSettings().get('OUTPUT_DIR'))

    except Exception as e:
        print('Error in initializing settings:'.format(e))
        import traceback
        print(traceback.format_exc())
        import sys
        sys.exit()

    if initialize_cail:
        CAILContextMgr(singleton=True).initialize(BaseSettings().append_path(suffix_path=BaseSettings().get('CONTENT').get('DIR').replace('{root}/','')),
                  recording_path=BaseSettings().append_path(suffix_path=BaseSettings().get('CONTENT').get('RECORDING_OUTPUT_DIR').replace('{root}/','')))


        # BaseSettings().MARKET_DATA_PATH= BaseSettings().append_path(suffix_path=BaseSettings().get('CONTENT').get('MARKET_DATA_PATH').replace('{root}/',''))


    if initialize_ov:
        from ov_client.ov_client import OneViewPyClient
        try:

            ov = OneViewPyClient(singleton=True)
            ov.initialize_from_settings(settings)
        except Exception as e:
            print('Error in initializing Oneview:'.format(e))
            print('Settings: {}'.format(settings))
            import traceback

            print(traceback.format_exc())
            exit()
        return OvSettings()
    else:
        return OvSettings()

def initialize_base_settings(root_dir_path):

    b = BaseWebApp(singleton=True)
    b.initialize_settings('nxpy-api', 'nxpy-api',root_dir_path =root_dir_path)

    BaseSettings(singleton=True)