from nxpy2.cail.cail_client import cail_client as c
import os, json

from nxpy2.base.base_settings import BaseSettings

def initialize_context():
    from examples import context

    context.initialize_context(initialize_ov=False, initialize_cail=True)

def calc_request(scope: str = "all"):
    calc_rqst = {"resultScopeList": [scope]}
    return calc_rqst

def market_selection(market_name: str):
    from datetime import datetime
    market = {"marketName": market_name, "marketDate": datetime.now().strftime('%Y-%m-%d')}
    return market

def context_manager():
    from nxpy2.cail.cail_client.cail_context_manager import CAILContextMgr
    cxt_mgr = CAILContextMgr()
    return cxt_mgr

def initialize_cail_client_parameters(market_name: str, unpack: bool = False, pricing_policy: str ='Standard', market_policy: str ='Standard', market_reports: list = None):
    market = market_selection(market_name)

    cail_client_parameters = c.initialize_cail_client(context_manager=context_manager(),
                                                      market_selection=market,
                                                      market_report_file_names=market_reports,
                                                      market_policy_name=market_policy,
                                                      pricing_policy_name=pricing_policy,
                                                      unpack=unpack, read_fixings=True)
    return cail_client_parameters

def rsf_calc(market_name: str, terms_list: list, market_reports: list, unpack: bool, pricing_policy: str='Standard', market_policy: str='Standard'):

    cail_client_parameters = initialize_cail_client_parameters(market_name, unpack, pricing_policy, market_policy, market_reports)

    result = c.run_sensitivity_calculation(context_manager=context_manager(), cail_client_parameters = cail_client_parameters,
                                       term_file_names = terms_list,
                                           calc_measure_selection=calc_request(), profile=False,
                                           validate_market_data=False,
                                           print_results=True)

    return result

if __name__ == '__main__':


    market_name = 'EOD_28-Jan-20'
    trade_csv = ['TRADE-1577337609881152606067']
    rsf_report = ['IRDeltaGammaByInstrument']
    pricing_policy = 'Standard_50'
    market_policy = 'Standard'
    unpack = False

    print('Entered calculation parameters:')
    print('Market Name:', market_name)
    print('Trade CSVs:', ','.join(trade_csv))
    print('RSF Report:', ','.join(rsf_report))
    print('Pricing Policy:', pricing_policy)
    print('Market Policy:', market_policy)
    print('Unpack flag:', unpack)

    initialize_context()

    result = rsf_calc(market_name, trade_csv, rsf_report, unpack, pricing_policy=pricing_policy, market_policy=market_policy)

    c.summarize_results(result)
    # Save results in file

    output_dir = BaseSettings().get('OUTPUT_DIR')
    output_file = 'rsf_calc_results_{}.json'.format(market_name)
    with open(os.path.join(output_dir, output_file), 'w') as fout:
        json.dump(result, fout, indent=2)
        print('Generated file {} in directory {}'.format(output_file, output_dir))