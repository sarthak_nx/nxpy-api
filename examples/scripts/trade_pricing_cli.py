from nxpy2.cail.cail_client import cail_client as c
import argparse
import os, json

# os.environ['NXPY_API_ROOT_DIR'] = r'C:\Temp\nxpy-api'

from nxpy2.base.base_settings import BaseSettings

def initialize_context(nxpy_api_root_dir: str = None):
    from examples import context

    if nxpy_api_root_dir:
        NXPY_API_ROOT_DIR = nxpy_api_root_dir
    else:
        NXPY_API_ROOT_DIR = os.environ['NXPY_API_ROOT_DIR']

    print('NXPY_API_ROOT_DIR:', NXPY_API_ROOT_DIR)
    context.initialize_context(initialize_ov=False, root_dir_path=NXPY_API_ROOT_DIR, initialize_cail=True)

def calc_request():
    calc_rqst = {"resultScopeList": ["pv"]}
    return calc_rqst

def market_selection(market_name):
    from datetime import datetime
    market = {"marketName": market_name, "marketDate": datetime.now().strftime('%Y-%m-%d')}
    return market

def context_manager():
    from nxpy2.cail.cail_client.cail_context_manager import CAILContextMgr
    cxt_mgr = CAILContextMgr()
    return cxt_mgr

def initialize_cail_client_parameters(market_name: str, unpack: str = None, pricing_policy: str ='Standard', market_policy: str ='Standard'):
    market = market_selection(market_name)

    try:
        if int(unpack) == 1:
            unpack_flag = True
        else:
            unpack_flag = False
    except:
        unpack_flag = False

    cail_client_parameters = c.initialize_cail_client(context_manager=context_manager(),
                                                      market_selection=market,
                                                      market_policy_name=market_policy,
                                                      pricing_policy_name=pricing_policy,
                                                      unpack=unpack, read_fixings=True)
    return cail_client_parameters

def pricing_calc(market_name: str, terms_list: list, unpack: str, pricing_policy: str='Standard', market_policy: str='Standard'):

    cail_client_parameters = initialize_cail_client_parameters(market_name, unpack, pricing_policy, market_policy)

    result = c.run_pricing_calculation(context_manager=context_manager(), cail_client_parameters = cail_client_parameters,
                                       term_file_names = terms_list,
                                           calc_measure_selection=calc_request(), profile=False,
                                           validate_market_data=False,
                                           print_results=True)

    return result

def get_trade_ref_list_from_group_id(trade_group_id):

    with open(os.path.join(BaseSettings().DATA_DIR, 'trade_group_ref.json'), 'r') as f:
        trade_groups = json.load(f)

    if trade_group_id == 'portfolio':
        trade_refs = []
        for trade_group in trade_groups:
            trade_refs.extend(trade_group['trade_refs'])
        return trade_refs
    else:
        trade_ref_check = next((item for item in trade_groups if str(item['trade_group_id']) == trade_group_id), None)
        if trade_ref_check:
            return trade_ref_check['trade_refs']

# def script_mode(market_name: str, trade_group_id: str, unpack: int = 0,root_dir: str = None):
#
#     root_dir = root_dir
#     market_name = market_name
#     trade_group_id = trade_group_id
#     unpack = unpack
#
#     print('Entered root dir {}'.format(root_dir))
#     print('Entered calculation parameters:')
#     print('Market Name:', market_name)
#     print('Trade Group ID:', trade_group_id)
#     print('Unpack flag:', unpack)
#
#     initialize_context(root_dir)
#
#     result = pricing_calc(market_name, trade_group_id, unpack)
#
#     c.summarize_results(result)
#     # Save results in file
#
#     output_dir = BaseSettings().get('OUTPUT_DIR')
#     output_file = 'calc_results_{}_trade_group_{}.json'.format(market_name, trade_group_id)
#     with open(os.path.join(output_dir, output_file), 'w') as fout:
#         json.dump(result, fout, indent=2)
#         print('Generated file {} in directory {}'.format(output_file, output_dir))
#
# script_mode('EOD_28-Jan-20_EUR', 'portfolio',0, r'C:\NxData\OneDrive - Numerix\sshreya2\Documents\Numerix\OneviewPython\nxpy-api')
if __name__ == '__main__':

    cline = argparse.ArgumentParser(description='Triggers pricing in CAIL using Python SDK and requires name of Market and Trade Group ID.')
    cline.add_argument('-market_name', default='', help='market data file name, no file extension')
    cline.add_argument('-market_policy', default='Standard', help='market policy file name, no file extension')
    cline.add_argument('-pricing_policy', default='Standard', help='pricing policy file name, no file extension')
    cline.add_argument('-unpack', default=0, help='whether to dump unpacked instantiations')
    cline.add_argument('-trade_csv_list', default=[], help='list of trades to price')
    args = cline.parse_args()

    print('Entered calculation parameters:')
    print('Market Name:', args.market_name)
    print('Trade Group ID:', args.trade_csv_list)
    print('Market Policy:', args.market_policy)
    print('Pricing Policy:', args.pricing_policy)
    print('Unpack flag:', args.unpack)

    initialize_context()

    result = pricing_calc(args.market_name, args.trade_csv_list, args.unpack, args.pricing_policy, args.market_policy)

    c.summarize_results(result)
    # Save results in file

    output_dir = BaseSettings().get('OUTPUT_DIR')
    output_file = 'calc_results_{}.json'.format(args.market_name)
    with open(os.path.join(output_dir, output_file), 'w') as fout:
        json.dump(result, fout, indent=2)
        print('Generated file {} in directory {}'.format(output_file, output_dir))