SET PYTHON_PATH={temp}\python.exe
SET PIP_PATH={temp}\pip.exe
SET SCRIPT_PATH={temp}\nxpy-api

cd %SCRIPT_PATH%

%PIP_PATH% install wheel
%PIP_PATH% install -r requirements.txt
%PYTHON_PATH% setup.py bdist_wheel

cd lib
%PIP_PATH% install oneview_python-2.0.1-py3-none-any.whl

cd ../dist
%PIP_PATH% install nxpy_api-0.1-py3-none-any.whl

cd ../